/* Copyright (c) 2021 Xie Meiyi(xiemeiyi@hust.edu.cn) and OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by Wangyunlai on 2022/5/22.
//

#include "sql/stmt/insert_stmt.h"
#include "common/log/log.h"
#include "storage/common/db.h"
#include "storage/common/table.h"
#include <iostream>
#include <cmath>
#include <sstream>
int get_int_from_char(char in[]);
float get_float_from_char(char in[]);

InsertStmt::InsertStmt(Table *table, const Value *values, int value_amount, int tuple_amount)
  : table_ (table), values_(values), value_amount_(value_amount), tuple_amount_(tuple_amount)
{}

RC InsertStmt::create(Db *db, const Inserts &inserts, Stmt *&stmt)
{
  const char *table_name = inserts.relation_name;
  if (nullptr == db || nullptr == table_name || inserts.value_num <= 0) {
    LOG_WARN("invalid argument. db=%p, table_name=%p, value_num=%d", 
             db, table_name, inserts.value_num);
    return RC::INVALID_ARGUMENT;
  }

  // check whether the table exists
  Table *table = db->find_table(table_name);
  if (nullptr == table) {
    LOG_WARN("no such table. db=%s, table_name=%s", db->name(), table_name);
    return RC::SCHEMA_TABLE_NOT_EXIST;
  }

  // check the fields number
  const Value *values = inserts.values;
  const int value_num = inserts.value_num;
  const TableMeta &table_meta = table->table_meta();
  const int field_num = table_meta.field_num() - table_meta.sys_field_num();
  if (value_num % field_num) {
    LOG_WARN("schema mismatch. value num=%d, field num in schema=%d", value_num, field_num);
    return RC::SCHEMA_FIELD_MISSING;
  }
  const int tuple_num = value_num / field_num;

  // check fields type
  const int sys_field_num = table_meta.sys_field_num();
  int offset = 0;
  for (int i = 0; i < tuple_num; i++) {
    for (int j = 0; j < field_num; j++) {
      const FieldMeta *field_meta = table_meta.field(j + sys_field_num);
      const AttrType field_type = field_meta->type(); // 创建表时规定的类型
      const AttrType value_type = values[offset + j].type; // 真实insert时输入的类型
      if (!(field_type == value_type || (field_type == DATES && value_type == CHARS))) { // TODO try to convert the value type to field type
        // type convertion tong
        if (field_type == INTS && value_type == CHARS){ // insert into t values('1','b','12');
          memcpy((void*)(&(values[offset + j].type)), &field_type, sizeof(AttrType));
          int tmp2 = get_int_from_char((char*)(values[offset + j].data));
          memcpy(values[offset + j].data, &tmp2, field_meta->len());
        } else if (field_type == INTS && value_type == FLOATS) {
          memcpy((void*)(&(values[offset + j].type)), &field_type, sizeof(AttrType));
          float* tmp = (float*)(values[offset + j].data); //取多大
          int tmp2 = round(*tmp);
          memcpy(values[offset + j].data, &tmp2, field_meta->len());
        } else if (field_type == FLOATS && value_type == INTS){ // insert into t values(1,'b','12');进入这里很不合理
          memcpy((void*)(&(values[offset + j].type)), &field_type, sizeof(AttrType));
          int* tmp = (int*)(values[offset + j].data); //取多大
          float tmp2 = (*tmp);
          memcpy(values[offset + j].data, &tmp2, field_meta->len());
        } else if (field_type == FLOATS && value_type == CHARS){ // insert into t values(1,'b','12');
          memcpy((void*)(&(values[offset + j].type)), &field_type, sizeof(AttrType));
          float tmp2 = get_float_from_char((char*)(values[offset + j].data));
          memcpy(values[offset + j].data, &tmp2, field_meta->len());
        } else if (field_type == CHARS && value_type == INTS) {
          memcpy((void*)(&(values[offset + j].type)), &field_type, sizeof(AttrType));
          int *tmp = (int *)values[offset + j].data;
          std::ostringstream str_stream;
          str_stream << *tmp;
          std::string tmp2 = str_stream.str();
          tmp2.push_back('\0');
          tmp2.resize(4);
          memcpy(values[offset + j].data, tmp2.c_str(), tmp2.size());
        } else if (field_type == CHARS && value_type == FLOATS) {
          memcpy((void*)(&(values[offset + j].type)), &field_type, sizeof(AttrType));
          float *tmp = (float *)values[offset + j].data;
          std::ostringstream str_stream;
          str_stream << *tmp;
          std::string tmp2 = str_stream.str();
          tmp2.push_back('\0');
          tmp2.resize(4);
          memcpy(values[offset + j].data, tmp2.c_str(), tmp2.size());
        } else if (field_type == TEXTS && value_type == CHARS) {
          memcpy((void*)(&(values[offset + j].type)), &field_type, sizeof(AttrType));
        } else if (value_type == NULLS) {
          if (!field_meta->nullable()) {
            return RC::CONSTRAINT_NOTNULL;
          }
        } else {
          LOG_WARN("field type mismatch. table=%s, field=%s, field type=%d, value_type=%d", 
                  table_name, field_meta->name(), field_type, value_type);
          return RC::SCHEMA_FIELD_TYPE_MISMATCH;
        }
        // end
      } 
    }
    offset += field_num;
  }
  // everything alright
  stmt = new InsertStmt(table, values, value_num, tuple_num);
  return RC::SUCCESS;
}

// add by tong
int get_int_from_char(char in[]){
   int i = 0;
   int sum = 0;
   while ((in[i] != '\0') && (in[i] >= '0') && (in[i] <= '9')) {
      sum = sum * 10 + (in[i] - '0');
      i += 1;
   }
   return sum;
}
float get_float_from_char(char in[]){
   int i = 0;
   int flag = 0;
   float sum = 0.0;
   float after = 0.0;
   float after_b = 1;
   while ((in[i] >= '0') && (in[i] <= '9')){
      sum = sum * 10.0 + (in[i] - '0');
      i += 1; 
   }
   if (!((in[i] >= '0') && (in[i] <= '9')) && !(in[i] == '.')) {
      return (sum+after);
   } else if ((in[i] == '.') && (flag == 0)) {
      i += 1;
      flag = 1;
      while ((in[i] >= '0') && (in[i] <= '9')){
         after = after + 0.1 * (in[i] - '0') * after_b;
         after_b = 0.1 * after_b;
         i += 1;
      }
      return (sum+after);
   }
   return (sum+after);
}
// end
