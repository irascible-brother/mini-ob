/* Copyright (c) 2021 Xie Meiyi(xiemeiyi@hust.edu.cn) and OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by Wangyunlai on 2022/5/22.
//

#include "rc.h"
#include "common/log/log.h"
#include "common/lang/string.h"
#include "sql/stmt/filter_stmt.h"
#include "storage/common/db.h"
#include "storage/common/table.h"
#include "sql/stmt/select_stmt.h"

FilterStmt::~FilterStmt()
{
  for (FilterUnit *unit : filter_units_) {
    delete unit;
  }
  filter_units_.clear();
}

RC FilterStmt::create(Db *db, Table *default_table, std::unordered_map<std::string, Table *> *tables,
		      const Condition *conditions, int condition_num,
		      FilterStmt *&stmt,
          std::unordered_map<std::string, std::string> father_query_table_alias)
{
  RC rc = RC::SUCCESS;
  stmt = nullptr;

  FilterStmt *tmp_stmt = new FilterStmt();
  for (int i = 0; i < condition_num; i++) {
    FilterUnit *filter_unit = nullptr;
    rc = create_filter_unit(db, default_table, tables, conditions[i], filter_unit, father_query_table_alias);
    if (rc != RC::SUCCESS) {
      delete tmp_stmt;
      LOG_WARN("failed to create filter unit. condition index=%d", i);
      return rc;
    }
    tmp_stmt->filter_units_.push_back(filter_unit);
  }

  stmt = tmp_stmt;
  return rc;
}

RC get_table_and_field(Db *db, Table *default_table, std::unordered_map<std::string, Table *> *tables,
		       const RelAttr &attr, Table *&table, const FieldMeta *&field,
           std::unordered_map<std::string, std::string> &father_query_tables_alias)
{
  if (common::is_blank(attr.relation_name)) {
    table = default_table;
  } else if (nullptr != tables) {
    const char *table_name = attr.relation_name;
    auto table_alias_iter = father_query_tables_alias.find(table_name);
    if (table_alias_iter != father_query_tables_alias.end()) {
      table_name = table_alias_iter->second.c_str();
    }
    auto iter = tables->find(table_name);
    if (iter != tables->end()) {
      table = iter->second;
    }
  } else {
    table = db->find_table(attr.relation_name);
  }
  if (nullptr == table) {
    LOG_WARN("No such table: attr.relation_name: %s", attr.relation_name);
    return RC::SCHEMA_TABLE_NOT_EXIST;
  }

  field = table->table_meta().field(attr.attribute_name);
  if (nullptr == field) {
    LOG_WARN("no such field in table: table %s, field %s", table->name(), attr.attribute_name);
    table = nullptr;
    return RC::SCHEMA_FIELD_NOT_EXIST;
  }

  return RC::SUCCESS;
}

RC FilterStmt::create_set(Db *db, Table *default_table, std::unordered_map<std::string, Table *> *tables,
		      const SetCondition *conditions, int condition_num,
		      FilterStmt *&stmt,
          std::unordered_map<std::string, std::string> father_query_tables_alias)
{
  RC rc = RC::SUCCESS;
  stmt = nullptr;

  FilterStmt *set_stmt = new FilterStmt();
  for (int i = 0; i < condition_num; i++) {
    FilterUnit *filter_unit = nullptr;
    Expression *left = nullptr;
    Expression *right = nullptr;

    Table *table = nullptr;
    const FieldMeta *field = nullptr;
    rc = get_table_and_field(db, default_table, tables, conditions[i].left_field, table, field, father_query_tables_alias);  
    if (rc != RC::SUCCESS) {
      LOG_WARN("cannot find attr");
      delete set_stmt;
      return rc;
    }
    left = new FieldExpr(table, field);

    if(conditions[i].type == ExprType::QUERY) {
      Stmt *stmt = nullptr;
      rc = SelectStmt::create(db, *(conditions[i].right_subquery), stmt, father_query_tables_alias);
      right = new QueryExpr(stmt);
      if (rc != RC::SUCCESS) {
        LOG_WARN("cannot find attr");
        delete left;
        delete set_stmt;
        return rc;
      }
    } else if(conditions[i].type == ExprType::VALUE) {
      right = new ValueListExpr(conditions[i].right_value);
    }
    filter_unit = new FilterUnit;
    filter_unit->set_comp(CompOp::EQUAL_TO);
    filter_unit->set_left(left);
    filter_unit->set_right(right);
    set_stmt->filter_units_.push_back(filter_unit);
  }

  stmt = set_stmt;
  return rc;
}

RC FilterStmt::add_unit(FilterUnit *filter_unit) {
  filter_units_.push_back(filter_unit);
  return RC::SUCCESS;
}
//remove but dont delete *
RC FilterStmt::erase_unit(int i) {
  filter_units_.erase(filter_units_.begin() + i);
  return RC::SUCCESS;
}

RC FilterStmt::create_filter_unit(Db *db, Table *default_table, std::unordered_map<std::string, Table *> *tables,
				  const Condition &condition, FilterUnit *&filter_unit,
          std::unordered_map<std::string, std::string> &father_query_table_alias)
{
  RC rc = RC::SUCCESS;
  
  CompOp comp = condition.comp;
  if (comp < EQUAL_TO || comp >= NO_OP) {
    LOG_WARN("invalid compare operator : %d", comp);
    return RC::INVALID_ARGUMENT;
  }

  Expression *left = nullptr;
  Expression *right = nullptr;

  if (condition.left_type == ExprType::FIELD) {
    Table *table = nullptr;
    const FieldMeta *field = nullptr;
    rc = get_table_and_field(db, default_table, tables, condition.left_attr, table, field, father_query_table_alias);  
    if (rc != RC::SUCCESS) {
      LOG_WARN("cannot find attr");
      return rc;
    }
    left = new FieldExpr(table, field);
  } else if(condition.left_type == ExprType::VALUE){
    // left = new ValueExpr(condition.left_value);
    left = new ValueListExpr(condition.left_value);
  } else if(condition.left_type == ExprType::QUERY) {
    for(auto it=tables->begin(); it!=tables->end();it++){
      bool is_out = true;
      for(std::size_t i = 0; i < condition.left_subquery->relation_num; i++) {
        if(it->first == std::string(condition.left_subquery->relations[i])) {
          is_out = false;
        }
      }
      if(is_out == true) {
        char *a = const_cast<char *>(it->first.c_str());
        condition.left_subquery->out_relations[condition.left_subquery->out_relations_num] = (char *)malloc(sizeof(char) * (it->first.size() + 1));
        memcpy(condition.left_subquery->out_relations[condition.left_subquery->out_relations_num], a, it->first.size());
        condition.left_subquery->out_relations[condition.left_subquery->out_relations_num][it->first.size()] = '\0';
        condition.left_subquery->out_relations_num++;
      }
        
    }
    Stmt *stmt = nullptr;
    SelectStmt::create(db, *(condition.left_subquery), stmt, father_query_table_alias);
    left = new QueryExpr(stmt);
  } else if(condition.left_type == ExprType::NONE) {
    left = new NoneExpr();
  }

  if (condition.right_type == ExprType::FIELD) {
    Table *table = nullptr;
    const FieldMeta *field = nullptr;
    rc = get_table_and_field(db, default_table, tables, condition.right_attr, table, field, father_query_table_alias);  
    if (rc != RC::SUCCESS) {
      LOG_WARN("cannot find attr");
      delete left;
      return rc;
    }
    right = new FieldExpr(table, field);
  } else if(condition.right_type == ExprType::VALUE){
    // right = new ValueExpr(condition.right_value);
    right = new ValueListExpr(condition.right_value);
  } else if(condition.right_type == ExprType::QUERY) {
    for(auto it=tables->begin(); it!=tables->end();it++){
      bool is_out = true;
      for(std::size_t i = 0; i < condition.right_subquery->relation_num; i++) {
        if(it->first == std::string(condition.right_subquery->relations[i])) {
          is_out = false;
        }
      }
      if(is_out == true) {
        char *a = const_cast<char *>(it->first.c_str());
        condition.right_subquery->out_relations[condition.right_subquery->out_relations_num] = (char *)malloc(sizeof(char) * (it->first.size() + 1));
        memcpy(condition.right_subquery->out_relations[condition.right_subquery->out_relations_num], a, it->first.size());
        condition.right_subquery->out_relations[condition.right_subquery->out_relations_num][it->first.size()] = '\0';
        condition.right_subquery->out_relations_num++;
      }
        
    }
    // right = new QueryExpr(condition.right_subquery);
    Stmt *stmt = nullptr;
    rc = SelectStmt::create(db, *(condition.right_subquery), stmt, father_query_table_alias);
    right = new QueryExpr(stmt);
    if (rc != RC::SUCCESS) {
      LOG_WARN("cannot find attr");
      delete right;
      return rc;
    }
    SelectStmt *s_test = (SelectStmt *)stmt;
    if(comp == IS_IN || comp == NOT_IN) {
      if(s_test->aggrs().size() == 0 && s_test->query_fields().size() != 1) {
        return RC::INVALID_ARGUMENT;
      }
      if(s_test->query_fields().size() != 1 && s_test->aggrs().size() != s_test->query_fields().size()) {
        return RC::INVALID_ARGUMENT;
      }
    }
  } else if(condition.right_type == ExprType::LIST) {
    right = new ValueListExpr(const_cast<Condition &>(condition).right_value_list);
  }
  
  filter_unit = new FilterUnit;
  filter_unit->set_is_and(condition.is_and);
  filter_unit->set_comp(comp);
  filter_unit->set_left(left);
  filter_unit->set_right(right);

  // 检查两个类型是否能够比较
  return rc;
}
