/* Copyright (c) 2021 Xie Meiyi(xiemeiyi@hust.edu.cn) and OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by Wangyunlai on 2022/6/5.
//

#pragma once

#include <vector>

#include "rc.h"
#include "sql/stmt/stmt.h"
#include "storage/common/field.h"
#include "algorithm"
#include "unordered_set"
#include "unordered_map"

class FieldMeta;
class FilterStmt;
class Db;
class Table;

struct SelectColumn {
  std::string table_;
  std::string column_;

  SelectColumn(const std::string &table, const std::string &column) {
    table_ = table;
    column_ = column;
  }

  SelectColumn(const char *table, const char *column) {
    table_ = table;
    column_ = column;
  }

  bool operator == (const SelectColumn &rhs) const {
    return table_ == rhs.table_ && column_ == rhs.column_;
  }
};

namespace std {
  template <> //function-template-specialization
    class hash<SelectColumn>{
    public :
      size_t operator()(const SelectColumn &select_column) const {
        return std::hash<std::string>()(select_column.table_) ^ std::hash<std::string>()(select_column.column_);
      }
  };
};

class SelectStmt : public Stmt
{
public:

  SelectStmt() = default;
  ~SelectStmt() override;

  StmtType type() const override { return StmtType::SELECT; }
public:
  static RC create(Db *db, const Selects &select_sql, Stmt *&stmt,
    std::unordered_map<std::string, std::string> father_query_tables_alias = std::unordered_map<std::string, std::string>());

public:
  void add_table(Table *t) {tables_.push_back(t);}
  void add_out_table(Table *t) {out_tables_.push_back(t);}
  const std::vector<Table *> &out_tables() const { return out_tables_; }
  const std::vector<Table *> &tables() const { return tables_; }
  const std::vector<QueryField> &query_fields() const { return query_fields_; }
  const std::vector<RelAggr> &aggrs() const {return aggrs_;}
  const std::vector<Order> &orders() const { return orders_; }
  FilterStmt *filter_stmt() const { return filter_stmt_; }
  const std::vector<Field> &group_fields() const { return group_fields_; }
  HavingCondition condition_;
  bool is_having = false;
  std::vector<int> aggr_pos;
  int group_num = 0;
private:
  std::vector<QueryField> query_fields_;
  std::vector<Field> group_fields_;
  std::vector<Table *> tables_;
  std::vector<RelAggr> aggrs_;
  std::vector<Table *> out_tables_;
  std::vector<Order> orders_;
  FilterStmt *filter_stmt_ = nullptr;
};

