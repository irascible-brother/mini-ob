/* Copyright (c) 2021 Xie Meiyi(xiemeiyi@hust.edu.cn) and OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by Wangyunlai on 2022/6/6.
//

#include "sql/stmt/select_stmt.h"
#include "sql/stmt/filter_stmt.h"
#include "common/log/log.h"
#include "common/lang/string.h"
#include "storage/common/db.h"
#include "storage/common/table.h"

SelectStmt::~SelectStmt()
{
  if (nullptr != filter_stmt_) {
    delete filter_stmt_;
    filter_stmt_ = nullptr;
  }
}

static void wildcard_fields(Table *table, std::vector<QueryField> &field_metas)
{
  const TableMeta &table_meta = table->table_meta();
  const int field_num = table_meta.field_num();
  for (int i = table_meta.sys_field_num(); i < field_num; i++) {
    field_metas.push_back(QueryField(table, table_meta.field(i), nullptr));
  }
}

static void wildcard_fields(std::vector<Table *> &tables, std::vector<QueryField> &field_metas,
  std::unordered_map<std::string, std::string> &tables_alias) {
  // for (Table *table : tables) {
  for (auto table_iter = tables.rbegin(); table_iter != tables.rend(); table_iter++) {
    Table *table  = *table_iter;
    const TableMeta &table_meta = table->table_meta();
    const int field_num = table_meta.field_num();
    for (int i = table_meta.sys_field_num(); i < field_num; i++) {
      bool found = false;
      for (auto iter : tables_alias) {
        if (iter.second != table->name()) {
          continue;
        }
        field_metas.push_back(QueryField(table, table_meta.field(i), std::string(iter.first + "." + table_meta.field(i)->name()).c_str()));
        found = true;
      }
      if (!found) field_metas.push_back(QueryField(table, table_meta.field(i), nullptr));
    } 
  }
}

RC SelectStmt::create(Db *db, const Selects &select_sql, Stmt *&stmt,
  std::unordered_map<std::string, std::string> father_query_tables_alias)
{
  if (nullptr == db) {
    LOG_WARN("invalid argument. db is null");
    return RC::INVALID_ARGUMENT;
  }

  std::unordered_set<std::string> appear;
  std::unordered_map<std::string, std::string> tables_alias = father_query_tables_alias;
  for (size_t i = 0; i < select_sql.table_alias_num; i++) {
    if (appear.find(select_sql.table_alias_to[i]) != appear.end()) {
      return RC::INVALID_ARGUMENT;
    }
    tables_alias[select_sql.table_alias_to[i]] = select_sql.table_alias_from[i];
    appear.insert(select_sql.table_alias_to[i]);
  }

  std::unordered_map<SelectColumn, std::string> columns_alias;
  for (size_t i = 0; i < select_sql.column_alias_num; i++) {
    SelectColumn select_column(select_sql.column_alias_table[i] == nullptr ? "" : select_sql.column_alias_table[i], select_sql.column_alias_from[i]);
    columns_alias.insert(std::pair<SelectColumn, std::string>(select_column, select_sql.column_alias_to[i]));
  }

  // collect tables in `from` statement
  std::vector<Table *> tables;
  std::unordered_map<std::string, Table *> table_map;

  if(select_sql.group_num == 0 && select_sql.aggr_num > 0 && select_sql.attr_num > select_sql.aggr_num) {
    LOG_WARN("invalid argument. input error");
    return RC::INVALID_ARGUMENT;
  }
  for (size_t i = 0; i < select_sql.relation_num; i++) {
    const char *table_name = select_sql.relations[i];
    if (nullptr == table_name) {
      LOG_WARN("invalid argument. relation name is null. index=%d", i);
      return RC::INVALID_ARGUMENT;
    }

    Table *table = db->find_table(table_name);
    if (nullptr == table) {
      LOG_WARN("no such table. db=%s, table_name=%s", db->name(), table_name);
      return RC::SCHEMA_TABLE_NOT_EXIST;
    }

    tables.push_back(table);
    table_map.insert(std::pair<std::string, Table*>(table_name, table));
  }
  
  for (size_t i = 0; i < select_sql.out_relations_num; i++) {
    const char *table_name = select_sql.out_relations[i];
    if (nullptr == table_name) {
      LOG_WARN("invalid argument. relation name is null. index=%d", i);
      return RC::INVALID_ARGUMENT;
    }

    Table *table = db->find_table(table_name);
    if (nullptr == table) {
      LOG_WARN("no such table. db=%s, table_name=%s", db->name(), table_name);
      return RC::SCHEMA_TABLE_NOT_EXIST;
    }
    table_map.insert(std::pair<std::string, Table*>(table_name, table));
  }
  // collect query fields in `select` statement
  std::vector<QueryField> query_fields;
  if(select_sql.group_num > 0) {
    for(u_int i = 0; i < select_sql.attr_num; i++) {
      if(!select_sql.attributes[i].is_agg) {
        bool is_same = false;
        for(u_int j = 0; j < select_sql.group_num; j++) {
          if((0 == strcmp(select_sql.attributes[i].attribute_name,select_sql.groups[j].attribute_name)) && ((common::is_blank(select_sql.attributes[i].relation_name) && common::is_blank(select_sql.groups[j].relation_name))||(0 == strcmp(select_sql.attributes[i].relation_name,select_sql.groups[j].relation_name)))) {
            is_same = true;
            break;
          }
        }
        if(is_same == false) {
          return RC::INVALID_ARGUMENT;
        }
      }
    }
  }
  
  for (int i = select_sql.attr_num - 1; i >= 0; i--) {
    const RelAttr &relation_attr = select_sql.attributes[i];

    if (common::is_blank(relation_attr.relation_name) && 0 == strcmp(relation_attr.attribute_name, "*")) {
      // select * from ...
      std::size_t start = query_fields.size();
      // for(int i = tables.size() - 1; i >=0; i--) {
      //   wildcard_fields(tables[i], query_fields);
      // }
      if (tables.size() == 1) {
        wildcard_fields(tables[0], query_fields);
      } else {
        wildcard_fields(tables, query_fields, tables_alias);
      }
      if(relation_attr.is_agg) {
        for(std::size_t j = start; j < query_fields.size(); j++) {
          query_fields[j].is_agg = true;
          query_fields[j].agg = relation_attr.agg;
          query_fields[j].end = query_fields.size();
        }
      }
      for(std::size_t j = start; j < query_fields.size(); j++) {
        query_fields[j].field_type = relation_attr.field_type;
      }
      SelectColumn select_column("", "*");
      auto iter = columns_alias.find(select_column);
      if (iter != columns_alias.end()) {
        query_fields[start].set_alias(iter->second);
      }

    } else if (!common::is_blank(relation_attr.relation_name)) { // TODO
      // select a.id, b.*, *.c from ...
      const char *table_name = relation_attr.relation_name;
      const char *field_name = relation_attr.attribute_name;

      if (0 == strcmp(table_name, "*")) {
        if (0 != strcmp(field_name, "*")) {
          LOG_WARN("invalid field name while table is *. attr=%s", field_name);
          return RC::SCHEMA_FIELD_MISSING;
        }
        std::size_t start = query_fields.size();
        for (Table *table : tables) {
          wildcard_fields(table, query_fields);
        }
        if(relation_attr.is_agg) {
          for(std::size_t j = start; j < query_fields.size(); j++) {
            query_fields[j].is_agg = true;
            query_fields[j].agg = relation_attr.agg;
            query_fields[j].end = query_fields.size();
          }
        }
        for(std::size_t j = start; j < query_fields.size(); j++) {
          query_fields[j].field_type = relation_attr.field_type;
        }
      } else {
        auto tables_alias_iter = tables_alias.find(table_name);
        if (tables_alias_iter != tables_alias.end()) {
          char *new_table_name = (char *)malloc(tables_alias_iter->second.size() + 1);
          memcpy(new_table_name, tables_alias_iter->second.c_str(), tables_alias_iter->second.size());
          new_table_name[tables_alias_iter->second.size()] = '\0';
          free((void *)table_name);
          memcpy((void *)&relation_attr.relation_name, &new_table_name, sizeof(char *));
          table_name = relation_attr.relation_name;
        }

        auto iter = table_map.find(table_name);
        if (iter == table_map.end()) {
          LOG_WARN("no such table in from list: %s", table_name);
          return RC::SCHEMA_FIELD_MISSING;
        }

        Table *table = iter->second;
        if (0 == strcmp(field_name, "*")) {
          std::size_t start = query_fields.size();
          if (tables.size() == 1) {
            wildcard_fields(table, query_fields);
          } else {
            std::vector<Table *> tables{ table };
            wildcard_fields(tables, query_fields, tables_alias);
          }
          if(relation_attr.is_agg) {
            for(std::size_t j = start; j < query_fields.size(); j++) {
              query_fields[j].is_agg = true;
              query_fields[j].agg = relation_attr.agg;
              query_fields[j].end = query_fields.size();
            }
          }
          for(std::size_t i = start; i < query_fields.size(); i++) {
            query_fields[i].field_type = relation_attr.field_type;
          }
        } else {
          const FieldMeta *field_meta = table->table_meta().field(field_name);
          if (nullptr == field_meta) {
            LOG_WARN("no such field. field=%s.%s.%s", db->name(), table->name(), field_name);
            return RC::SCHEMA_FIELD_MISSING;
          }

          auto columns_alias_iter = columns_alias.end();
          if (tables_alias_iter == tables_alias.end()) {
            // a.id 其中 a 是真正的表名
            SelectColumn select_column(table_name, field_name);
            columns_alias_iter = columns_alias.find(select_column);
          } else {
            SelectColumn select_column(tables_alias_iter->first, field_name);
            columns_alias_iter = columns_alias.find(select_column);
          }
          if (columns_alias_iter == columns_alias.end()) {
            if (tables_alias_iter != tables_alias.end() && tables.size() > 1) {
              query_fields.push_back(QueryField(table, field_meta, std::string(tables_alias_iter->first + "." + field_name).c_str()));
            } else {
              query_fields.push_back(QueryField(table, field_meta, nullptr));
            }
          } else {
            // c 作为 a.id 的别名
            query_fields.push_back(QueryField(table, field_meta, columns_alias_iter->second.c_str()));
          }

          if(relation_attr.is_agg) {
            query_fields[query_fields.size() - 1].is_agg = true;
            query_fields[query_fields.size() - 1].agg = relation_attr.agg;
            query_fields[query_fields.size() - 1].end = query_fields.size();
          }
          query_fields[query_fields.size() - 1].field_type = relation_attr.field_type;
        }
      }
    } else {
      // select id from ...
      if (tables.size() != 1) {
        LOG_WARN("invalid. I do not know the attr's table. attr=%s", relation_attr.attribute_name);
        return RC::SCHEMA_FIELD_MISSING;
      }

      Table *table = tables[0];
      const FieldMeta *field_meta = table->table_meta().field(relation_attr.attribute_name);
      if (nullptr == field_meta) {
        LOG_WARN("no such field. field=%s.%s.%s", db->name(), table->name(), relation_attr.attribute_name);
        return RC::SCHEMA_FIELD_MISSING;
      }

      SelectColumn select_column("", relation_attr.attribute_name);
      auto columns_alias_iter = columns_alias.find(select_column);
      if (columns_alias_iter == columns_alias.end()) {
        query_fields.push_back(QueryField(table, field_meta, nullptr));
      } else {
        query_fields.push_back(QueryField(table, field_meta, columns_alias_iter->second.c_str()));
      }
      if(relation_attr.is_agg) {
        query_fields[query_fields.size() - 1].is_agg = true;
        query_fields[query_fields.size() - 1].agg = relation_attr.agg;
        query_fields[query_fields.size() - 1].end = query_fields.size();
      }
      query_fields[query_fields.size() - 1].field_type = relation_attr.field_type;
    }
  }
  std::vector<RelAggr> aggrs;
  for(int i = select_sql.aggr_num - 1; i >= 0; i--) {
    aggrs.push_back(select_sql.aggregations[i]);
  }

  std::vector<Order> orders(select_sql.order_num);
  for (u_int i = 0; i < select_sql.order_num; i++) {
    orders[i] = select_sql.orders[i];
  }
  std::reverse(orders.begin(), orders.end());

  LOG_INFO("got %d tables in from stmt and %d fields in query stmt", tables.size(), query_fields.size());

  Table *default_table = nullptr;
  if (tables.size() == 1) {
    default_table = tables[0];
  }

  // create filter statement in `where` statement
  FilterStmt *filter_stmt = nullptr;
  RC rc = FilterStmt::create(db, default_table, &table_map,
           select_sql.conditions, select_sql.condition_num, filter_stmt, tables_alias);
  if (rc != RC::SUCCESS) {
    LOG_WARN("cannot construct filter stmt");
    return rc;
  }
  // everything alright
  SelectStmt *select_stmt = new SelectStmt();
  select_stmt->tables_.swap(tables);
  select_stmt->query_fields_.swap(query_fields);
  select_stmt->group_num = select_sql.group_num;
  select_stmt->filter_stmt_ = filter_stmt;
  select_stmt->condition_ = select_sql.having_condition;
  select_stmt->is_having = select_sql.is_having;
  select_stmt->aggrs_.swap(aggrs);
  select_stmt->orders_.swap(orders);
  stmt = select_stmt;
  return RC::SUCCESS;
}
