/* Copyright (c) 2021 Xie Meiyi(xiemeiyi@hust.edu.cn) and OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by WangYunlai on 2022/6/7.
//

#pragma once

#include <iostream>
#include "string.h"
#include "storage/common/table.h"
#include "storage/common/field_meta.h"
#include "common/log/log.h"
class TupleCell
{
public: 
  TupleCell() = default;
  
  TupleCell(FieldMeta *meta, char *data)
    : TupleCell(meta->type(), data)
  {}
  TupleCell(AttrType attr_type, char *data)
    : attr_type_(attr_type), data_(data)
  {}

  void set_type(AttrType type) { this->attr_type_ = type; }
  void set_length(int length) { this->length_ = length; }
  void set_data(char *data) { this->data_ = data; }
  void set_data(const char *data) { this->set_data(const_cast<char *>(data)); }

  static int get_int_from_char(char in[]);
  static float get_float_from_char(char in[]);
  void to_string(std::ostream &os, RecordFileHandler *handler) const;

  int compare(const TupleCell &other) const;

  const char *data() const
  {
    return data_;
  }

  int length() const { return length_; }

  AttrType attr_type() const
  {
    return attr_type_;
  }

  bool is_null() const {
    uint32_t meta = *(uint32_t *)(data_ - 4);
    return meta & 1;
  }

private:
  AttrType attr_type_ = UNDEFINED;
  int length_ = -1;
  char *data_ = nullptr; // real data. no need to move to field_meta.offset
};
class TulpeCellOut {
public:
  TulpeCellOut() = default;
  TulpeCellOut(const char *data, int length, AttrType type) {
    attr_type_ = type;
    length_ = length;
    data_ = (char *)malloc(sizeof(char) * length + 4);
    memcpy(data_, data - 4, sizeof(char) * length + 4);
    data_ = data_ + 4;
    cell_ = new TupleCell(attr_type_, data_);
    cell_->set_length(length_);
  }
  ~TulpeCellOut() {
    if(data_) {
      data_ = data_ - 4;
      free(data_) ;
      data_ = nullptr;
    }
    if(cell_) {
      delete cell_;
      cell_ = nullptr;
    }

    
  }
  void set_type(AttrType type) { this->attr_type_ = type; this->cell_->set_type(type);}
  void set_length(int length) { this->length_ = length; this->cell_->set_length(length);}
  void set_data(char *data) { memcpy(data_ - 4, data - 4, 8); this->cell_->set_data(this->data_);}
  void set_normal_data(char *data) {memcpy(data_, data, 4); this->cell_->set_data(this->data_);}
  void set_data(const char *data) { memcpy(data_ - 4, data - 4, 8);  this->cell_->set_data(this->data_);}
  void set_null() {
    uint32_t meta = 1;
    memcpy(data_ - 4, &meta, 4);
  }
  void set_not_null() {
    uint32_t meta = 0;
    memcpy(data_ - 4, &meta, 4);
  }
  TupleCell *get_cell() {return cell_;}
  void to_string(std::ostream &os, RecordFileHandler *handler) {cell_->to_string(os, handler);}
  bool is_null() { 
    uint32_t meta = *(uint32_t *)(data_ - 4);
    if (meta == 1) {
      return true;
    } else {
      return false;
    }
  }
  int compare(TulpeCellOut &other) {return cell_->compare(*(other.get_cell()));}

  const char *data() const
  {
    return data_;
  }

  int length() const { return length_; }

  AttrType attr_type() const
  {
    return attr_type_;
  }
private:
  TupleCell *cell_;
  AttrType attr_type_ = UNDEFINED;
  int length_ = -1;
  char *data_ = nullptr; // real data. no need to move to field_meta.offset
  char *nullable = nullptr;
};

class MemRowTuple {
public:
  MemRowTuple() = default;
  ~MemRowTuple() {
    for(auto cell: cells_) {
      if(cell) {
        delete cell;
        cell = nullptr;
      }
    }
    cells_.clear();
  }
  void add(TupleCell cell) {
    TulpeCellOut *new_cell = new TulpeCellOut(cell.data(), cell.length(), cell.attr_type());
    cells_.push_back(new_cell);
  }
  void add(const char *data, int length, AttrType type) {
    TulpeCellOut *new_cell = new TulpeCellOut(data, length, type);
    cells_.push_back(new_cell);
  }
  int length() {
    return cells_.size();
  }
  TulpeCellOut *get(int i) {
    return cells_[i];
  }
private:
  std::vector<TulpeCellOut *> cells_; 
};

class MemTuple {
public:
  MemTuple() = default;
  ~MemTuple() {
    for(auto row: row_tuples_) {
      if(row) {
        delete row;
        row = nullptr;
      }
    }
    row_tuples_.clear();
  }
  void add(MemRowTuple *row) {
    row_tuples_.push_back(row);
  }
  int length() {
    return row_tuples_.size();
  }
  MemRowTuple *get(int i) {
    return row_tuples_[i];
  }

  void sort(int start, const std::vector<Order> &orders);
private:
  std::vector<MemRowTuple *> row_tuples_;
};