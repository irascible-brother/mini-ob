/* Copyright (c) 2021 Xie Meiyi(xiemeiyi@hust.edu.cn) and OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by Wangyunlai on 2022/07/05.
//

#pragma once

#include <string.h>
#include "vector"
#include "storage/common/field.h"
#include "sql/expr/tuple_cell.h"
#include "common/log/log.h"
#include "sql/stmt/select_stmt.h"
class Tuple;
class TulpeCellOut;


class Expression
{
public: 
  Expression() = default;
  virtual ~Expression() = default;
  
  virtual RC get_value(const Tuple &tuple, TupleCell &cell) const = 0;
  virtual ExprType type() const = 0;
  void set_is_out(bool is_out) {
    is_out_ = is_out;
  }
  bool is_out() {
    return is_out_;
  }
private:
  bool is_out_ = false;

};

class FieldExpr : public Expression
{
public:
  FieldExpr() = default;
  FieldExpr(const Table *table, const FieldMeta *field) : field_(table, field)
  {}

  virtual ~FieldExpr() = default;

  ExprType type() const override
  {
    return ExprType::FIELD;
  }

  Field &field()
  {
    return field_;
  }

  const Field &field() const
  {
    return field_;
  }

  const char *table_name() const
  {
    return field_.table_name();
  }

  const char *field_name() const
  {
    return field_.field_name();
  }

  RC get_value(const Tuple &tuple, TupleCell &cell) const override;
private:
  Field field_;
};
class NoneExpr : public Expression {
public:
  NoneExpr() = default;
  ~NoneExpr() = default;
  ExprType type() const override
  {
    return ExprType::NONE;
  }
  RC get_value(const Tuple &tuple, TupleCell &cell) const override;
};

class ValueListExpr : public Expression {
public:
  ValueListExpr() = default;
  ValueListExpr(const ValueList &value_list)
  {
    for(int i = 0; i < value_list.value_num; i++) {
      Value *value = new Value();
      value->type = value_list.values[i].type;
      if (value_list.values[i].type == CHARS) {
        value->data = malloc(sizeof(char) * (4 +strlen((const char *)value_list.values[i].data) + 1));
        memcpy((char*)value->data + 4, value_list.values[i].data, strlen((const char *)value_list.values[i].data) + 1);
      } else if(value_list.values[i].type == NULLS) {
        value->data = malloc(sizeof(char) * 8);
      } else {
        value->data = malloc(sizeof(char) * 8);
        memcpy((char*)value->data + 4, value_list.values[i].data, 4);
      }
      uint32_t meta = 0;
      if(value_list.values[i].type == NULLS) {
        meta = 1;
      }
      memcpy((char*)value->data, &meta, 4);
      value->data = (char *)value->data + 4;
      value_.push_back(value);
      set_tuple();
    }
  }
  
  ValueListExpr(const Value &va) {
    Value *value = new Value();
    value->type = va.type;
    
    if (va.type == CHARS) {
      value->data = malloc(sizeof(char) * (4 +strlen((const char *)va.data) + 1));
      memcpy((char*)value->data + 4, va.data, strlen((const char *)va.data) + 1);
    } else if(va.type == NULLS) {
      value->data = malloc(sizeof(char) * 8);

    } else {
      value->data = malloc(sizeof(char) * 8);
      memcpy((char*)value->data + 4, va.data, 4);
    }
    uint32_t meta = 0;
    if(va.type == NULLS) {
      meta = 1;
    }
    memcpy((char*)value->data, &meta, 4);
    value->data = (char*)value->data + 4;
    value_.push_back(value);
    set_tuple();
  }

  ~ValueListExpr() override {
    for(auto value : value_) {
      if(value->data) {
        value->data = (char *)value->data - 4;
        free(value->data);
        value->data = nullptr;
      }
      if(value) {
        delete value;
        value = nullptr;
      }
    }
    value_.clear();
  }

  ExprType type() const override
  {
    return ExprType::LIST;
  }
  void add(TulpeCellOut *cell) {
    Value *value = new Value();
    if(cell->is_null()) {
      value->type = NULLS;
    } else {
      value->type = cell->attr_type();
    }
    value->data = malloc(sizeof(char) * 8);
    memcpy((char*)value->data, cell->data() - 4, 8);
    value->data = (char *)value->data + 4;
    value_.push_back(value);
    set_tuple();
  }

  TupleCell get_i_value(int i) {
    cur_tuple_.set_data((char *)value_[i]->data);
    cur_tuple_.set_type(value_[i]->type);
    if (value_[i]->type == CHARS) {
      cur_tuple_.set_length(strlen((const char *)value_[i]->data));
    }
    return cur_tuple_;
  }

  Value get_zero_value() {
    if(value_.size() == 0) {
      Value *value = new Value();
      value->type = NULLS;
      value->data = malloc(sizeof(char) * 8);
      value->data = (char *)value->data + 4;
      value_.push_back(value);
      set_tuple();
    }
    return *(value_[0]);
  }
  void get_tuple_cell(TupleCell &cell) {
    cell = cur_tuple_;
    return;
  }
  void set_tuple_cell_type(const AttrType attr_type) {
    cur_tuple_.set_type(attr_type);
  }
  void set_tuple() {
    if(value_.size() == 0 || value_[0]->type == NULLS || !(value_[0]->data)) {
      cur_tuple_.set_type(NULLS);
    } else {
      uint32_t meta = *(uint32_t *)((char *)value_[0]->data - 4);
      if(meta & 1) {
        cur_tuple_.set_type(NULLS);
      } else {
        cur_tuple_.set_type(value_[0]->type);
        if(value_[0]->type == CHARS) {
          cur_tuple_.set_length(strlen((const char *)value_[0]->data));
        }
      }
    }
    cur_tuple_.set_data((char *)value_[0]->data);
  }
  int length() {
    return value_.size();
  }
  RC get_value(const Tuple &tuple, TupleCell &cell) const override;
private:
  std::vector<Value *> value_;
  TupleCell cur_tuple_;
};
class QueryExpr : public Expression
{
public:
  QueryExpr() = default;
  QueryExpr(Stmt *selects) : selects_(selects) 
  {}
  ~QueryExpr() {
    if(selects_) {
      delete selects_;
    }
    selects_ = nullptr;
  };
  Stmt *get_stmt() {
    return selects_;
  }
  ExprType type() const override
  {
    return ExprType::QUERY;
  }
  RC get_value(const Tuple &tuple, TupleCell &cell) const override;
private:
  Stmt *selects_;
};


class ValueExpr : public Expression
{
public:
  ValueExpr() = default;
  ValueExpr(const Value &value) : tuple_cell_(value.type, (char *)value.data)
  {
    if (value.type == CHARS) {
      tuple_cell_.set_length(strlen((const char *)value.data));
    }
  }

  virtual ~ValueExpr() = default;

  RC get_value(const Tuple &tuple, TupleCell & cell) const override;
  ExprType type() const override
  {
    return ExprType::VALUE;
  }

  void get_tuple_cell(TupleCell &cell) const {
    cell = tuple_cell_;
  }
  
  void set_tuple_cell_type(const AttrType attr_type) {
    tuple_cell_.set_type(attr_type);
  }

private:
  TupleCell tuple_cell_;
};
