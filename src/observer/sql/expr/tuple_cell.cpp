/* Copyright (c) 2021 Xie Meiyi(xiemeiyi@hust.edu.cn) and OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by WangYunlai on 2022/07/05.
//

#include "sql/expr/tuple_cell.h"
#include "storage/common/field.h"
#include "storage/record/record_manager.h"
#include "common/log/log.h"
#include "util/comparator.h"
#include "util/util.h"
#include <string>
#include <sstream>
#include <cmath>
#include <algorithm>

int get_int_from_char(char in[]);
float get_float_from_char(char in[]);


void TupleCell::to_string(std::ostream &os, RecordFileHandler *handler) const
{
  uint32_t meta = *(uint32_t *)(data_ - 4);
  if (meta == 1) {
    os << "NULL";
    return;
  }
  switch (attr_type_) {
  case INTS: {
    os << *(int *)data_;
  } break;
  case FLOATS: {
    float v = *(float *)data_;
    os << double2string(v);
  } break;
  case CHARS: {
    for (int i = 0; i < length_; i++) {
      if (data_[i] == '\0') {
        break;
      }
      os << data_[i];
    }
  } break;
  case DATES: {
    uint32_t year = *((uint32_t*)data_) >> 12;
    uint32_t mouth = (*((uint32_t*)data_) >> 6) & 63;
    uint32_t day = *((uint32_t*)data_) & 63;

    char date[] = "0000-00-00\0";
    date[9] = day % 10 + '0';
    date[8] = day / 10 + '0';
    date[6] = mouth % 10 + '0';
    date[5] = mouth / 10 + '0';

    date[0] = year / 1000 + '0';
    year %= 1000;
    date[1] = year / 100 + '0';
    year %= 100;
    date[2] = year / 10 + '0';
    date[3] = year % 10 + '0';

    for (unsigned int i = 0; i < strlen(date); i++) {
      if (date[i] == '\0') {
        break;
      }
      os << date[i];
    }
  } break;
  case TEXTS: {
    char text[4097];
    PageNum page_num = *(PageNum *)data_;
    handler->read_text_page(text, page_num);
    os << std::string(text);
  } break;
  default: {
    LOG_WARN("unsupported attr type: %d", attr_type_);
  } break;
  }
}

int TupleCell::compare(const TupleCell &other) const
{
  if (this->attr_type_ == other.attr_type_) {
    switch (this->attr_type_) {
    case INTS: return compare_int(this->data_, other.data_);
    case FLOATS: return compare_float(this->data_, other.data_);
    case CHARS: return compare_string(this->data_, this->length_, other.data_, other.length_);
    case DATES: return compare_date(this->data_, other.data_);
    default: {
      LOG_WARN("unsupported type: %d", this->attr_type_);
    }
    }
  } else if (this->attr_type_ == INTS && other.attr_type_ == FLOATS) {
    float this_data = *(int *)data_;
    return compare_float(&this_data, other.data_);
  } else if (this->attr_type_ == INTS && other.attr_type_ == CHARS) { // add by tong
    std::string str = other.data_;
    std::istringstream str_stream(str);
    float other_data_f = 0.0f;
    str_stream >> other_data_f;
    int other_data =  round(other_data_f);
    return compare_int(data_, &other_data);
  } else if (this->attr_type_ == FLOATS && other.attr_type_ == INTS) {
    float other_data = *(int *)other.data_;
    return compare_float(data_, &other_data);
  } else if (this->attr_type_ == CHARS && other.attr_type_ == INTS) {
    int this_data = get_int_from_char(data_);
    return compare_int(&this_data, other.data_);
  } else if (this->attr_type_ == FLOATS && other.attr_type_ == CHARS) { 
    float other_data = get_float_from_char(other.data_);
    return compare_float(data_, &other_data);
  } else if (this->attr_type_ == CHARS && other.attr_type_ == FLOATS) {
    float this_data = get_float_from_char(data_);
    return compare_float(&this_data, other.data_);
  } // end
  LOG_WARN("not supported");
  return -1; // TODO return rc?
}

// add by tong
int TupleCell::get_int_from_char(char in[]){
   int i = 0;
   int sum = 0;
   while ((in[i] != '\0') && (in[i] >= '0') && (in[i] <= '9')) {
      sum = sum * 10 + (in[i] - '0');
      i += 1;
   }
   return sum;
}
float TupleCell::get_float_from_char(char in[]){
   int i = 0;
   int flag = 0;
   float sum = 0.0;
   float after = 0.0;
   float after_b = 1;
   while ((in[i] >= '0') && (in[i] <= '9')){
      sum = sum * 10.0 + (in[i] - '0');
      i += 1; 
   }
   if (!((in[i] >= '0') && (in[i] <= '9')) && !(in[i] == '.')) {
      return (sum+after);
   } else if ((in[i] == '.') && (flag == 0)) {
      i += 1;
      flag = 1;
      while ((in[i] >= '0') && (in[i] <= '9')){
         after = after + 0.1 * (in[i] - '0') * after_b;
         after_b = 0.1 * after_b;
         i += 1;
      }
      return (sum+after);
   }
   return (sum+after);
}
// end

void MemTuple::sort(int start, const std::vector<Order> &orders) {
  int order_num = orders.size();
  auto cmp = [order_num, &orders, start] (MemRowTuple *lhs, MemRowTuple *rhs) {
    for (int i = order_num - 1 + start; i >= start; i--) {
      TulpeCellOut *left_tco = lhs->get(i);
      TupleCell *left_tc = left_tco->get_cell();
      TulpeCellOut *right_tco = rhs->get(i);
      TupleCell *right_tc = right_tco->get_cell();
      if (left_tc->is_null()) return orders[i].asc;
      if (right_tc->is_null()) return !orders[i].asc;
      const int compare = left_tc->compare(*right_tc);
      if (compare != 0) return (orders[i].asc && compare < 0) || (!orders[i].asc && compare > 0);
    }
    return false;
  };
  std::sort(row_tuples_.begin(), row_tuples_.end(), cmp);
}
