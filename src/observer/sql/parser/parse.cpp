/* Copyright (c) 2021 Xie Meiyi(xiemeiyi@hust.edu.cn) and OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by Meiyi 
//

#include <mutex>
#include "sql/parser/parse.h"
#include "rc.h"
#include "common/log/log.h"

RC parse(char *st, Query *sqln);

#ifdef __cplusplus
extern "C" {
#endif  // __cplusplus
void relation_attr_init(RelAttr *relation_attr, const char *relation_name, const char *attribute_name, bool is_agg, Aggregator agg, FIELDTYPE field_type)
{
  relation_attr->field_type = field_type;
  if (relation_name != nullptr) {
    relation_attr->relation_name = strdup(relation_name);
    free(const_cast<char *>(relation_name));
  } else {
    relation_attr->relation_name = nullptr;
  }
  if(is_agg) {
    relation_attr->is_agg = true;
    relation_attr->agg = agg;
  } else {
    relation_attr->is_agg = false;
  }
  relation_attr->attribute_name = strdup(attribute_name);
  free(const_cast<char *>(attribute_name));
}

void list_append_value(ValueList *list, Value *left_value) {
  list->values[list->value_num++] = *left_value;
}

void print_func(size_t i) {
  LOG_WARN("==============%d", i);
}

void print_add(void *i) {
  LOG_WARN("=----------------%p", i);
}
void relation_aggr_init(RelAggr *relation_agg, const char *relation_name, const char *attribute_name, const Aggregator aggregator_type) {
  if (relation_name != nullptr) {
    relation_agg->relation_name = strdup(relation_name);
  } else {
    relation_agg->relation_name = nullptr;
  }
  relation_agg->attribute_name = strdup(attribute_name);
  relation_agg->aggregator_type = aggregator_type;
}

void relation_attr_destroy(RelAttr *relation_attr)
{
  free(relation_attr->relation_name);
  free(relation_attr->attribute_name);
  relation_attr->relation_name = nullptr;
  relation_attr->attribute_name = nullptr;
}

void relation_aggr_destroy(RelAggr *relation_aggr) {
  free(relation_aggr->relation_name);
  free(relation_aggr->attribute_name);
  relation_aggr->relation_name = nullptr;
  relation_aggr->attribute_name = nullptr;
}


void value_init_integer(Value *value, int v)
{
  value->type = INTS;
  value->data = malloc(sizeof(v));
  memcpy(value->data, &v, sizeof(v));
}
void value_init_float(Value *value, float v)
{
  value->type = FLOATS;
  value->data = malloc(sizeof(v));
  memcpy(value->data, &v, sizeof(v));
}
void value_init_string(Value *value, const char *v)
{
  value->type = CHARS;
  value->data = strdup(v);
  free(const_cast<char *>(v));
}
void value_init_null(Value *value)
{
  value->type = NULLS;
}
void value_destroy(Value *value)
{
  if (value->type != NULLS) free(value->data);
  value->type = UNDEFINED;
  value->data = nullptr;
}
void set_condition_init(SetCondition *condition,RelAttr *left_attr, ExprType type, Value *value, Selects *selects) {
  condition->type = type;
  condition->left_field = *left_attr;
  if(type == ExprType::VALUE) {
    condition->right_value = *value;
  } else if(type == ExprType::QUERY) {
    condition->right_subquery = selects;
  }
}

void having_condition_init(HavingCondition *condition, ExprType left_type, RelAttr *left_attr, Value * left_value, Aggregator agg_type) {
  condition->left_type = left_type;
  if(condition->left_type == FIELD) {
    // condition->left_field = *left_attr; 
  } else if(condition->left_type == AGGR) {
    // condition->left_field = *left_attr; 
    condition->left_agg_type = agg_type;
  } else if(condition->left_type == VALUE) {
    condition->left_value = *left_value;
  }
}
void having_condition_append(HavingCondition *condition, ExprType right_type, RelAttr *right_attr, Value * right_value, Aggregator agg_type) {
  condition->right_type = right_type;
  if(condition->right_type == FIELD) {
    // condition->right_field = *right_attr; 
  } else if(condition->right_type == AGGR) {
    // condition->right_field = *right_attr; 
    condition->right_agg_type = agg_type;
  } else if(condition->right_type == VALUE) {
    condition->right_value = *right_value;
  }
}
void having_condition_append_comp(HavingCondition *condition, CompOp comp) {
  condition->comp = comp;
}
void selects_append_having_condition(Selects *select, HavingCondition *condition, bool is_having) {
  select->having_condition = *condition;
  select->is_having = is_having;
}
void condition_init(Condition *condition, CompOp comp,
                      ExprType left_type, RelAttr *left_attr, Value *left_value, Selects *left_selects, ValueList *left_list,
                      ExprType right_type, RelAttr *right_attr, Value *right_value, Selects *right_selects, ValueList *right_list, bool is_and)
{
  condition->comp = comp;
  condition->left_type = left_type;
  condition->is_and = is_and;
  if (left_type == ExprType::FIELD)
  {
    condition->left_attr = *left_attr;
  }
  else if (left_type == ExprType::VALUE)
  {
    condition->left_value = *left_value;
  } else if (left_type == ExprType::QUERY)
  {
    // Selects *new_left_selects =  (Selects *)malloc(sizeof(Selects));
    // memcpy(new_left_selects, left_selects, sizeof(Selects));
    condition->left_subquery = left_selects;
  } else if(left_type == ExprType::LIST) {
    memcpy(&(condition->left_value_list), right_list,sizeof(ValueList));

  }

  condition->right_type = right_type;
  if (right_type == ExprType::FIELD)
  {
    condition->right_attr = *right_attr;
  }
  else if (right_type == ExprType::VALUE)
  {
    condition->right_value = *right_value;
  }
  else if (right_type == ExprType::QUERY) 
  {
    // Selects *new_left_selects = new Selects(*right_selects);
    // Selects *new_left_selects = (Selects *)malloc(sizeof(Selects));
    // memcpy(new_left_selects, right_selects, sizeof(Selects));
    condition->right_subquery = right_selects;
  } else if(right_type == ExprType::LIST) {
    memcpy(&(condition->right_value_list), right_list, sizeof(ValueList));
  }
}
void set_condition_destroy(SetCondition *condition) {
  relation_attr_destroy(&condition->left_field);
  if( condition->type == ExprType::VALUE ) {
    value_destroy(&(condition->right_value));
  } else if(condition->type == ExprType::QUERY) {
    selects_destroy(condition->right_subquery);
    free(condition->right_subquery);
    condition->right_subquery = nullptr;
  }
}
void condition_destroy(Condition *condition)
  {
    if (condition->left_type == ExprType::FIELD)
    {
      relation_attr_destroy(&condition->left_attr);
    }
    else if (condition->left_type == ExprType::VALUE)
    {
      value_destroy(&condition->left_value);
    }
    else if(condition->left_type == ExprType::QUERY) {
      selects_destroy(condition->left_subquery);
      free(condition->left_subquery);
      condition->left_subquery = nullptr;
    } else if(condition->left_type == ExprType::LIST) {
      for(int i = 0; i < condition->left_value_list.value_num; i++) {
        value_destroy(&(condition->left_value_list.values[i]));
      }
      condition->left_value_list.value_num = 0;
    }
    if (condition->right_type == ExprType::FIELD)
    {
      relation_attr_destroy(&condition->right_attr);
    }
    else if (condition->right_type == ExprType::VALUE)
    {
      value_destroy(&condition->right_value);
    }
    else if(condition->right_type == ExprType::QUERY) {
      selects_destroy(condition->right_subquery);
      free(condition->right_subquery);
      condition->right_subquery = nullptr;
    } else if(condition->right_type == ExprType::LIST) {
      for(int i = 0; i < condition->right_value_list.value_num; i++) {
        value_destroy(&(condition->right_value_list.values[i]));
      }
      condition->right_value_list.value_num = 0;
    }
  }

void attr_info_init(AttrInfo *attr_info, const char *name, AttrType type, size_t length, bool nullable)
{
  attr_info->name = strdup(name);
  attr_info->type = type;
  attr_info->length = length;
  attr_info->nullable = nullable;
}
void attr_info_destroy(AttrInfo *attr_info)
{
  free(attr_info->name);
  attr_info->name = nullptr;
}
void select_init(Selects **selects) {
  *selects = (Selects *)malloc(sizeof(Selects));
  memset(*selects, 0, sizeof(Selects));
  (*selects)->aggr_num = 0;
  selects_clear(selects);
}
void selects_init(Selects *selects, ...);
void selects_append_attribute(Selects *selects, RelAttr *rel_attr)
{
  selects->attributes[selects->attr_num++] = *rel_attr;
}

void selects_append_aggragation(Selects *selects, RelAggr *rel_agg) {
  selects->aggregations[selects->aggr_num++] = *rel_agg;
}

void selects_append_relation(Selects *selects, const char *relation_name)
{
  selects->relations[selects->relation_num++] = strdup(relation_name);
  free(const_cast<char *>(relation_name));
}

void selects_append_group(Selects *selects, RelAttr *group_rel) {
  selects->group_num++;
}
void selects_append_conditions(Selects *selects, Condition conditions[], size_t condition_num)
{
  assert(condition_num <= sizeof(selects->conditions) / sizeof(selects->conditions[0]));
  for (size_t i = 0; i < condition_num; i++) {
    selects->conditions[i] = conditions[i];
  }
  selects->condition_num = condition_num;
}

void selects_append_condition(Selects *selects, Condition *condition)
{
  // assert(condition_num <= sizeof(selects->conditions) / sizeof(selects->conditions[0]));
  selects->conditions[selects->condition_num++] = *condition;
  // selects->condition_num = condition_num;
}
void selects_append_groups(Selects *selects, RelAttr groups[], size_t group_num)
{
  assert(group_num <= sizeof(selects->groups) / sizeof(selects->groups[0]));
  for (size_t i = 0; i < group_num; i++)
  {
    selects->groups[i] = groups[i];
  }
  selects->group_num = group_num;
}

void selects_append_order(Selects *selects, Order *order)
{
  selects->orders[selects->order_num++] = *order;
}

void selects_set_agg(Selects *selects, bool agg)
{
  selects->agg = agg;
}

void having_condition_destroy(HavingCondition *having_condition) {
  if(having_condition->left_type == FIELD || having_condition->left_type == AGGR) {
    // relation_attr_destroy(&having_condition->left_field);
  } else if(having_condition->left_type == VALUE) {
    value_destroy(&having_condition->left_value);
  } 
  if(having_condition->right_type == FIELD || having_condition->right_type == AGGR) {
    // relation_attr_destroy(&having_condition->right_field);
  } else if(having_condition->right_type == VALUE) {
    value_destroy(&having_condition->right_value);
  } 
}
void selects_destroy(Selects *selects)
{
  for (size_t i = 0; i < selects->attr_num; i++) {
    relation_attr_destroy(&selects->attributes[i]);
  }
  selects->attr_num = 0;

  selects->group_num = 0;
  if(selects->is_having) {
    having_condition_destroy(&selects->having_condition);
  }
  for (size_t i = 0; i < selects->relation_num; i++) {
    free(selects->relations[i]);
    selects->relations[i] = NULL;
  }
  selects->relation_num = 0;

  for(size_t i = 0; i < selects->out_relations_num; i++) {
    free(selects->out_relations[i]);
    selects->out_relations[i] = NULL;
  }
  selects->out_relations_num = 0;
  for (size_t i = 0; i < selects->condition_num; i++) {
    condition_destroy(&selects->conditions[i]);
  }
  selects->condition_num = 0;
  for (size_t i = 0; i < selects->group_num; i++)
  {
    relation_attr_destroy(&selects->groups[i]);
  }
  for (size_t i = 0; i < selects->aggr_num; i++) {
    relation_aggr_destroy(&selects->aggregations[i]);
  }
  selects->group_num = 0;
  selects->order_num = 0;
  for (size_t i = 0; i < selects->table_alias_num; i++) {
    table_alias_destroy(selects, i);
  }
  selects->table_alias_num = 0;
  for (size_t i = 0; i < selects->column_alias_num; i++) {
    column_alias_destroy(selects, i);
  }
  selects->column_alias_num = 0;
}

void inserts_init(Inserts *inserts, const char *relation_name, Value values[], size_t value_num)
{
  assert(value_num <= sizeof(inserts->values) / sizeof(inserts->values[0]));

  inserts->relation_name = strdup(relation_name);
  free(const_cast<char *>(relation_name));
  for (size_t i = 0; i < value_num; i++) {
    inserts->values[i] = values[i];
  }
  inserts->value_num = value_num;
}
void inserts_destroy(Inserts *inserts)
{
  free(inserts->relation_name);
  inserts->relation_name = nullptr;

  for (size_t i = 0; i < inserts->value_num; i++) {
    value_destroy(&inserts->values[i]);
  }
  inserts->value_num = 0;
}

void deletes_init_relation(Deletes *deletes, const char *relation_name)
{
  deletes->relation_name = strdup(relation_name);
  free(const_cast<char *>(relation_name));
}

void deletes_set_conditions(Deletes *deletes, Condition conditions[], size_t condition_num)
{
  assert(condition_num <= sizeof(deletes->conditions) / sizeof(deletes->conditions[0]));
  for (size_t i = 0; i < condition_num; i++) {
    deletes->conditions[i] = conditions[i];
  }
  deletes->condition_num = condition_num;
}
void deletes_destroy(Deletes *deletes)
{
  for (size_t i = 0; i < deletes->condition_num; i++) {
    condition_destroy(&deletes->conditions[i]);
  }
  deletes->condition_num = 0;
  free(deletes->relation_name);
  deletes->relation_name = nullptr;
}

void updates_init(Updates *updates, const char *relation_name, SetCondition set_conditions[], size_t set_condition_num,
    Condition conditions[], size_t condition_num)
{
  updates->relation_name = strdup(relation_name);
  free(const_cast<char *>(relation_name));
  updates->condition_num = condition_num;
  updates->set_condition_num = set_condition_num;
  assert(condition_num <= sizeof(updates->conditions) / sizeof(updates->conditions[0]));
  for (size_t i = 0; i < condition_num; i++) {
    updates->conditions[i] = conditions[i];
  }
  assert(set_condition_num <= sizeof(updates->set_conditions) / sizeof(updates->set_conditions[0]));
  for (size_t i = 0; i < set_condition_num; i++) {
    updates->set_conditions[i] = set_conditions[i];
  }
}

void updates_destroy(Updates *updates)
{
  free(updates->relation_name);

  updates->relation_name = nullptr;

  for (size_t i = 0; i < updates->set_condition_num; i++) {
    set_condition_destroy(&updates->set_conditions[i]);
  }
  updates->set_condition_num = 0;

  for (size_t i = 0; i < updates->condition_num; i++) {
    condition_destroy(&updates->conditions[i]);
  }
  updates->condition_num = 0;
}

void create_table_append_attribute(CreateTable *create_table, AttrInfo *attr_info)
{
  create_table->attributes[create_table->attribute_count++] = *attr_info;
}
void order_init(Order *order, bool asc)
{
  order->asc = asc;
}

void selects_clear(Selects **selects)
{
  // memset(selects, 0, sizeof(Selects));
  (*selects)->agg = false;
  (*selects)->attr_num = 0;
  (*selects)->condition_num = 0;
  (*selects)->group_num = 0;
  (*selects)->order_num = 0;
  (*selects)->out_relations_num = 0;
  (*selects)->relation_num = 0;
  (*selects)->subquery_num = 0;
}
void create_table_init_name(CreateTable *create_table, const char *relation_name)
{
  create_table->relation_name = strdup(relation_name);
  free(const_cast<char *>(relation_name));
}

void create_table_destroy(CreateTable *create_table)
{
  for (size_t i = 0; i < create_table->attribute_count; i++) {
    attr_info_destroy(&create_table->attributes[i]);
  }
  create_table->attribute_count = 0;
  free(create_table->relation_name);
  create_table->relation_name = nullptr;
}

void drop_table_init(DropTable *drop_table, const char *relation_name)
{
  drop_table->relation_name = strdup(relation_name);
  free(const_cast<char *>(relation_name));

}

void drop_table_destroy(DropTable *drop_table)
{
  free(drop_table->relation_name);
  drop_table->relation_name = nullptr;
}

void create_index_init(
    CreateIndex *create_index, const char *index_name, const char *relation_name, bool is_unique)
{
  create_index->index_name = strdup(index_name);
  create_index->relation_name = strdup(relation_name);
  create_index->is_unique = is_unique;
  free(const_cast<char *>(index_name));
  free(const_cast<char *>(relation_name));

}

void index_append_attribute(CreateIndex *create_index, const char *attribute_name) {
  create_index->attributes_name[create_index->attribute_num++] = strdup(attribute_name);
  free(const_cast<char *>(attribute_name));
}

void create_index_destroy(CreateIndex *create_index)
{
  free(create_index->index_name);
  free(create_index->relation_name);
  create_index->index_name = nullptr;
  create_index->relation_name = nullptr;
  for (int i = 0; i < create_index->attribute_num; i++) {
    free(create_index->attributes_name[i]);
    create_index->attributes_name[i] = nullptr;
  }
}

void drop_index_init(DropIndex *drop_index, const char *index_name)
{
  drop_index->index_name = strdup(index_name);
  free(const_cast<char *>(index_name));
}

void drop_index_destroy(DropIndex *drop_index)
{
  free((char *)drop_index->index_name);
  drop_index->index_name = nullptr;
}

void desc_table_init(DescTable *desc_table, const char *relation_name)
{
  desc_table->relation_name = strdup(relation_name);
  free(const_cast<char *>(relation_name));
}

void desc_table_destroy(DescTable *desc_table)
{
  free((char *)desc_table->relation_name);
  desc_table->relation_name = nullptr;
}

void show_index_init(ShowIndex *show_index, const char *relation_name)
{
  show_index->relation_name = strdup(relation_name);
  free(const_cast<char *>(relation_name));
}

void show_index_destroy(ShowIndex *show_index)
{
  free((char *)show_index->relation_name);
  show_index->relation_name = nullptr;
}

void load_data_init(LoadData *load_data, const char *relation_name, const char *file_name)
{
  load_data->relation_name = strdup(relation_name);

  if (file_name[0] == '\'' || file_name[0] == '\"') {
    file_name++;
  }
  char *dup_file_name = strdup(file_name);
  int len = strlen(dup_file_name);
  if (dup_file_name[len - 1] == '\'' || dup_file_name[len - 1] == '\"') {
    dup_file_name[len - 1] = 0;
  }
  load_data->file_name = dup_file_name;
}

void load_data_destroy(LoadData *load_data)
{
  free((char *)load_data->relation_name);
  free((char *)load_data->file_name);
  load_data->relation_name = nullptr;
  load_data->file_name = nullptr;
}

void query_init(Query *query)
{
  query->flag = SCF_ERROR;
  memset(&query->sstr, 0, sizeof(query->sstr));
}

Query *query_create()
{
  Query *query = (Query *)malloc(sizeof(Query));
  if (nullptr == query) {
    LOG_ERROR("Failed to alloc memroy for query. size=%ld", sizeof(Query));
    return nullptr;
  }

  query_init(query);
  return query;
}

void query_reset(Query *query)
{
  switch (query->flag) {
    case SCF_SELECT: {
      selects_destroy(&query->sstr.selection);
    } break;
    case SCF_INSERT: {
      inserts_destroy(&query->sstr.insertion);
    } break;
    case SCF_DELETE: {
      deletes_destroy(&query->sstr.deletion);
    } break;
    case SCF_UPDATE: {
      updates_destroy(&query->sstr.update);
    } break;
    case SCF_CREATE_TABLE: {
      create_table_destroy(&query->sstr.create_table);
    } break;
    case SCF_DROP_TABLE: {
      drop_table_destroy(&query->sstr.drop_table);
    } break;
    case SCF_CREATE_INDEX: {
      create_index_destroy(&query->sstr.create_index);
    } break;
    case SCF_DROP_INDEX: {
      drop_index_destroy(&query->sstr.drop_index);
    } break;
    case SCF_SYNC: {

    } break;
    case SCF_SHOW_TABLES:
      break;

    case SCF_DESC_TABLE: {
      desc_table_destroy(&query->sstr.desc_table);
    } break;

    case SCF_LOAD_DATA: {
      load_data_destroy(&query->sstr.load_data);
    } break;
    case SCF_SHOW_INDEX: {
      show_index_destroy(&query->sstr.show_index);
    } break;
    case SCF_CLOG_SYNC:
    case SCF_BEGIN:
    case SCF_COMMIT:
    case SCF_ROLLBACK:
    case SCF_HELP:
    case SCF_EXIT:
    case SCF_ERROR:
      break;
  }
}

void query_destroy(Query *query)
{
  query_reset(query);
  free(query);
}

void selects_append_column_alias(Selects *selects, char *table, char *from, char *to)
{
  if (table) selects->column_alias_table[selects->column_alias_num] = strdup(table);
  selects->column_alias_from[selects->column_alias_num] = strdup(from);
  selects->column_alias_to[selects->column_alias_num++] = strdup(to);
  free(to);
}

void column_alias_destroy(Selects *selects, size_t i)
{
  if (selects->column_alias_table[i])
  {
    free(selects->column_alias_table[i]);
    selects->column_alias_table[i] = nullptr;
  }
  free(selects->column_alias_from[i]);
  selects->column_alias_from[i] = nullptr;
  free(selects->column_alias_to[i]);
  selects->column_alias_to[i] = nullptr;
}

void selects_append_table_alias(Selects *selects, char *from, char *to)
{
  selects->table_alias_from[selects->table_alias_num] = strdup(from);
  selects->table_alias_to[selects->table_alias_num++] = strdup(to);
  free(to);
}

void table_alias_destroy(Selects *selects, size_t i)
{
  free(selects->table_alias_from[i]);
  selects->table_alias_from[i] = nullptr;
  free(selects->table_alias_to[i]);
  selects->table_alias_to[i] = nullptr;
}

#ifdef __cplusplus
}  // extern "C"
#endif  // __cplusplus

////////////////////////////////////////////////////////////////////////////////

extern "C" int sql_parse(const char *st, Query *sqls);

RC parse(const char *st, Query *sqln)
{
  sql_parse(st, sqln);

  if (sqln->flag == SCF_ERROR)
    return SQL_SYNTAX;
  else
    return SUCCESS;
}