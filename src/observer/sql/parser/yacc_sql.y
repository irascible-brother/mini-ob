
%{

#include "sql/parser/parse_defs.h"
#include "sql/parser/yacc_sql.tab.h"
#include "sql/parser/lex.yy.h"
// #include "common/log/log.h" // 包含C++中的头文件

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct ParserContext {
  Query * ssql;
  size_t select_length;
  size_t condition_length;
  size_t from_length;
  size_t value_length;
  size_t value_start;
  Value values[MAX_NUM];
  Condition conditions[MAX_NUM];
  SetCondition set_conditions[MAX_NUM];
  size_t set_condition_length;
  RelAttr groups[MAX_NUM];
  size_t group_length;
  char id[MAX_NUM];
  bool is_and[MAX_NUM];
  size_t cur_condition;
  Aggregator cur_agg;
  bool is_right;
  HavingCondition having_condition;
  bool is_having;
  // 子查询数组
  Selects* selects[MAX_SELECT_NUM];
} ParserContext;

//获取子串
char *substr(const char *s,int n1,int n2)/*从s中提取下标为n1~n2的字符组成一个新字符串，然后返回这个新串的首地址*/
{
  char *sp = malloc(sizeof(char) * (n2 - n1 + 2));
  int i, j = 0;
  for (i = n1; i <= n2; i++) {
    sp[j++] = s[i];
  }
  sp[j] = 0;
  free((char *)s);
  return sp;
}

void yyerror(yyscan_t scanner, const char *str)
{
  ParserContext *context = (ParserContext *)(yyget_extra(scanner));
  query_reset(context->ssql);
  context->ssql->flag = SCF_ERROR;
  context->condition_length = 0;
  context->cur_condition = 0;
  context->from_length = 0;
  context->value_start = 0;
  context->select_length = 0;
  context->value_length = 0;
  context->ssql->sstr.insertion.value_num = 0;
  printf("parse sql failed. error=%s", str);
}

ParserContext *get_context(yyscan_t scanner)
{
  return (ParserContext *)yyget_extra(scanner);
}

#define CONTEXT get_context(scanner)

%}

%define api.pure full
%lex-param { yyscan_t scanner }
%parse-param { void *scanner }

//标识tokens
%token  SEMICOLON
        CREATE
        DROP
        TABLE
        TABLES
        INDEX
        SELECT
        DESC
        SHOW
        SYNC
        INSERT
        DELETE
        UPDATE
        LBRACE
        RBRACE
        COMMA
        TRX_BEGIN
        TRX_COMMIT
        TRX_ROLLBACK
        INT_T
        STRING_T
        FLOAT_T
		TEXT_T
        HELP
        EXIT
        DOT //QUOTE
        INTO
        VALUES
        FROM
        WHERE
        AND
		OR
        SET
        ON
        LOAD
        DATA
		INNER
		JOIN
        INFILE
		DATE_T
		CNT
		MAX
		MIN
		AVG
		SUM
		UNIQUE
		NULLABLE
		NULL_V
		LIKE
		IN
		HAVING
		AS
		EXIST
		NOT
		ORDER
		GROUP
		BY
		ASC
		CIS
        EQ
        LT
        GT
        LE
        GE
        NE

%union {
  struct _Attr *attr;
  struct _Condition *condition1;
  struct _SetCondition *condition2;
  struct _Value *value1;
  char *string;
  int number;
  float floats;
	char *position;
}

%token <number> NUMBER
%token <floats> FLOAT 
%token <string> ID
%token <string> PATH
%token <string> SSS
%token <string> STAR
%token <string> STRING_V
//非终结符

%type <number> type;
%type <condition1> condition;
%type <value1> value;
%type <number> number;
%type <number> comOp;

%type <string> alias;
%%

commands:		//commands or sqls. parser starts here.
    /* empty */
    | commands command
    ;

command:
	  select_table  
	| insert
	| update
	| delete
	| create_table
	| drop_table
	| show_tables
	| desc_table
	| show_index
	| create_index	
	| drop_index
	| sync
	| begin
	| commit
	| rollback
	| load_data
	| help
	| exit
    ;

exit:			
    EXIT SEMICOLON {
        CONTEXT->ssql->flag=SCF_EXIT;//"exit";
    };

help:
    HELP SEMICOLON {
        CONTEXT->ssql->flag=SCF_HELP;//"help";
    };

sync:
    SYNC SEMICOLON {
      CONTEXT->ssql->flag = SCF_SYNC;
    }
    ;

begin:
    TRX_BEGIN SEMICOLON {
      CONTEXT->ssql->flag = SCF_BEGIN;
    }
    ;

commit:
    TRX_COMMIT SEMICOLON {
      CONTEXT->ssql->flag = SCF_COMMIT;
    }
    ;

rollback:
    TRX_ROLLBACK SEMICOLON {
      CONTEXT->ssql->flag = SCF_ROLLBACK;
    }
    ;

drop_table:		/*drop table 语句的语法解析树*/
    DROP TABLE ID SEMICOLON {
        CONTEXT->ssql->flag = SCF_DROP_TABLE;//"drop_table";
        drop_table_init(&CONTEXT->ssql->sstr.drop_table, $3);
    };

show_tables:
    SHOW TABLES SEMICOLON {
      CONTEXT->ssql->flag = SCF_SHOW_TABLES;
    }
    ;

desc_table:
    DESC ID SEMICOLON {
      CONTEXT->ssql->flag = SCF_DESC_TABLE;
      desc_table_init(&CONTEXT->ssql->sstr.desc_table, $2);
    }
    ;

show_index:
    SHOW INDEX FROM ID SEMICOLON {
      CONTEXT->ssql->flag = SCF_SHOW_INDEX;
      show_index_init(&CONTEXT->ssql->sstr.show_index, $4);
    }
	;

create_index:		/*create index 语句的语法解析树*/
    CREATE INDEX ID ON ID LBRACE index_attr RBRACE SEMICOLON 
		{
			CONTEXT->ssql->flag = SCF_CREATE_INDEX;//"create_index";
			create_index_init(&CONTEXT->ssql->sstr.create_index, $3, $5, false);
		}
	|CREATE UNIQUE INDEX ID ON ID LBRACE index_attr RBRACE SEMICOLON
		{
			CONTEXT->ssql->flag = SCF_CREATE_INDEX;//"create_index";
			create_index_init(&CONTEXT->ssql->sstr.create_index, $4, $6, true);
		}
    ;

index_attr:
	ID index_attr_list {
		index_append_attribute(&CONTEXT->ssql->sstr.create_index, $1);
	}
	;

index_attr_list:
	/* empty */
	| COMMA ID index_attr_list {
		index_append_attribute(&CONTEXT->ssql->sstr.create_index, $2);
	}
	;

drop_index:			/*drop index 语句的语法解析树*/
    DROP INDEX ID  SEMICOLON 
		{
			CONTEXT->ssql->flag=SCF_DROP_INDEX;//"drop_index";
			drop_index_init(&CONTEXT->ssql->sstr.drop_index, $3);
		}
    ;
create_table:		/*create table 语句的语法解析树*/
    CREATE TABLE ID LBRACE attr_def attr_def_list RBRACE SEMICOLON 
		{
			CONTEXT->ssql->flag=SCF_CREATE_TABLE;//"create_table";
			// CONTEXT->ssql->sstr.create_table.attribute_count = CONTEXT->value_length;
			create_table_init_name(&CONTEXT->ssql->sstr.create_table, $3);
			//临时变量清零	
			CONTEXT->value_length = 0;
		}
    ;
attr_def_list:
    /* empty */
    | COMMA attr_def attr_def_list {    }
    ;
    
attr_def:
    ID_get type LBRACE number RBRACE 
		{
			AttrInfo attribute;
			attr_info_init(&attribute, CONTEXT->id, $2, $4, false);
			create_table_append_attribute(&CONTEXT->ssql->sstr.create_table, &attribute);
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].name =(char*)malloc(sizeof(char));
			// strcpy(CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].name, CONTEXT->id); 
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].type = $2;  
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].length = $4;
			CONTEXT->value_length++;
		}
    |ID_get type
		{
			AttrInfo attribute;
			attr_info_init(&attribute, CONTEXT->id, $2, 4, false);
			create_table_append_attribute(&CONTEXT->ssql->sstr.create_table, &attribute);
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].name=(char*)malloc(sizeof(char));
			// strcpy(CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].name, CONTEXT->id); 
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].type=$2;  
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].length=4; // default attribute length
			CONTEXT->value_length++;
		}
    |ID_get type LBRACE number RBRACE NULLABLE
		{
			AttrInfo attribute;
			attr_info_init(&attribute, CONTEXT->id, $2, $4, true);
			create_table_append_attribute(&CONTEXT->ssql->sstr.create_table, &attribute);
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].name =(char*)malloc(sizeof(char));
			// strcpy(CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].name, CONTEXT->id); 
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].type = $2;  
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].length = $4;
			CONTEXT->value_length++;
		}
    |ID_get type NULLABLE
		{
			AttrInfo attribute;
			attr_info_init(&attribute, CONTEXT->id, $2, 4, true);
			create_table_append_attribute(&CONTEXT->ssql->sstr.create_table, &attribute);
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].name=(char*)malloc(sizeof(char));
			// strcpy(CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].name, CONTEXT->id); 
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].type=$2;  
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].length=4; // default attribute length
			CONTEXT->value_length++;
		}
    |ID_get type LBRACE number RBRACE NOT NULL_V
		{
			AttrInfo attribute;
			attr_info_init(&attribute, CONTEXT->id, $2, $4, false);
			create_table_append_attribute(&CONTEXT->ssql->sstr.create_table, &attribute);
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].name =(char*)malloc(sizeof(char));
			// strcpy(CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].name, CONTEXT->id); 
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].type = $2;  
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].length = $4;
			CONTEXT->value_length++;
		}
    |ID_get type NOT NULL_V
		{
			AttrInfo attribute;
			attr_info_init(&attribute, CONTEXT->id, $2, 4, false);
			create_table_append_attribute(&CONTEXT->ssql->sstr.create_table, &attribute);
			CONTEXT->value_length++;
		}
    ;
number:
		NUMBER {$$ = $1;}
		;
type:
	INT_T { $$=INTS; }
	|STRING_T { $$=CHARS; }
	|FLOAT_T { $$=FLOATS; }
	|DATE_T { $$=DATES; }
	|TEXT_T { $$=TEXTS; }
	;
ID_get:
	ID 
	{
		char *temp=$1; 
		snprintf(CONTEXT->id, sizeof(CONTEXT->id), "%s", temp);
		free(temp);
	}
	;

	
insert:				/*insert   语句的语法解析树*/
    INSERT INTO ID VALUES LBRACE value value_list RBRACE tuple_list SEMICOLON 
		{
			// CONTEXT->values[CONTEXT->value_length++] = *$6;

			CONTEXT->ssql->flag=SCF_INSERT;//"insert";
			// CONTEXT->ssql->sstr.insertion.relation_name = $3;
			// CONTEXT->ssql->sstr.insertion.value_num = CONTEXT->value_length;
			// for(i = 0; i < CONTEXT->value_length; i++){
			// 	CONTEXT->ssql->sstr.insertion.values[i] = CONTEXT->values[i];
      // }
			inserts_init(&CONTEXT->ssql->sstr.insertion, $3, CONTEXT->values, CONTEXT->value_length);

      //临时变量清零
      CONTEXT->value_length=0;
    }

value_list:
    /* empty */
    | COMMA value value_list  { 
  		// CONTEXT->values[CONTEXT->value_length++] = *$2;
	  }
    ;
value:
    NUMBER{	
  		value_init_integer(&CONTEXT->values[CONTEXT->value_length++], $1);
		}
    |FLOAT{
  		value_init_float(&CONTEXT->values[CONTEXT->value_length++], $1);
		}
    |SSS {
		$1 = substr($1,1,strlen($1)-2);
  		value_init_string(&CONTEXT->values[CONTEXT->value_length++], $1);
		}
	|NULL_V {
		value_init_null(&CONTEXT->values[CONTEXT->value_length++]);
	}
    ;
    
delete:		/*  delete 语句的语法解析树*/
    DELETE FROM ID where SEMICOLON 
		{
			CONTEXT->ssql->flag = SCF_DELETE;//"delete";
			deletes_init_relation(&CONTEXT->ssql->sstr.deletion, $3);
			deletes_set_conditions(&CONTEXT->ssql->sstr.deletion, 
					CONTEXT->conditions, CONTEXT->condition_length);
			CONTEXT->condition_length = 0;	
    }
    ;
update:			/*  update 语句的语法解析树*/
    UPDATE ID SET eq_comp eq_comp_list where SEMICOLON
		{
			CONTEXT->ssql->flag = SCF_UPDATE;//"update";
			updates_init(&CONTEXT->ssql->sstr.update, $2, CONTEXT->set_conditions, CONTEXT->set_condition_length, 
					CONTEXT->conditions, CONTEXT->condition_length);
			CONTEXT->condition_length = 0;
		}
    ;
select_flag:
	SELECT
	{
		if(CONTEXT->select_length == 0) {
			CONTEXT->selects[0] = &(CONTEXT->ssql->sstr.selection);
		} else {
			select_init(&(CONTEXT->selects[CONTEXT->select_length]));
		}
	}
	;
select_table:
	select SEMICOLON
	{
		selects_append_conditions(&CONTEXT->ssql->sstr.selection, CONTEXT->conditions, CONTEXT->condition_length);
		selects_append_groups(&CONTEXT->ssql->sstr.selection, CONTEXT->groups, CONTEXT->group_length);
		selects_append_having_condition(&CONTEXT->ssql->sstr.selection, &CONTEXT->having_condition, CONTEXT->is_having);
	}
	;
select:				/*  select 语句的语法解析树*/
	select_flag select_attr FROM ID alias rel_list join_list where groups orders
	{
		if (strcmp($5, "")) {
			selects_append_table_alias(CONTEXT->selects[CONTEXT->select_length], $4, $5);
		}
		selects_append_relation(CONTEXT->selects[CONTEXT->select_length], $4);

		CONTEXT->ssql->flag=SCF_SELECT;//"select";

	}
 	;
join_flag:
	INNER JOIN {
		CONTEXT->is_and[CONTEXT->cur_condition]=true;
		CONTEXT->cur_condition++;
	}
	;
join_list:
	/* empty */
    | join_flag ID ON condition condition_list join_list
 		{
 			selects_append_relation(CONTEXT->selects[CONTEXT->select_length], $2);
 		}
 	;
alias:
	/* empty */ { $$ = ""; }
	| ID {
		$$ = $1;
	}	
	| AS ID {
		$$ = $2;
	}
	;
select_attr:
    STAR attr_list {  
			RelAttr attr;
			relation_attr_init(&attr, NULL, strdup("*"), false, 5, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		}
    | ID alias attr_list {
			if (strcmp($2, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, $1, $2);
			}

			RelAttr attr;
			relation_attr_init(&attr, NULL, $1, false, 5, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		}
  	| ID DOT ID alias attr_list {
			if (strcmp($4, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], $1, $3, $4);
			}

			RelAttr attr;
			relation_attr_init(&attr, $1, $3, false, 5, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		}
	| ID DOT STAR alias attr_list {
			if (strcmp($4, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], $1, $3, $4);
			}

			RelAttr attr;
			relation_attr_init(&attr, $1, strdup("*"), false, 5, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		}
	| MAX LBRACE ID RBRACE alias attr_list {
			if (strcmp($5, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, $3, $5);
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, $3, MAXIMUM);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, NULL, $3, true, MAXIMUM, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		}
	| MAX LBRACE ID DOT ID RBRACE alias attr_list {
			if (strcmp($7, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], $3, $5, $7);
			}
			
			RelAggr aggr;
			relation_aggr_init(&aggr, $3, $5, MAXIMUM);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, $3, $5, true, MAXIMUM, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		}
	| MIN LBRACE ID RBRACE alias attr_list {
			if (strcmp($5, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, $3, $5);
			}
				
			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, $3, MINIMUM);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, NULL, $3, true, MINIMUM, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
	| MIN LBRACE ID DOT ID RBRACE alias attr_list {
			if (strcmp($7, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], $3, $5, $7);
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, $3, $5, MINIMUM);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, $3, $5, true, MINIMUM, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
	| AVG LBRACE ID DOT ID RBRACE alias attr_list {
			if (strcmp($7, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], $3, $5, $7);
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, $3, $5, AVERAGE);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, $3, $5, true, AVERAGE, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
	| AVG LBRACE ID RBRACE alias attr_list {
			if (strcmp($5, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, $3, $5);
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, $3, AVERAGE);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, NULL, $3, true, AVERAGE, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
	| SUM LBRACE ID DOT ID RBRACE alias attr_list {
			if (strcmp($7, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], $3, $5, $7);
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, $3, $5, SUMMARY);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, $3, $5, true, SUMMARY, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
	| SUM LBRACE ID RBRACE alias attr_list {
			if (strcmp($5, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, $3, $5);
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, $3, SUMMARY);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, NULL, $3, true, SUMMARY, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
	| CNT LBRACE STAR RBRACE alias attr_list {
			if (strcmp($5, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, "*", $5);
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, "*", COUNT);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, NULL, strdup("*"), true, COUNT, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
	| CNT LBRACE number RBRACE alias attr_list {
			RelAggr aggr;
			char number[12];
			int i = 0;
			while ($3) {
				number[i++] = '0' + $3 % 10;
				$3 = $3 / 10;
			}
			number[i--] = '\0';
			for (int j = 0; j < i; j++, i--) {
				char c = number[j];
				number[j] = number[i];
				number[i] = c;
			}
			if (strcmp($5, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, number, $5);
			}

			relation_aggr_init(&aggr, NULL, number, COUNT);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
		}
	| CNT LBRACE ID RBRACE alias attr_list {
			if (strcmp($5, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, $3, $5);
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, $3, COUNT);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, NULL, $3, true, COUNT, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
	| CNT LBRACE ID DOT ID RBRACE alias attr_list {
			if (strcmp($7, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], $3, $5, $7);
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, $3, $5, COUNT);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, $3, $5, true, COUNT, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
    ;
attr_list:
    /* empty */
	| COMMA MAX LBRACE ID RBRACE alias attr_list {
			if (strcmp($6, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, $4, $6);
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, $4, MAXIMUM);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, NULL, $4, true, MAXIMUM, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
	| COMMA MAX LBRACE ID DOT ID RBRACE alias attr_list {
			if (strcmp($8, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], $4, $6, $8);
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, $4, $6, MAXIMUM);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, $4, $6, true, MAXIMUM, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
	| COMMA MIN LBRACE ID RBRACE alias attr_list {
			if (strcmp($6, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, $4, $6);
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, $4, MINIMUM);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, NULL, $4, true, MINIMUM, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
	| COMMA MIN LBRACE ID DOT ID RBRACE alias attr_list {
			if (strcmp($8, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], $4, $6, $8);
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, $4, $6, MINIMUM);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, $4, $6, true, MINIMUM, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
	| COMMA AVG LBRACE ID RBRACE alias attr_list {
			if (strcmp($6, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, $4, $6);
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, $4, AVERAGE);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, NULL, $4, true, AVERAGE, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
	| COMMA AVG LBRACE ID DOT ID RBRACE alias attr_list {
			if (strcmp($8, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], $4, $6, $8);
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, $4, $6, AVERAGE);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, $4, $6, true, AVERAGE, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		}
	| COMMA SUM LBRACE ID RBRACE alias attr_list {
			if (strcmp($6, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, $4, $6);
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, $4, SUMMARY);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, NULL, $4, true, SUMMARY, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
	| COMMA SUM LBRACE ID DOT ID RBRACE alias attr_list {
			if (strcmp($8, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], $4, $6, $8);
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, $4, $6, SUMMARY);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, $4, $6, true, SUMMARY, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
	| COMMA CNT LBRACE STAR RBRACE alias attr_list {
			if (strcmp($6, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, "*", $6);
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, "*", COUNT);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			
			RelAttr attr;
			relation_attr_init(&attr, NULL, strdup("*"), true, COUNT, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		}
	| COMMA CNT LBRACE number RBRACE alias attr_list {
			RelAggr aggr;
			char number[12];
			int i = 0;
			while ($4) {
				number[i++] = '0' + $4 % 10;
				$4 = $4 / 10;
			}
			number[i--] = '\0';
			for (int j = 0; j < i; j++, i--) {
				char c = number[j];
				number[j] = number[i];
				number[i] = c;
			}
			if (strcmp($6, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, number, $6);
			}

			relation_aggr_init(&aggr, NULL, number, COUNT);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
		}
	| COMMA CNT LBRACE ID RBRACE alias attr_list {
			if (strcmp($6, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, $4, $6);
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, $4, COUNT);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, NULL, $4, true, COUNT, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
	| COMMA CNT LBRACE ID DOT ID RBRACE alias attr_list {
			if (strcmp($8, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], $4, $6, $8);
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, $4, $6, COUNT);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, $4, $6, true, COUNT, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
    | COMMA ID alias attr_list {
			if (strcmp($3, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, $2, $3);
			}
			RelAttr attr;
			relation_attr_init(&attr, NULL, $2, false, 5, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
     	  // CONTEXT->ssql->sstr.selection.attributes[CONTEXT->select_length].relation_name = NULL;
        // CONTEXT->ssql->sstr.selection.attributes[CONTEXT->select_length++].attribute_name=$2;
      }
    | COMMA ID DOT ID alias attr_list {
			if (strcmp($5, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], $2, $4, $5);
			}
			RelAttr attr;
			relation_attr_init(&attr, $2, $4, false, 5, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
        // CONTEXT->ssql->sstr.selection.attributes[CONTEXT->select_length].attribute_name=$4;
        // CONTEXT->ssql->sstr.selection.attributes[CONTEXT->select_length++].relation_name=$2;
  	  }
	| COMMA ID DOT STAR alias attr_list {
			if (strcmp($5, "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], $2, $4, $5);
			}
			RelAttr attr;
			relation_attr_init(&attr, $2, strdup("*"), false, 5, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
  	  } 
  	;

rel_list:
    /* empty */
    | COMMA ID alias rel_list {	
			if (strcmp($3, "")) {
				selects_append_table_alias(CONTEXT->selects[CONTEXT->select_length], $2, $3);
			}	
			selects_append_relation(CONTEXT->selects[CONTEXT->select_length], $2);
		  }
    ;
order:
	ID {
		RelAttr attr;
		relation_attr_init(&attr, NULL, $1, false, 5, ORDERFIELD);
		selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		Order order;
		order_init(&order, true);
		selects_append_order(CONTEXT->selects[CONTEXT->select_length], &order);
	}
	| ID ASC {
		RelAttr attr;
		relation_attr_init(&attr, NULL, $1, false, 5, ORDERFIELD);
		selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		Order order;
		order_init(&order, true);
		selects_append_order(CONTEXT->selects[CONTEXT->select_length], &order);
	}
	| ID DESC {
		RelAttr attr;
		relation_attr_init(&attr, NULL, $1, false, 5, ORDERFIELD);
		selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		Order order;
		order_init(&order, false);
		selects_append_order(CONTEXT->selects[CONTEXT->select_length], &order);
	}
	| ID DOT ID {
		RelAttr attr;
		relation_attr_init(&attr, $1, $3, false, 5, ORDERFIELD);
		selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		Order order;
		order_init(&order, true);
		selects_append_order(CONTEXT->selects[CONTEXT->select_length], &order);
	}
	| ID DOT ID ASC {
		RelAttr attr;
		relation_attr_init(&attr, $1, $3, false, 5, ORDERFIELD);
		selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		Order order;
		order_init(&order, true);
		selects_append_order(CONTEXT->selects[CONTEXT->select_length], &order);
	}
	| ID DOT ID DESC {
		RelAttr attr;
		relation_attr_init(&attr, $1, $3, false, 5, ORDERFIELD);
		selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		Order order;
		order_init(&order, false);
		selects_append_order(CONTEXT->selects[CONTEXT->select_length], &order);
	}
	;
order_list:
	/* empty */
	| COMMA order order_list
	;
orders:
	/* empty */
	| ORDER BY order order_list
	;
group:
	ID {
		RelAttr attr;
		relation_attr_init(&attr, NULL, $1, false, 5, GROUPFIELD);
		selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		if(CONTEXT->select_length == 0) {
			CONTEXT->groups[CONTEXT->group_length++] = attr;
		} else {
			selects_append_group(CONTEXT->selects[CONTEXT->select_length], &attr);
		}
	}
	| ID DOT ID {
		RelAttr attr;
		relation_attr_init(&attr, $1, $3, false, 5, GROUPFIELD);
		selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		if(CONTEXT->select_length == 0) {
			CONTEXT->groups[CONTEXT->group_length++] = attr;
		} else {
			selects_append_group(CONTEXT->selects[CONTEXT->select_length], &attr);
		}
	}
	;

aggr_flag:
	MAX {
		CONTEXT->cur_agg = MAXIMUM;
	}
	| MIN {
		CONTEXT->cur_agg = MINIMUM;
	}
	| AVG {
		CONTEXT->cur_agg = AVERAGE;
	}
	| SUM {
		CONTEXT->cur_agg = SUMMARY;
	}
	| CNT {
		CONTEXT->cur_agg = COUNT;
	}
group_list:
	/* empty */
	| COMMA group group_list
	;
part:
	aggr_flag LBRACE ID RBRACE {
		RelAttr attr;
		relation_attr_init(&attr, NULL, $3, true, CONTEXT->cur_agg, HAVINGFIELD);
		selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		if(!CONTEXT->is_right) {
			HavingCondition condition;
			having_condition_init(&condition, 6, &attr, NULL, CONTEXT->cur_agg);
			CONTEXT->having_condition = condition;
		} else {
			having_condition_append(&CONTEXT->having_condition, 6, &attr, NULL, CONTEXT->cur_agg);
		}
		CONTEXT->is_right = !CONTEXT->is_right;
	}
	| aggr_flag LBRACE STAR RBRACE {
		RelAttr attr;
		relation_attr_init(&attr, NULL, strdup("*"), true, CONTEXT->cur_agg, HAVINGFIELD);
		selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		if(!CONTEXT->is_right) {
			HavingCondition condition;
			having_condition_init(&condition, 6, &attr, NULL, CONTEXT->cur_agg);
			CONTEXT->having_condition = condition;
		} else {
			having_condition_append(&CONTEXT->having_condition, 6, &attr, NULL, CONTEXT->cur_agg);
		}
		CONTEXT->is_right = !CONTEXT->is_right;
	}
	| aggr_flag LBRACE ID DOT ID RBRACE {
		RelAttr attr;
		relation_attr_init(&attr, $3, $5, true, CONTEXT->cur_agg, HAVINGFIELD);
		selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		if(!CONTEXT->is_right) {
			HavingCondition condition;
			having_condition_init(&condition, 6, &attr, NULL, CONTEXT->cur_agg);
			CONTEXT->having_condition = condition;
		} else {
			having_condition_append(&CONTEXT->having_condition, 6, &attr, NULL, CONTEXT->cur_agg);
		}
		CONTEXT->is_right = !CONTEXT->is_right;
	}
	| value {
		Value *value = &CONTEXT->values[CONTEXT->value_length - 1];
		if(!CONTEXT->is_right) {
			HavingCondition condition;
			having_condition_init(&condition, 2, NULL, value, CONTEXT->cur_agg);
			CONTEXT->having_condition = condition;
		} else {
			having_condition_append(&CONTEXT->having_condition, 2, NULL, value, CONTEXT->cur_agg);
		}
		CONTEXT->is_right = !CONTEXT->is_right;
	}
	;
having:
	/* empty */
	| HAVING part comOp part {
		CONTEXT->is_having = true;
		having_condition_append_comp(&CONTEXT->having_condition, $3);
	}
	;
groups:
	/* empty */
	| GROUP BY group group_list having
	;
where_flag:
	WHERE {
		CONTEXT->is_and[CONTEXT->cur_condition]=true;
		CONTEXT->cur_condition++;
	}
	;
where:
    /* empty */ 
    | where_flag condition condition_list {	
			}
    ;
and_or:
	AND {
		CONTEXT->is_and[CONTEXT->cur_condition]=true;
		CONTEXT->cur_condition++;
	}
	| OR {
		CONTEXT->is_and[CONTEXT->cur_condition]=false;
		CONTEXT->cur_condition++;
	}
	;
condition_list:
    /* empty */
    | and_or condition condition_list {
			}
    ;
subquery_start:
	LBRACE
	{
		CONTEXT->selects[CONTEXT->select_length++];
	}
	;
sublist_start:
	LBRACE
	{
		CONTEXT->value_start = CONTEXT->value_length;
	}
	;
condition:
    ID comOp value 
		{
			RelAttr left_attr;
			relation_attr_init(&left_attr, NULL, $1, false, 5, CONDITIONFIELD);

			Value *right_value = &CONTEXT->values[CONTEXT->value_length - 1];

			Condition condition;
			condition_init(&condition, $2, 1, &left_attr, NULL, NULL, NULL, 2, NULL, right_value, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
			CONTEXT->cur_condition--;
			if(CONTEXT->select_length == 0) {
				CONTEXT->conditions[CONTEXT->condition_length++] = condition;
			} else {
				selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
			}
		}
	|value comOp value 
		{
			Value *left_value = &CONTEXT->values[CONTEXT->value_length - 2];
			Value *right_value = &CONTEXT->values[CONTEXT->value_length - 1];
			
			Condition condition;
			condition_init(&condition, $2, 2, NULL, left_value, NULL, NULL, 2, NULL, right_value, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
			CONTEXT->cur_condition--;
			if(CONTEXT->select_length == 0) {
				CONTEXT->conditions[CONTEXT->condition_length++] = condition;
			} else {
				selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
			}
		}
	|ID comOp ID 
		{
			RelAttr left_attr;
			relation_attr_init(&left_attr, NULL, $1, false, 5, CONDITIONFIELD);
			RelAttr right_attr;
			relation_attr_init(&right_attr, NULL, $3, false, 5, CONDITIONFIELD);
			
			Condition condition;
			condition_init(&condition, $2, 1, &left_attr, NULL, NULL, NULL, 1, &right_attr, NULL, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
			CONTEXT->cur_condition--;
			
			if(CONTEXT->select_length == 0) {
				CONTEXT->conditions[CONTEXT->condition_length++] = condition;
			} else {
				selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
			}

		}
    |value comOp ID
		{
			Value *left_value = &CONTEXT->values[CONTEXT->value_length - 1];
			RelAttr right_attr;
			relation_attr_init(&right_attr, NULL, $3, false, 5, CONDITIONFIELD);
			
			Condition condition;
			condition_init(&condition, $2, 2, NULL, left_value, NULL, NULL, 1, &right_attr, NULL, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
			CONTEXT->cur_condition--;
			if(CONTEXT->select_length == 0) {
				CONTEXT->conditions[CONTEXT->condition_length++] = condition;
			} else {
				selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
			}
		}
    |ID DOT ID comOp value
		{
			RelAttr left_attr;
			relation_attr_init(&left_attr, $1, $3, false, 5, CONDITIONFIELD);
			Value *right_value = &CONTEXT->values[CONTEXT->value_length - 1];
			
			Condition condition;
			condition_init(&condition, $4, 1, &left_attr, NULL, NULL, NULL, 2, NULL, right_value ,NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
			CONTEXT->cur_condition--;
			if(CONTEXT->select_length == 0) {
				CONTEXT->conditions[CONTEXT->condition_length++] = condition;
			} else {
				selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
			}
							
    }
    |value comOp ID DOT ID
		{
			Value *left_value = &CONTEXT->values[CONTEXT->value_length - 1];

			RelAttr right_attr;
			relation_attr_init(&right_attr, $3, $5, false, 5, CONDITIONFIELD);
			
			Condition condition;
			condition_init(&condition, $2, 2, NULL, left_value, NULL, NULL, 1, &right_attr, NULL, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
			CONTEXT->cur_condition--;
			if(CONTEXT->select_length == 0) {
				CONTEXT->conditions[CONTEXT->condition_length++] = condition;
			} else {
				selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
			}
									
    }
    |ID DOT ID comOp ID DOT ID
		{
			RelAttr left_attr;
			relation_attr_init(&left_attr, $1, $3, false, 5, CONDITIONFIELD);
			RelAttr right_attr;
			relation_attr_init(&right_attr, $5, $7, false, 5, CONDITIONFIELD);
			
			Condition condition;
			condition_init(&condition, $4, 1, &left_attr, NULL, NULL, NULL, 1, &right_attr, NULL, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
			CONTEXT->cur_condition--;
			if(CONTEXT->select_length == 0) {
				CONTEXT->conditions[CONTEXT->condition_length++] = condition;
			} else {
				selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
			}
    }
	| comOp subquery_start select RBRACE
	{
		Value *left_value = NULL;
		
		Condition condition;
		condition_init(&condition, $1, 0, NULL, left_value, NULL, NULL, 3, NULL, NULL, CONTEXT->selects[CONTEXT->select_length], NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		CONTEXT->select_length--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
	| value comOp subquery_start select RBRACE
	{
		Value *left_value = &CONTEXT->values[CONTEXT->value_length - 1];
		
		Condition condition;
		condition_init(&condition, $2, 2, NULL, left_value, NULL, NULL, 3, NULL, NULL, CONTEXT->selects[CONTEXT->select_length], NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		CONTEXT->select_length--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
	| ID comOp sublist_start value value_list RBRACE {
		RelAttr left_attr;
		relation_attr_init(&left_attr, NULL, $1, false, 5, CONDITIONFIELD);
		ValueList right_value_list;
		right_value_list.value_num = 0;

		for(int i = CONTEXT->value_start; i < CONTEXT->value_length; i++) {
			Value *right_value = &CONTEXT->values[i];
			list_append_value(&right_value_list,right_value);
		}
		
		Condition condition;
		condition_init(&condition, $2, 1, &left_attr, NULL, NULL, NULL, 4, NULL, NULL, NULL, &right_value_list, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
	| ID DOT ID comOp sublist_start value value_list RBRACE {
		RelAttr left_attr;
		relation_attr_init(&left_attr, $1, $3, false, 5, CONDITIONFIELD);
		ValueList right_value_list;
		right_value_list.value_num = 0;

		for(int i = CONTEXT->value_start; i < CONTEXT->value_length; i++) {
			Value *right_value = &CONTEXT->values[i];
			list_append_value(&right_value_list,right_value);
		}
		
		Condition condition;
		condition_init(&condition, $4, 1, &left_attr, NULL, NULL, NULL, 4, NULL, NULL, NULL, &right_value_list, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
	| value comOp sublist_start value value_list RBRACE {
		Value *left_value = &CONTEXT->values[CONTEXT->value_start - 1];

		ValueList right_value_list;
		right_value_list.value_num = 0;

		for(int i = CONTEXT->value_start; i < CONTEXT->value_length; i++) {
			Value *right_value = &CONTEXT->values[i];
			list_append_value(&right_value_list,right_value);
		}
		
		Condition condition;
		condition_init(&condition, $2, 2, NULL, left_value, NULL, NULL, 4, NULL, NULL, NULL, &right_value_list, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
	| sublist_start value RBRACE comOp sublist_start value value_list RBRACE {
		Value *left_value = &CONTEXT->values[CONTEXT->value_start - 1];

		ValueList right_value_list;
		right_value_list.value_num = 0;

		for(int i = CONTEXT->value_start; i < CONTEXT->value_length; i++) {
			Value *right_value = &CONTEXT->values[i];
			list_append_value(&right_value_list,right_value);
		}
		
		Condition condition;
		condition_init(&condition, $4, 2, NULL, left_value, NULL, NULL, 4, NULL, NULL, NULL, &right_value_list, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
	| subquery_start select RBRACE comOp sublist_start value value_list RBRACE {

		ValueList right_value_list;
		right_value_list.value_num = 0;

		for(int i = CONTEXT->value_start; i < CONTEXT->value_length; i++) {
			Value *right_value = &CONTEXT->values[i];
			list_append_value(&right_value_list,right_value);
		}
		
		Condition condition;
		condition_init(&condition, $4, 3, NULL, NULL, CONTEXT->selects[CONTEXT->select_length], NULL, 4, NULL, NULL, NULL, &right_value_list, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
	| sublist_start value RBRACE comOp ID RBRACE {
		Value *left_value = &CONTEXT->values[CONTEXT->value_length - 1];

		RelAttr right_attr;
		relation_attr_init(&right_attr, NULL, $5, false, 5, CONDITIONFIELD);
		
		Condition condition;
		condition_init(&condition, $4, 2, NULL, left_value, NULL, NULL, 1, &right_attr, NULL, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
	| sublist_start value RBRACE comOp ID DOT ID RBRACE {
		Value *left_value = &CONTEXT->values[CONTEXT->value_length - 1];

		RelAttr right_attr;
		relation_attr_init(&right_attr, $5, $7, false, 5, CONDITIONFIELD);
		
		Condition condition;
		condition_init(&condition, $4, 2, NULL, left_value, NULL, NULL, 1, &right_attr, NULL, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
	| sublist_start value RBRACE comOp value RBRACE {
		Value *left_value = &CONTEXT->values[CONTEXT->value_length - 2];
		Value *right_value = &CONTEXT->values[CONTEXT->value_length - 1];
		
		Condition condition;
		condition_init(&condition, $4, 2, NULL, left_value, NULL, NULL, 2, NULL, right_value, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
	| sublist_start value RBRACE comOp subquery_start select RBRACE
	{
		Value *left_value = &CONTEXT->values[CONTEXT->value_length - 1];
		
		Condition condition;
		condition_init(&condition, $4, 2, NULL, left_value, NULL, NULL, 3, NULL, NULL, CONTEXT->selects[CONTEXT->select_length], NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		CONTEXT->select_length--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
	| ID comOp subquery_start select RBRACE
	{
		RelAttr left_attr;
		relation_attr_init(&left_attr, NULL, $1, false, 5, CONDITIONFIELD);
		
		Condition condition;
		condition_init(&condition, $2, 1, &left_attr, NULL, NULL, NULL, 3, NULL, NULL, CONTEXT->selects[CONTEXT->select_length], NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		CONTEXT->select_length--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
	| ID DOT ID comOp subquery_start select RBRACE
	{
		RelAttr left_attr;
		relation_attr_init(&left_attr, $1, $3, false, 5, CONDITIONFIELD);
		
		Condition condition;
		condition_init(&condition, $4, 1, &left_attr, NULL, NULL, NULL, 3, NULL, NULL, CONTEXT->selects[CONTEXT->select_length], NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		CONTEXT->select_length--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
	| subquery_start select RBRACE comOp value
	{
		Value *left_value = &CONTEXT->values[CONTEXT->value_length - 1];
		
		Condition condition;
		condition_init(&condition, $4, 3, NULL, NULL, CONTEXT->selects[CONTEXT->select_length], NULL, 2, NULL, left_value, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		CONTEXT->select_length--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
	| subquery_start select RBRACE comOp ID
	{
		RelAttr right_attr;
		relation_attr_init(&right_attr, NULL, $5, false, 5, CONDITIONFIELD);
		
		Condition condition;
		condition_init(&condition, $4, 3, NULL, NULL, CONTEXT->selects[CONTEXT->select_length], NULL, 1, &right_attr, NULL, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		CONTEXT->select_length--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
	| subquery_start select RBRACE comOp ID DOT ID
	{
		RelAttr right_attr;
		relation_attr_init(&right_attr, $5, $7, false, 5, CONDITIONFIELD);
		
		Condition condition;
		condition_init(&condition, $4, 3, NULL, NULL, CONTEXT->selects[CONTEXT->select_length], NULL, 1, &right_attr, NULL, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		CONTEXT->select_length--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
	| subquery_start select RBRACE comOp subquery_start select RBRACE
	{
		
		Condition condition;
		condition_init(&condition, $4, 3, NULL, NULL, CONTEXT->selects[CONTEXT->select_length - 1], NULL, 3, NULL, NULL, CONTEXT->selects[CONTEXT->select_length], NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		CONTEXT->select_length -= 2;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
    ;
eq_comp_list:
	/* empty */
    | COMMA eq_comp eq_comp_list {
				
			}
	;
eq_comp:
	ID EQ value
	{
		RelAttr left_attr;
		relation_attr_init(&left_attr, NULL, $1, false, 5, CONDITIONFIELD);
		Value *right_value = &CONTEXT->values[CONTEXT->value_length - 1];
		SetCondition condition;
		set_condition_init(&condition, &left_attr, 2, right_value, NULL);
		CONTEXT->set_conditions[CONTEXT->set_condition_length++] = condition;
	}
	| ID EQ subquery_start select RBRACE {
		RelAttr left_attr;
		relation_attr_init(&left_attr, NULL, $1, false, 5, CONDITIONFIELD);
		SetCondition condition;
		set_condition_init(&condition, &left_attr, 3, NULL, CONTEXT->selects[CONTEXT->select_length]);
		CONTEXT->select_length--;
		CONTEXT->set_conditions[CONTEXT->set_condition_length++] = condition;
	}
	;
comOp:
  	  EQ { $$ = EQUAL_TO; }
    | LT { $$ = LESS_THAN; }
    | GT { $$ = GREAT_THAN; }
    | LE { $$ = LESS_EQUAL; }
    | GE { $$ = GREAT_EQUAL; }
    | NE { $$ = NOT_EQUAL; }
	| IN { $$ = IS_IN; }
	| NOT IN { $$ = NOT_IN; }
	| NOT EXIST { $$ = NOT_EXIST; }
	| EXIST { $$ = EXIST_IN; }
	| CIS { $$ = IS; }
	| CIS NOT { $$ = IS_NOT; }
	| LIKE { $$ = LIKE_OP; }
	| NOT LIKE { $$ = NOT_LIKE_OP; }
    ;

load_data:
		LOAD DATA INFILE SSS INTO TABLE ID SEMICOLON
		{
		  CONTEXT->ssql->flag = SCF_LOAD_DATA;
			load_data_init(&CONTEXT->ssql->sstr.load_data, $7, $4);
		}
		;

tuple_list:
    /* empty */
    | COMMA LBRACE value value_list RBRACE tuple_list {	

	}
    ;
%%
//_____________________________________________________________________
extern void scan_string(const char *str, yyscan_t scanner);

int sql_parse(const char *s, Query *sqls){
	ParserContext context;
	memset(&context, 0, sizeof(context));
	yyscan_t scanner;
	yylex_init_extra(&context, &scanner);
	context.ssql = sqls;
	scan_string(s, scanner);
	int result = yyparse(scanner);
	yylex_destroy(scanner);
	return result;
}
