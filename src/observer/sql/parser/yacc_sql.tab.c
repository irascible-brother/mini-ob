/* A Bison parser, made by GNU Bison 3.7.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.7"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 2

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 2 "yacc_sql.y"


#include "sql/parser/parse_defs.h"
#include "sql/parser/yacc_sql.tab.h"
#include "sql/parser/lex.yy.h"
// #include "common/log/log.h" // 包含C++中的头文件

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct ParserContext {
  Query * ssql;
  size_t select_length;
  size_t condition_length;
  size_t from_length;
  size_t value_length;
  size_t value_start;
  Value values[MAX_NUM];
  Condition conditions[MAX_NUM];
  SetCondition set_conditions[MAX_NUM];
  size_t set_condition_length;
  RelAttr groups[MAX_NUM];
  size_t group_length;
  char id[MAX_NUM];
  bool is_and[MAX_NUM];
  size_t cur_condition;
  Aggregator cur_agg;
  bool is_right;
  HavingCondition having_condition;
  bool is_having;
  // 子查询数组
  Selects* selects[MAX_SELECT_NUM];
} ParserContext;

//获取子串
char *substr(const char *s,int n1,int n2)/*从s中提取下标为n1~n2的字符组成一个新字符串，然后返回这个新串的首地址*/
{
  char *sp = malloc(sizeof(char) * (n2 - n1 + 2));
  int i, j = 0;
  for (i = n1; i <= n2; i++) {
    sp[j++] = s[i];
  }
  sp[j] = 0;
  free((char *)s);
  return sp;
}

void yyerror(yyscan_t scanner, const char *str)
{
  ParserContext *context = (ParserContext *)(yyget_extra(scanner));
  query_reset(context->ssql);
  context->ssql->flag = SCF_ERROR;
  context->condition_length = 0;
  context->cur_condition = 0;
  context->from_length = 0;
  context->value_start = 0;
  context->select_length = 0;
  context->value_length = 0;
  context->ssql->sstr.insertion.value_num = 0;
  printf("parse sql failed. error=%s", str);
}

ParserContext *get_context(yyscan_t scanner)
{
  return (ParserContext *)yyget_extra(scanner);
}

#define CONTEXT get_context(scanner)


#line 143 "yacc_sql.tab.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "yacc_sql.tab.h"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_SEMICOLON = 3,                  /* SEMICOLON  */
  YYSYMBOL_CREATE = 4,                     /* CREATE  */
  YYSYMBOL_DROP = 5,                       /* DROP  */
  YYSYMBOL_TABLE = 6,                      /* TABLE  */
  YYSYMBOL_TABLES = 7,                     /* TABLES  */
  YYSYMBOL_INDEX = 8,                      /* INDEX  */
  YYSYMBOL_SELECT = 9,                     /* SELECT  */
  YYSYMBOL_DESC = 10,                      /* DESC  */
  YYSYMBOL_SHOW = 11,                      /* SHOW  */
  YYSYMBOL_SYNC = 12,                      /* SYNC  */
  YYSYMBOL_INSERT = 13,                    /* INSERT  */
  YYSYMBOL_DELETE = 14,                    /* DELETE  */
  YYSYMBOL_UPDATE = 15,                    /* UPDATE  */
  YYSYMBOL_LBRACE = 16,                    /* LBRACE  */
  YYSYMBOL_RBRACE = 17,                    /* RBRACE  */
  YYSYMBOL_COMMA = 18,                     /* COMMA  */
  YYSYMBOL_TRX_BEGIN = 19,                 /* TRX_BEGIN  */
  YYSYMBOL_TRX_COMMIT = 20,                /* TRX_COMMIT  */
  YYSYMBOL_TRX_ROLLBACK = 21,              /* TRX_ROLLBACK  */
  YYSYMBOL_INT_T = 22,                     /* INT_T  */
  YYSYMBOL_STRING_T = 23,                  /* STRING_T  */
  YYSYMBOL_FLOAT_T = 24,                   /* FLOAT_T  */
  YYSYMBOL_TEXT_T = 25,                    /* TEXT_T  */
  YYSYMBOL_HELP = 26,                      /* HELP  */
  YYSYMBOL_EXIT = 27,                      /* EXIT  */
  YYSYMBOL_DOT = 28,                       /* DOT  */
  YYSYMBOL_INTO = 29,                      /* INTO  */
  YYSYMBOL_VALUES = 30,                    /* VALUES  */
  YYSYMBOL_FROM = 31,                      /* FROM  */
  YYSYMBOL_WHERE = 32,                     /* WHERE  */
  YYSYMBOL_AND = 33,                       /* AND  */
  YYSYMBOL_OR = 34,                        /* OR  */
  YYSYMBOL_SET = 35,                       /* SET  */
  YYSYMBOL_ON = 36,                        /* ON  */
  YYSYMBOL_LOAD = 37,                      /* LOAD  */
  YYSYMBOL_DATA = 38,                      /* DATA  */
  YYSYMBOL_INNER = 39,                     /* INNER  */
  YYSYMBOL_JOIN = 40,                      /* JOIN  */
  YYSYMBOL_INFILE = 41,                    /* INFILE  */
  YYSYMBOL_DATE_T = 42,                    /* DATE_T  */
  YYSYMBOL_CNT = 43,                       /* CNT  */
  YYSYMBOL_MAX = 44,                       /* MAX  */
  YYSYMBOL_MIN = 45,                       /* MIN  */
  YYSYMBOL_AVG = 46,                       /* AVG  */
  YYSYMBOL_SUM = 47,                       /* SUM  */
  YYSYMBOL_UNIQUE = 48,                    /* UNIQUE  */
  YYSYMBOL_NULLABLE = 49,                  /* NULLABLE  */
  YYSYMBOL_NULL_V = 50,                    /* NULL_V  */
  YYSYMBOL_LIKE = 51,                      /* LIKE  */
  YYSYMBOL_IN = 52,                        /* IN  */
  YYSYMBOL_HAVING = 53,                    /* HAVING  */
  YYSYMBOL_AS = 54,                        /* AS  */
  YYSYMBOL_EXIST = 55,                     /* EXIST  */
  YYSYMBOL_NOT = 56,                       /* NOT  */
  YYSYMBOL_ORDER = 57,                     /* ORDER  */
  YYSYMBOL_GROUP = 58,                     /* GROUP  */
  YYSYMBOL_BY = 59,                        /* BY  */
  YYSYMBOL_ASC = 60,                       /* ASC  */
  YYSYMBOL_CIS = 61,                       /* CIS  */
  YYSYMBOL_EQ = 62,                        /* EQ  */
  YYSYMBOL_LT = 63,                        /* LT  */
  YYSYMBOL_GT = 64,                        /* GT  */
  YYSYMBOL_LE = 65,                        /* LE  */
  YYSYMBOL_GE = 66,                        /* GE  */
  YYSYMBOL_NE = 67,                        /* NE  */
  YYSYMBOL_NUMBER = 68,                    /* NUMBER  */
  YYSYMBOL_FLOAT = 69,                     /* FLOAT  */
  YYSYMBOL_ID = 70,                        /* ID  */
  YYSYMBOL_PATH = 71,                      /* PATH  */
  YYSYMBOL_SSS = 72,                       /* SSS  */
  YYSYMBOL_STAR = 73,                      /* STAR  */
  YYSYMBOL_STRING_V = 74,                  /* STRING_V  */
  YYSYMBOL_YYACCEPT = 75,                  /* $accept  */
  YYSYMBOL_commands = 76,                  /* commands  */
  YYSYMBOL_command = 77,                   /* command  */
  YYSYMBOL_exit = 78,                      /* exit  */
  YYSYMBOL_help = 79,                      /* help  */
  YYSYMBOL_sync = 80,                      /* sync  */
  YYSYMBOL_begin = 81,                     /* begin  */
  YYSYMBOL_commit = 82,                    /* commit  */
  YYSYMBOL_rollback = 83,                  /* rollback  */
  YYSYMBOL_drop_table = 84,                /* drop_table  */
  YYSYMBOL_show_tables = 85,               /* show_tables  */
  YYSYMBOL_desc_table = 86,                /* desc_table  */
  YYSYMBOL_show_index = 87,                /* show_index  */
  YYSYMBOL_create_index = 88,              /* create_index  */
  YYSYMBOL_index_attr = 89,                /* index_attr  */
  YYSYMBOL_index_attr_list = 90,           /* index_attr_list  */
  YYSYMBOL_drop_index = 91,                /* drop_index  */
  YYSYMBOL_create_table = 92,              /* create_table  */
  YYSYMBOL_attr_def_list = 93,             /* attr_def_list  */
  YYSYMBOL_attr_def = 94,                  /* attr_def  */
  YYSYMBOL_number = 95,                    /* number  */
  YYSYMBOL_type = 96,                      /* type  */
  YYSYMBOL_ID_get = 97,                    /* ID_get  */
  YYSYMBOL_insert = 98,                    /* insert  */
  YYSYMBOL_value_list = 99,                /* value_list  */
  YYSYMBOL_value = 100,                    /* value  */
  YYSYMBOL_delete = 101,                   /* delete  */
  YYSYMBOL_update = 102,                   /* update  */
  YYSYMBOL_select_flag = 103,              /* select_flag  */
  YYSYMBOL_select_table = 104,             /* select_table  */
  YYSYMBOL_select = 105,                   /* select  */
  YYSYMBOL_join_flag = 106,                /* join_flag  */
  YYSYMBOL_join_list = 107,                /* join_list  */
  YYSYMBOL_alias = 108,                    /* alias  */
  YYSYMBOL_select_attr = 109,              /* select_attr  */
  YYSYMBOL_attr_list = 110,                /* attr_list  */
  YYSYMBOL_rel_list = 111,                 /* rel_list  */
  YYSYMBOL_order = 112,                    /* order  */
  YYSYMBOL_order_list = 113,               /* order_list  */
  YYSYMBOL_orders = 114,                   /* orders  */
  YYSYMBOL_group = 115,                    /* group  */
  YYSYMBOL_aggr_flag = 116,                /* aggr_flag  */
  YYSYMBOL_group_list = 117,               /* group_list  */
  YYSYMBOL_part = 118,                     /* part  */
  YYSYMBOL_having = 119,                   /* having  */
  YYSYMBOL_groups = 120,                   /* groups  */
  YYSYMBOL_where_flag = 121,               /* where_flag  */
  YYSYMBOL_where = 122,                    /* where  */
  YYSYMBOL_and_or = 123,                   /* and_or  */
  YYSYMBOL_condition_list = 124,           /* condition_list  */
  YYSYMBOL_subquery_start = 125,           /* subquery_start  */
  YYSYMBOL_sublist_start = 126,            /* sublist_start  */
  YYSYMBOL_condition = 127,                /* condition  */
  YYSYMBOL_eq_comp_list = 128,             /* eq_comp_list  */
  YYSYMBOL_eq_comp = 129,                  /* eq_comp  */
  YYSYMBOL_comOp = 130,                    /* comOp  */
  YYSYMBOL_load_data = 131,                /* load_data  */
  YYSYMBOL_tuple_list = 132                /* tuple_list  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_int16 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                            \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  2
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   487

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  75
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  58
/* YYNRULES -- Number of rules.  */
#define YYNRULES  186
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  476

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   329


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   174,   174,   176,   180,   181,   182,   183,   184,   185,
     186,   187,   188,   189,   190,   191,   192,   193,   194,   195,
     196,   197,   201,   206,   211,   217,   223,   229,   235,   241,
     247,   254,   261,   266,   274,   279,   281,   287,   294,   303,
     305,   309,   320,   331,   342,   353,   364,   373,   376,   377,
     378,   379,   380,   383,   393,   409,   411,   416,   419,   422,
     426,   432,   442,   451,   461,   469,   481,   486,   488,   494,
     495,   498,   503,   508,   517,   526,   535,   547,   559,   572,
     585,   598,   611,   624,   637,   650,   671,   684,   698,   700,
     713,   726,   739,   752,   765,   777,   790,   803,   816,   837,
     850,   863,   873,   883,   893,   895,   903,   911,   919,   927,
     935,   943,   952,   954,   956,   958,   961,   971,   984,   987,
     990,   993,   996,   999,  1001,  1004,  1017,  1030,  1043,  1055,
    1057,  1062,  1064,  1067,  1072,  1074,  1078,  1082,  1087,  1089,
    1093,  1099,  1105,  1121,  1135,  1153,  1168,  1184,  1201,  1217,
    1231,  1245,  1265,  1285,  1305,  1325,  1344,  1359,  1374,  1387,
    1401,  1416,  1431,  1445,  1460,  1475,  1489,  1491,  1496,  1505,
    1515,  1516,  1517,  1518,  1519,  1520,  1521,  1522,  1523,  1524,
    1525,  1526,  1527,  1528,  1532,  1539,  1541
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "SEMICOLON", "CREATE",
  "DROP", "TABLE", "TABLES", "INDEX", "SELECT", "DESC", "SHOW", "SYNC",
  "INSERT", "DELETE", "UPDATE", "LBRACE", "RBRACE", "COMMA", "TRX_BEGIN",
  "TRX_COMMIT", "TRX_ROLLBACK", "INT_T", "STRING_T", "FLOAT_T", "TEXT_T",
  "HELP", "EXIT", "DOT", "INTO", "VALUES", "FROM", "WHERE", "AND", "OR",
  "SET", "ON", "LOAD", "DATA", "INNER", "JOIN", "INFILE", "DATE_T", "CNT",
  "MAX", "MIN", "AVG", "SUM", "UNIQUE", "NULLABLE", "NULL_V", "LIKE", "IN",
  "HAVING", "AS", "EXIST", "NOT", "ORDER", "GROUP", "BY", "ASC", "CIS",
  "EQ", "LT", "GT", "LE", "GE", "NE", "NUMBER", "FLOAT", "ID", "PATH",
  "SSS", "STAR", "STRING_V", "$accept", "commands", "command", "exit",
  "help", "sync", "begin", "commit", "rollback", "drop_table",
  "show_tables", "desc_table", "show_index", "create_index", "index_attr",
  "index_attr_list", "drop_index", "create_table", "attr_def_list",
  "attr_def", "number", "type", "ID_get", "insert", "value_list", "value",
  "delete", "update", "select_flag", "select_table", "select", "join_flag",
  "join_list", "alias", "select_attr", "attr_list", "rel_list", "order",
  "order_list", "orders", "group", "aggr_flag", "group_list", "part",
  "having", "groups", "where_flag", "where", "and_or", "condition_list",
  "subquery_start", "sublist_start", "condition", "eq_comp_list",
  "eq_comp", "comOp", "load_data", "tuple_list", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_int16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329
};
#endif

#define YYPACT_NINF (-283)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-141)

#define yytable_value_is_error(Yyn) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
    -283,   328,  -283,     8,    62,  -283,   -38,    41,    37,    26,
      45,    10,   103,   107,   124,   132,   139,    51,  -283,  -283,
    -283,  -283,  -283,  -283,  -283,  -283,  -283,  -283,  -283,  -283,
    -283,  -283,  -283,  -283,  -283,   229,  -283,   144,  -283,    31,
      73,   146,    93,    99,   173,   174,   154,  -283,   117,   126,
     159,  -283,  -283,  -283,  -283,  -283,   161,   182,   184,   188,
     201,   202,   -11,   212,   200,  -283,   223,   207,   175,   249,
     254,  -283,  -283,   189,   247,   253,   209,   215,   143,   219,
     221,   225,   226,    27,   230,  -283,   212,   177,  -283,   231,
     240,   241,   276,  -283,  -283,   310,   298,  -283,   199,   312,
     256,   301,   296,  -283,     6,   309,   314,    11,    16,    24,
      25,   -33,   -33,  -283,  -283,   311,   318,   319,   329,   330,
      -9,   -33,  -283,   326,   137,   334,   259,  -283,    72,   321,
    -283,  -283,  -283,  -283,   120,   295,  -283,  -283,  -283,  -283,
    -283,  -283,  -283,  -283,   242,  -283,   332,   343,    72,    86,
     337,  -283,   140,   209,   253,   355,   -33,   292,   -33,   -33,
     -33,   294,   -33,   297,   -33,   299,   -33,   300,   212,   212,
     210,   302,   303,   304,   305,   183,   212,   348,   240,   351,
    -283,  -283,  -283,  -283,  -283,    13,   306,   361,   353,  -283,
    -283,  -283,  -283,   308,    14,    22,   362,   363,  -283,  -283,
     199,  -283,  -283,   343,  -283,   343,   301,   383,   320,   212,
     372,   212,   212,   212,   374,   212,   375,   212,   384,   212,
     385,  -283,  -283,    29,   386,   387,    68,    76,    81,    88,
     -33,   -33,  -283,   335,   367,   326,   397,   339,  -283,   358,
     391,   393,   306,    72,   394,   332,  -283,  -283,   343,    72,
     388,  -283,   343,    72,   332,   332,    86,   395,   396,  -283,
    -283,   411,  -283,   -33,  -283,  -283,  -283,   -33,  -283,   -33,
    -283,   -33,  -283,   -33,   -33,   345,   -33,   -33,   -33,   347,
     -33,   349,   -33,   350,   -33,   352,   212,   212,   -33,   378,
     354,   253,  -283,  -283,   404,  -283,   356,  -283,   420,   408,
     353,   409,    83,   412,   353,   360,   414,   353,    98,   157,
    -283,  -283,  -283,  -283,   212,   212,   212,   212,   212,   212,
     415,   212,   212,   212,   416,   212,   417,   212,   418,   212,
     419,  -283,  -283,   348,  -283,   392,   379,    -2,   391,  -283,
     435,  -283,   423,   437,   413,  -283,   343,    72,  -283,   425,
    -283,  -283,   426,   421,  -283,   343,    72,   109,   427,   343,
      72,  -283,  -283,  -283,  -283,  -283,  -283,   -33,  -283,  -283,
    -283,   -33,  -283,   -33,  -283,   -33,  -283,   -33,  -283,   199,
     389,   390,  -283,   400,  -283,  -283,    72,  -283,   376,   428,
     353,  -283,  -283,   381,   436,   353,  -283,   382,  -283,   438,
     353,   212,   212,   212,   212,   212,    86,   398,   399,  -283,
    -283,   353,  -283,  -283,   439,  -283,  -283,   440,   442,  -283,
     443,  -283,  -283,  -283,  -283,  -283,   367,   433,   444,   401,
     446,  -283,  -283,  -283,  -283,  -283,   402,   398,   422,     3,
     447,   409,  -283,   444,   313,  -283,  -283,   403,  -283,   401,
    -283,  -283,  -283,  -283,  -283,  -283,  -283,  -283,  -283,   448,
     332,     5,   447,   211,   313,  -283,  -283,  -283,   122,   449,
    -283,  -283,   406,  -283,   450,  -283
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       2,     0,     1,     0,     0,    63,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     3,    21,
      20,    15,    16,    17,    18,     9,    10,    11,    12,    13,
      14,     8,     5,     7,     6,     0,     4,     0,    19,     0,
       0,     0,     0,     0,     0,     0,     0,    24,     0,     0,
       0,    25,    26,    27,    23,    22,     0,     0,     0,     0,
       0,     0,    69,    88,     0,    64,     0,     0,     0,     0,
       0,    30,    29,     0,     0,   134,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    70,    88,     0,    72,     0,
       0,     0,     0,    28,    37,     0,     0,   133,     0,     0,
       0,   166,     0,    47,     0,     0,     0,     0,     0,     0,
       0,    69,    69,    71,    73,     0,     0,     0,     0,     0,
      69,    69,    53,    39,     0,     0,     0,    31,     0,   141,
      60,   182,   176,   179,     0,   180,   170,   171,   172,   173,
     174,   175,    57,    58,     0,    59,     0,     0,     0,   138,
       0,    61,     0,     0,   134,     0,    69,     0,    69,    69,
      69,     0,    69,     0,    69,     0,    69,     0,    88,    88,
       0,     0,     0,     0,     0,     0,    88,   104,     0,     0,
      48,    49,    50,    52,    51,    42,     0,     0,    55,   183,
     177,   178,   181,     0,     0,     0,     0,     0,   136,   137,
       0,   135,   140,     0,   168,     0,   166,     0,     0,    88,
       0,    88,    88,    88,     0,    88,     0,    88,     0,    88,
       0,    74,    75,     0,     0,     0,     0,     0,     0,     0,
      69,    69,   101,     0,    67,    39,     0,     0,    44,     0,
      35,     0,     0,     0,     0,     0,   144,   142,     0,     0,
     145,   143,     0,     0,     0,     0,   138,     0,     0,   167,
      62,     0,    86,    69,    84,    85,    76,    69,    78,    69,
      81,    69,    83,    69,    69,     0,    69,    69,    69,     0,
      69,     0,    69,     0,    69,     0,    88,    88,    69,     0,
       0,   134,    40,    38,     0,    46,     0,    34,     0,     0,
      55,   185,     0,     0,    55,     0,     0,    55,     0,     0,
     139,   149,   169,   184,    88,    88,    88,    88,    88,    88,
       0,    88,    88,    88,     0,    88,     0,    88,     0,    88,
       0,   102,   103,   104,    66,     0,   131,    41,    35,    32,
       0,    56,     0,     0,     0,   146,     0,     0,   160,     0,
     147,   150,     0,   163,   162,     0,     0,     0,     0,     0,
       0,    87,    77,    79,    80,    82,    99,    69,    97,    98,
      89,    69,    91,    69,    93,    69,    95,    69,   105,     0,
       0,   114,    43,     0,    36,    33,     0,    54,     0,     0,
      55,   151,   153,     0,     0,    55,   156,     0,   158,     0,
      55,    88,    88,    88,    88,    88,   138,     0,     0,    65,
      45,    55,   148,   161,     0,   164,   165,     0,     0,   159,
       0,   100,    90,    92,    94,    96,    67,   116,   123,     0,
       0,   152,   155,   157,   154,    68,     0,     0,   129,   106,
     112,   185,   117,   123,     0,   132,   108,     0,   107,     0,
     115,   186,   124,   122,   118,   119,   120,   121,   128,     0,
       0,   109,   112,     0,     0,   111,   110,   113,     0,     0,
     130,   125,     0,   126,     0,   127
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -283,  -283,  -283,  -283,  -283,  -283,  -283,  -283,  -283,  -283,
    -283,  -283,  -283,  -283,   227,   116,  -283,  -283,   235,   307,
    -159,  -283,  -283,  -283,  -282,  -128,  -283,  -283,  -283,  -283,
    -145,  -283,    48,   -85,  -283,   -81,   145,    28,    17,  -283,
      43,  -283,    38,    18,  -283,  -283,  -283,  -142,  -283,  -249,
    -144,  -185,  -196,   277,   331,  -143,  -283,    46
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,   241,   297,    30,    31,   179,   123,
     106,   185,   124,    32,   244,   146,    33,    34,    35,    36,
      37,   290,   291,    86,    64,    88,   234,   440,   450,   409,
     428,   459,   438,   460,   445,   381,    98,    99,   200,   201,
     147,   148,   149,   154,   101,   150,    38,   343
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
     188,   194,   196,   195,   256,   114,   203,   310,   205,   249,
     253,   225,   207,   446,    39,   465,    40,    83,   341,   175,
     197,    84,   349,   156,   204,   352,   168,   169,   160,   237,
     129,   447,    44,   162,   157,   176,   177,    85,   129,   161,
      47,   164,   166,    84,   163,    84,   274,   382,    45,    46,
     248,   252,   165,   167,   383,    48,    41,   275,   257,    85,
     258,    85,   238,   448,   130,   466,   247,   251,    42,   239,
      43,   209,   130,   211,   212,   213,    49,   215,   294,   217,
      50,   219,   142,   143,   246,   278,   145,   221,   222,    56,
     142,   143,   250,   280,   145,   232,   279,   111,   282,   129,
     112,    66,   302,   303,   281,   284,    51,   306,   414,   283,
      52,   308,   309,   417,   129,   300,   285,   347,   420,   198,
     199,   304,   130,   356,   360,   307,   396,    53,   262,   430,
     264,   265,   266,   130,   268,    54,   270,   397,   272,   471,
     142,   143,    55,    67,   145,   286,   287,    65,   130,   336,
     472,   142,   143,   344,    68,   145,   202,   426,   346,   180,
     181,   182,   183,    69,   355,   359,   142,   143,   353,    70,
     145,   189,   190,   129,   345,   191,    71,    72,   314,   184,
     354,   358,   315,   406,   316,    73,   317,    74,   318,   319,
     130,   321,   322,   323,    76,   325,    75,   327,    78,   329,
      79,   389,    77,   333,    80,   331,   332,   130,   142,   143,
     394,   103,   145,   104,   399,   129,   105,    81,    82,   390,
     115,   116,   117,   118,   119,   142,   143,   357,   395,   145,
      87,    89,   400,   361,   362,   363,   364,   365,   366,    90,
     368,   369,   370,    91,   372,    92,   374,   120,   376,   130,
     131,   132,    93,   230,   133,   134,   231,    94,   411,    95,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     193,   145,    57,    58,    59,    60,    61,    96,   103,   100,
     223,   468,   401,   224,   469,    97,   402,   102,   403,   107,
     404,   108,   405,   131,   132,   109,   110,   133,   134,    62,
     113,   121,    63,   135,   136,   137,   138,   139,   140,   141,
     122,   125,   126,   127,   128,   151,   458,   464,   152,   153,
     421,   422,   423,   424,   425,   155,   158,   170,     2,   187,
    -140,   159,     3,     4,   171,   172,   458,     5,     6,     7,
       8,     9,    10,    11,   178,   173,   174,    12,    13,    14,
     186,   192,     5,   202,    15,    16,   453,   454,   455,   456,
     457,   208,   210,   130,   214,    17,   233,   216,   236,   218,
     220,   243,   226,   227,   228,   229,   240,   242,   245,   254,
     255,   142,   143,   131,   132,   145,   260,   133,   134,   263,
     261,   267,   269,   135,   136,   137,   138,   139,   140,   141,
     293,   271,   273,   276,   277,   288,   289,   103,   295,   296,
     298,   301,   311,   312,   313,   320,   305,   324,   334,   326,
     328,   337,   330,   339,   335,   340,   338,   342,   379,   348,
     350,   351,   367,   371,   373,   375,   377,   380,   385,   386,
     387,   388,   391,   392,   398,   413,   412,   408,   407,   393,
     410,   415,   418,   416,   384,   419,   431,   432,   429,   433,
     434,   436,   437,   441,   463,   449,   473,   475,   427,   299,
     292,   439,   442,   461,   435,   444,   474,   462,   378,   467,
     443,   452,   470,   259,   206,   235,     0,   451
};

static const yytype_int16 yycheck[] =
{
     128,   144,   147,   146,   200,    86,   150,   256,   152,   194,
     195,   170,   154,    10,     6,    10,     8,    28,   300,    28,
     148,    54,   304,    17,   152,   307,   111,   112,    17,    16,
      16,    28,    70,    17,    28,   120,   121,    70,    16,    28,
       3,    17,    17,    54,    28,    54,    17,    49,     7,     8,
     194,   195,    28,    28,    56,    29,    48,    28,   203,    70,
     205,    70,    49,    60,    50,    60,   194,   195,     6,    56,
       8,   156,    50,   158,   159,   160,    31,   162,   237,   164,
      70,   166,    68,    69,    70,    17,    72,   168,   169,    38,
      68,    69,    70,    17,    72,   176,    28,    70,    17,    16,
      73,    70,   245,   248,    28,    17,     3,   252,   390,    28,
       3,   254,   255,   395,    16,   243,    28,   302,   400,    33,
      34,   249,    50,   308,   309,   253,    17,     3,   209,   411,
     211,   212,   213,    50,   215,     3,   217,    28,   219,    17,
      68,    69,     3,    70,    72,   230,   231,     3,    50,   291,
      28,    68,    69,    70,     8,    72,    16,   406,   302,    22,
      23,    24,    25,    70,   308,   309,    68,    69,    70,    70,
      72,    51,    52,    16,   302,    55,     3,     3,   263,    42,
     308,   309,   267,   379,   269,    31,   271,    70,   273,   274,
      50,   276,   277,   278,    35,   280,    70,   282,    16,   284,
      16,   346,    41,   288,    16,   286,   287,    50,    68,    69,
     355,    68,    72,    70,   359,    16,    73,    16,    16,   347,
      43,    44,    45,    46,    47,    68,    69,    70,   356,    72,
      18,    31,   360,   314,   315,   316,   317,   318,   319,    16,
     321,   322,   323,    36,   325,    70,   327,    70,   329,    50,
      51,    52,     3,    70,    55,    56,    73,     3,   386,    70,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
      28,    72,    43,    44,    45,    46,    47,    30,    68,    70,
      70,    70,   367,    73,    73,    32,   371,    72,   373,    70,
     375,    70,   377,    51,    52,    70,    70,    55,    56,    70,
      70,    70,    73,    61,    62,    63,    64,    65,    66,    67,
      70,    70,    36,     3,    16,     3,   444,   460,    62,    18,
     401,   402,   403,   404,   405,    29,    17,    16,     0,    70,
       9,    17,     4,     5,    16,    16,   464,     9,    10,    11,
      12,    13,    14,    15,    18,    16,    16,    19,    20,    21,
      16,    56,     9,    16,    26,    27,    43,    44,    45,    46,
      47,     6,    70,    50,    70,    37,    18,    70,    17,    70,
      70,    18,    70,    70,    70,    70,    70,    16,    70,    17,
      17,    68,    69,    51,    52,    72,     3,    55,    56,    17,
      70,    17,    17,    61,    62,    63,    64,    65,    66,    67,
       3,    17,    17,    17,    17,    70,    39,    68,    50,    18,
      17,    17,    17,    17,     3,    70,    28,    70,    40,    70,
      70,    17,    70,     3,    70,    17,    70,    18,    36,    17,
      70,    17,    17,    17,    17,    17,    17,    58,     3,    16,
       3,    28,    17,    17,    17,    17,    70,    57,    59,    28,
      50,    70,    70,    17,   338,    17,    17,    17,    59,    17,
      17,    28,    18,    17,    16,    18,    17,    17,    70,   242,
     235,    70,    70,    70,   426,    53,    70,   449,   333,   462,
     437,   443,   464,   206,   153,   178,    -1,   441
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    76,     0,     4,     5,     9,    10,    11,    12,    13,
      14,    15,    19,    20,    21,    26,    27,    37,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      91,    92,    98,   101,   102,   103,   104,   105,   131,     6,
       8,    48,     6,     8,    70,     7,     8,     3,    29,    31,
      70,     3,     3,     3,     3,     3,    38,    43,    44,    45,
      46,    47,    70,    73,   109,     3,    70,    70,     8,    70,
      70,     3,     3,    31,    70,    70,    35,    41,    16,    16,
      16,    16,    16,    28,    54,    70,   108,    18,   110,    31,
      16,    36,    70,     3,     3,    70,    30,    32,   121,   122,
      70,   129,    72,    68,    70,    73,    95,    70,    70,    70,
      70,    70,    73,    70,   110,    43,    44,    45,    46,    47,
      70,    70,    70,    94,    97,    70,    36,     3,    16,    16,
      50,    51,    52,    55,    56,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    72,   100,   125,   126,   127,
     130,     3,    62,    18,   128,    29,    17,    28,    17,    17,
      17,    28,    17,    28,    17,    28,    17,    28,   108,   108,
      16,    16,    16,    16,    16,    28,   108,   108,    18,    93,
      22,    23,    24,    25,    42,    96,    16,    70,   100,    51,
      52,    55,    56,    28,   130,   130,   105,   100,    33,    34,
     123,   124,    16,   125,   100,   125,   129,   122,     6,   108,
      70,   108,   108,   108,    70,   108,    70,   108,    70,   108,
      70,   110,   110,    70,    73,    95,    70,    70,    70,    70,
      70,    73,   110,    18,   111,    94,    17,    16,    49,    56,
      70,    89,    16,    18,    99,    70,    70,   100,   125,   126,
      70,   100,   125,   126,    17,    17,   127,   105,   105,   128,
       3,    70,   110,    17,   110,   110,   110,    17,   110,    17,
     110,    17,   110,    17,    17,    28,    17,    17,    17,    28,
      17,    28,    17,    28,    17,    28,   108,   108,    70,    39,
     106,   107,    93,     3,    95,    50,    18,    90,    17,    89,
     100,    17,   130,   105,   100,    28,   105,   100,   130,   130,
     124,    17,    17,     3,   108,   108,   108,   108,   108,   108,
      70,   108,   108,   108,    70,   108,    70,   108,    70,   108,
      70,   110,   110,   108,    40,    70,   122,    17,    70,     3,
      17,    99,    18,   132,    70,   100,   125,   126,    17,    99,
      70,    17,    99,    70,   100,   125,   126,    70,   100,   125,
     126,   110,   110,   110,   110,   110,   110,    17,   110,   110,
     110,    17,   110,    17,   110,    17,   110,    17,   111,    36,
      58,   120,    49,    56,    90,     3,    16,     3,    28,   105,
     100,    17,    17,    28,   105,   100,    17,    28,    17,   105,
     100,   108,   108,   108,   108,   108,   127,    59,    57,   114,
      50,   100,    70,    17,    99,    70,    17,    99,    70,    17,
      99,   110,   110,   110,   110,   110,   124,    70,   115,    59,
      99,    17,    17,    17,    17,   107,    28,    18,   117,    70,
     112,    17,    70,   115,    53,   119,    10,    28,    60,    18,
     113,   132,   117,    43,    44,    45,    46,    47,   100,   116,
     118,    70,   112,    16,   130,    10,    60,   113,    70,    73,
     118,    17,    28,    17,    70,    17
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    75,    76,    76,    77,    77,    77,    77,    77,    77,
      77,    77,    77,    77,    77,    77,    77,    77,    77,    77,
      77,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    88,    89,    90,    90,    91,    92,    93,
      93,    94,    94,    94,    94,    94,    94,    95,    96,    96,
      96,    96,    96,    97,    98,    99,    99,   100,   100,   100,
     100,   101,   102,   103,   104,   105,   106,   107,   107,   108,
     108,   108,   109,   109,   109,   109,   109,   109,   109,   109,
     109,   109,   109,   109,   109,   109,   109,   109,   110,   110,
     110,   110,   110,   110,   110,   110,   110,   110,   110,   110,
     110,   110,   110,   110,   111,   111,   112,   112,   112,   112,
     112,   112,   113,   113,   114,   114,   115,   115,   116,   116,
     116,   116,   116,   117,   117,   118,   118,   118,   118,   119,
     119,   120,   120,   121,   122,   122,   123,   123,   124,   124,
     125,   126,   127,   127,   127,   127,   127,   127,   127,   127,
     127,   127,   127,   127,   127,   127,   127,   127,   127,   127,
     127,   127,   127,   127,   127,   127,   128,   128,   129,   129,
     130,   130,   130,   130,   130,   130,   130,   130,   130,   130,
     130,   130,   130,   130,   131,   132,   132
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     0,     2,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     2,     2,     2,     2,     2,     2,     4,     3,
       3,     5,     9,    10,     2,     0,     3,     4,     8,     0,
       3,     5,     2,     6,     3,     7,     4,     1,     1,     1,
       1,     1,     1,     1,    10,     0,     3,     1,     1,     1,
       1,     5,     7,     1,     2,    10,     2,     0,     6,     0,
       1,     2,     2,     3,     5,     5,     6,     8,     6,     8,
       8,     6,     8,     6,     6,     6,     6,     8,     0,     7,
       9,     7,     9,     7,     9,     7,     9,     7,     7,     7,
       9,     4,     6,     6,     0,     4,     1,     2,     2,     3,
       4,     4,     0,     3,     0,     4,     1,     3,     1,     1,
       1,     1,     1,     0,     3,     4,     4,     6,     1,     0,
       4,     0,     5,     1,     0,     3,     1,     1,     0,     3,
       1,     1,     3,     3,     3,     3,     5,     5,     7,     4,
       5,     6,     8,     6,     8,     8,     6,     8,     6,     7,
       5,     7,     5,     5,     7,     7,     0,     3,     3,     5,
       1,     1,     1,     1,     1,     1,     1,     2,     2,     1,
       1,     2,     1,     2,     8,     0,     6
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (scanner, YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
# ifndef YY_LOCATION_PRINT
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif


# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value, scanner); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, void *scanner)
{
  FILE *yyoutput = yyo;
  YYUSE (yyoutput);
  YYUSE (scanner);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yykind < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yykind], *yyvaluep);
# endif
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, void *scanner)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep, scanner);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule, void *scanner)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)], scanner);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule, scanner); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep, void *scanner)
{
  YYUSE (yyvaluep);
  YYUSE (scanner);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}






/*----------.
| yyparse.  |
`----------*/

int
yyparse (void *scanner)
{
/* Lookahead token kind.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

    /* Number of syntax errors so far.  */
    int yynerrs = 0;

    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    goto yyexhaustedlab;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex (&yylval, scanner);
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 22: /* exit: EXIT SEMICOLON  */
#line 201 "yacc_sql.y"
                   {
        CONTEXT->ssql->flag=SCF_EXIT;//"exit";
    }
#line 1586 "yacc_sql.tab.c"
    break;

  case 23: /* help: HELP SEMICOLON  */
#line 206 "yacc_sql.y"
                   {
        CONTEXT->ssql->flag=SCF_HELP;//"help";
    }
#line 1594 "yacc_sql.tab.c"
    break;

  case 24: /* sync: SYNC SEMICOLON  */
#line 211 "yacc_sql.y"
                   {
      CONTEXT->ssql->flag = SCF_SYNC;
    }
#line 1602 "yacc_sql.tab.c"
    break;

  case 25: /* begin: TRX_BEGIN SEMICOLON  */
#line 217 "yacc_sql.y"
                        {
      CONTEXT->ssql->flag = SCF_BEGIN;
    }
#line 1610 "yacc_sql.tab.c"
    break;

  case 26: /* commit: TRX_COMMIT SEMICOLON  */
#line 223 "yacc_sql.y"
                         {
      CONTEXT->ssql->flag = SCF_COMMIT;
    }
#line 1618 "yacc_sql.tab.c"
    break;

  case 27: /* rollback: TRX_ROLLBACK SEMICOLON  */
#line 229 "yacc_sql.y"
                           {
      CONTEXT->ssql->flag = SCF_ROLLBACK;
    }
#line 1626 "yacc_sql.tab.c"
    break;

  case 28: /* drop_table: DROP TABLE ID SEMICOLON  */
#line 235 "yacc_sql.y"
                            {
        CONTEXT->ssql->flag = SCF_DROP_TABLE;//"drop_table";
        drop_table_init(&CONTEXT->ssql->sstr.drop_table, (yyvsp[-1].string));
    }
#line 1635 "yacc_sql.tab.c"
    break;

  case 29: /* show_tables: SHOW TABLES SEMICOLON  */
#line 241 "yacc_sql.y"
                          {
      CONTEXT->ssql->flag = SCF_SHOW_TABLES;
    }
#line 1643 "yacc_sql.tab.c"
    break;

  case 30: /* desc_table: DESC ID SEMICOLON  */
#line 247 "yacc_sql.y"
                      {
      CONTEXT->ssql->flag = SCF_DESC_TABLE;
      desc_table_init(&CONTEXT->ssql->sstr.desc_table, (yyvsp[-1].string));
    }
#line 1652 "yacc_sql.tab.c"
    break;

  case 31: /* show_index: SHOW INDEX FROM ID SEMICOLON  */
#line 254 "yacc_sql.y"
                                 {
      CONTEXT->ssql->flag = SCF_SHOW_INDEX;
      show_index_init(&CONTEXT->ssql->sstr.show_index, (yyvsp[-1].string));
    }
#line 1661 "yacc_sql.tab.c"
    break;

  case 32: /* create_index: CREATE INDEX ID ON ID LBRACE index_attr RBRACE SEMICOLON  */
#line 262 "yacc_sql.y"
                {
			CONTEXT->ssql->flag = SCF_CREATE_INDEX;//"create_index";
			create_index_init(&CONTEXT->ssql->sstr.create_index, (yyvsp[-6].string), (yyvsp[-4].string), false);
		}
#line 1670 "yacc_sql.tab.c"
    break;

  case 33: /* create_index: CREATE UNIQUE INDEX ID ON ID LBRACE index_attr RBRACE SEMICOLON  */
#line 267 "yacc_sql.y"
                {
			CONTEXT->ssql->flag = SCF_CREATE_INDEX;//"create_index";
			create_index_init(&CONTEXT->ssql->sstr.create_index, (yyvsp[-6].string), (yyvsp[-4].string), true);
		}
#line 1679 "yacc_sql.tab.c"
    break;

  case 34: /* index_attr: ID index_attr_list  */
#line 274 "yacc_sql.y"
                           {
		index_append_attribute(&CONTEXT->ssql->sstr.create_index, (yyvsp[-1].string));
	}
#line 1687 "yacc_sql.tab.c"
    break;

  case 36: /* index_attr_list: COMMA ID index_attr_list  */
#line 281 "yacc_sql.y"
                                   {
		index_append_attribute(&CONTEXT->ssql->sstr.create_index, (yyvsp[-1].string));
	}
#line 1695 "yacc_sql.tab.c"
    break;

  case 37: /* drop_index: DROP INDEX ID SEMICOLON  */
#line 288 "yacc_sql.y"
                {
			CONTEXT->ssql->flag=SCF_DROP_INDEX;//"drop_index";
			drop_index_init(&CONTEXT->ssql->sstr.drop_index, (yyvsp[-1].string));
		}
#line 1704 "yacc_sql.tab.c"
    break;

  case 38: /* create_table: CREATE TABLE ID LBRACE attr_def attr_def_list RBRACE SEMICOLON  */
#line 295 "yacc_sql.y"
                {
			CONTEXT->ssql->flag=SCF_CREATE_TABLE;//"create_table";
			// CONTEXT->ssql->sstr.create_table.attribute_count = CONTEXT->value_length;
			create_table_init_name(&CONTEXT->ssql->sstr.create_table, (yyvsp[-5].string));
			//临时变量清零	
			CONTEXT->value_length = 0;
		}
#line 1716 "yacc_sql.tab.c"
    break;

  case 40: /* attr_def_list: COMMA attr_def attr_def_list  */
#line 305 "yacc_sql.y"
                                   {    }
#line 1722 "yacc_sql.tab.c"
    break;

  case 41: /* attr_def: ID_get type LBRACE number RBRACE  */
#line 310 "yacc_sql.y"
                {
			AttrInfo attribute;
			attr_info_init(&attribute, CONTEXT->id, (yyvsp[-3].number), (yyvsp[-1].number), false);
			create_table_append_attribute(&CONTEXT->ssql->sstr.create_table, &attribute);
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].name =(char*)malloc(sizeof(char));
			// strcpy(CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].name, CONTEXT->id); 
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].type = $2;  
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].length = $4;
			CONTEXT->value_length++;
		}
#line 1737 "yacc_sql.tab.c"
    break;

  case 42: /* attr_def: ID_get type  */
#line 321 "yacc_sql.y"
                {
			AttrInfo attribute;
			attr_info_init(&attribute, CONTEXT->id, (yyvsp[0].number), 4, false);
			create_table_append_attribute(&CONTEXT->ssql->sstr.create_table, &attribute);
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].name=(char*)malloc(sizeof(char));
			// strcpy(CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].name, CONTEXT->id); 
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].type=$2;  
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].length=4; // default attribute length
			CONTEXT->value_length++;
		}
#line 1752 "yacc_sql.tab.c"
    break;

  case 43: /* attr_def: ID_get type LBRACE number RBRACE NULLABLE  */
#line 332 "yacc_sql.y"
                {
			AttrInfo attribute;
			attr_info_init(&attribute, CONTEXT->id, (yyvsp[-4].number), (yyvsp[-2].number), true);
			create_table_append_attribute(&CONTEXT->ssql->sstr.create_table, &attribute);
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].name =(char*)malloc(sizeof(char));
			// strcpy(CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].name, CONTEXT->id); 
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].type = $2;  
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].length = $4;
			CONTEXT->value_length++;
		}
#line 1767 "yacc_sql.tab.c"
    break;

  case 44: /* attr_def: ID_get type NULLABLE  */
#line 343 "yacc_sql.y"
                {
			AttrInfo attribute;
			attr_info_init(&attribute, CONTEXT->id, (yyvsp[-1].number), 4, true);
			create_table_append_attribute(&CONTEXT->ssql->sstr.create_table, &attribute);
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].name=(char*)malloc(sizeof(char));
			// strcpy(CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].name, CONTEXT->id); 
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].type=$2;  
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].length=4; // default attribute length
			CONTEXT->value_length++;
		}
#line 1782 "yacc_sql.tab.c"
    break;

  case 45: /* attr_def: ID_get type LBRACE number RBRACE NOT NULL_V  */
#line 354 "yacc_sql.y"
                {
			AttrInfo attribute;
			attr_info_init(&attribute, CONTEXT->id, (yyvsp[-5].number), (yyvsp[-3].number), false);
			create_table_append_attribute(&CONTEXT->ssql->sstr.create_table, &attribute);
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].name =(char*)malloc(sizeof(char));
			// strcpy(CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].name, CONTEXT->id); 
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].type = $2;  
			// CONTEXT->ssql->sstr.create_table.attributes[CONTEXT->value_length].length = $4;
			CONTEXT->value_length++;
		}
#line 1797 "yacc_sql.tab.c"
    break;

  case 46: /* attr_def: ID_get type NOT NULL_V  */
#line 365 "yacc_sql.y"
                {
			AttrInfo attribute;
			attr_info_init(&attribute, CONTEXT->id, (yyvsp[-2].number), 4, false);
			create_table_append_attribute(&CONTEXT->ssql->sstr.create_table, &attribute);
			CONTEXT->value_length++;
		}
#line 1808 "yacc_sql.tab.c"
    break;

  case 47: /* number: NUMBER  */
#line 373 "yacc_sql.y"
                       {(yyval.number) = (yyvsp[0].number);}
#line 1814 "yacc_sql.tab.c"
    break;

  case 48: /* type: INT_T  */
#line 376 "yacc_sql.y"
              { (yyval.number)=INTS; }
#line 1820 "yacc_sql.tab.c"
    break;

  case 49: /* type: STRING_T  */
#line 377 "yacc_sql.y"
                  { (yyval.number)=CHARS; }
#line 1826 "yacc_sql.tab.c"
    break;

  case 50: /* type: FLOAT_T  */
#line 378 "yacc_sql.y"
                 { (yyval.number)=FLOATS; }
#line 1832 "yacc_sql.tab.c"
    break;

  case 51: /* type: DATE_T  */
#line 379 "yacc_sql.y"
                { (yyval.number)=DATES; }
#line 1838 "yacc_sql.tab.c"
    break;

  case 52: /* type: TEXT_T  */
#line 380 "yacc_sql.y"
                { (yyval.number)=TEXTS; }
#line 1844 "yacc_sql.tab.c"
    break;

  case 53: /* ID_get: ID  */
#line 384 "yacc_sql.y"
        {
		char *temp=(yyvsp[0].string); 
		snprintf(CONTEXT->id, sizeof(CONTEXT->id), "%s", temp);
		free(temp);
	}
#line 1854 "yacc_sql.tab.c"
    break;

  case 54: /* insert: INSERT INTO ID VALUES LBRACE value value_list RBRACE tuple_list SEMICOLON  */
#line 394 "yacc_sql.y"
                {
			// CONTEXT->values[CONTEXT->value_length++] = *$6;

			CONTEXT->ssql->flag=SCF_INSERT;//"insert";
			// CONTEXT->ssql->sstr.insertion.relation_name = $3;
			// CONTEXT->ssql->sstr.insertion.value_num = CONTEXT->value_length;
			// for(i = 0; i < CONTEXT->value_length; i++){
			// 	CONTEXT->ssql->sstr.insertion.values[i] = CONTEXT->values[i];
      // }
			inserts_init(&CONTEXT->ssql->sstr.insertion, (yyvsp[-7].string), CONTEXT->values, CONTEXT->value_length);

      //临时变量清零
      CONTEXT->value_length=0;
    }
#line 1873 "yacc_sql.tab.c"
    break;

  case 56: /* value_list: COMMA value value_list  */
#line 411 "yacc_sql.y"
                              { 
  		// CONTEXT->values[CONTEXT->value_length++] = *$2;
	  }
#line 1881 "yacc_sql.tab.c"
    break;

  case 57: /* value: NUMBER  */
#line 416 "yacc_sql.y"
          {	
  		value_init_integer(&CONTEXT->values[CONTEXT->value_length++], (yyvsp[0].number));
		}
#line 1889 "yacc_sql.tab.c"
    break;

  case 58: /* value: FLOAT  */
#line 419 "yacc_sql.y"
          {
  		value_init_float(&CONTEXT->values[CONTEXT->value_length++], (yyvsp[0].floats));
		}
#line 1897 "yacc_sql.tab.c"
    break;

  case 59: /* value: SSS  */
#line 422 "yacc_sql.y"
         {
		(yyvsp[0].string) = substr((yyvsp[0].string),1,strlen((yyvsp[0].string))-2);
  		value_init_string(&CONTEXT->values[CONTEXT->value_length++], (yyvsp[0].string));
		}
#line 1906 "yacc_sql.tab.c"
    break;

  case 60: /* value: NULL_V  */
#line 426 "yacc_sql.y"
                {
		value_init_null(&CONTEXT->values[CONTEXT->value_length++]);
	}
#line 1914 "yacc_sql.tab.c"
    break;

  case 61: /* delete: DELETE FROM ID where SEMICOLON  */
#line 433 "yacc_sql.y"
                {
			CONTEXT->ssql->flag = SCF_DELETE;//"delete";
			deletes_init_relation(&CONTEXT->ssql->sstr.deletion, (yyvsp[-2].string));
			deletes_set_conditions(&CONTEXT->ssql->sstr.deletion, 
					CONTEXT->conditions, CONTEXT->condition_length);
			CONTEXT->condition_length = 0;	
    }
#line 1926 "yacc_sql.tab.c"
    break;

  case 62: /* update: UPDATE ID SET eq_comp eq_comp_list where SEMICOLON  */
#line 443 "yacc_sql.y"
                {
			CONTEXT->ssql->flag = SCF_UPDATE;//"update";
			updates_init(&CONTEXT->ssql->sstr.update, (yyvsp[-5].string), CONTEXT->set_conditions, CONTEXT->set_condition_length, 
					CONTEXT->conditions, CONTEXT->condition_length);
			CONTEXT->condition_length = 0;
		}
#line 1937 "yacc_sql.tab.c"
    break;

  case 63: /* select_flag: SELECT  */
#line 452 "yacc_sql.y"
        {
		if(CONTEXT->select_length == 0) {
			CONTEXT->selects[0] = &(CONTEXT->ssql->sstr.selection);
		} else {
			select_init(&(CONTEXT->selects[CONTEXT->select_length]));
		}
	}
#line 1949 "yacc_sql.tab.c"
    break;

  case 64: /* select_table: select SEMICOLON  */
#line 462 "yacc_sql.y"
        {
		selects_append_conditions(&CONTEXT->ssql->sstr.selection, CONTEXT->conditions, CONTEXT->condition_length);
		selects_append_groups(&CONTEXT->ssql->sstr.selection, CONTEXT->groups, CONTEXT->group_length);
		selects_append_having_condition(&CONTEXT->ssql->sstr.selection, &CONTEXT->having_condition, CONTEXT->is_having);
	}
#line 1959 "yacc_sql.tab.c"
    break;

  case 65: /* select: select_flag select_attr FROM ID alias rel_list join_list where groups orders  */
#line 470 "yacc_sql.y"
        {
		if (strcmp((yyvsp[-5].string), "")) {
			selects_append_table_alias(CONTEXT->selects[CONTEXT->select_length], (yyvsp[-6].string), (yyvsp[-5].string));
		}
		selects_append_relation(CONTEXT->selects[CONTEXT->select_length], (yyvsp[-6].string));

		CONTEXT->ssql->flag=SCF_SELECT;//"select";

	}
#line 1973 "yacc_sql.tab.c"
    break;

  case 66: /* join_flag: INNER JOIN  */
#line 481 "yacc_sql.y"
                   {
		CONTEXT->is_and[CONTEXT->cur_condition]=true;
		CONTEXT->cur_condition++;
	}
#line 1982 "yacc_sql.tab.c"
    break;

  case 68: /* join_list: join_flag ID ON condition condition_list join_list  */
#line 489 "yacc_sql.y"
                {
 			selects_append_relation(CONTEXT->selects[CONTEXT->select_length], (yyvsp[-4].string));
 		}
#line 1990 "yacc_sql.tab.c"
    break;

  case 69: /* alias: %empty  */
#line 494 "yacc_sql.y"
                    { (yyval.string) = ""; }
#line 1996 "yacc_sql.tab.c"
    break;

  case 70: /* alias: ID  */
#line 495 "yacc_sql.y"
             {
		(yyval.string) = (yyvsp[0].string);
	}
#line 2004 "yacc_sql.tab.c"
    break;

  case 71: /* alias: AS ID  */
#line 498 "yacc_sql.y"
                {
		(yyval.string) = (yyvsp[0].string);
	}
#line 2012 "yacc_sql.tab.c"
    break;

  case 72: /* select_attr: STAR attr_list  */
#line 503 "yacc_sql.y"
                   {  
			RelAttr attr;
			relation_attr_init(&attr, NULL, strdup("*"), false, 5, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		}
#line 2022 "yacc_sql.tab.c"
    break;

  case 73: /* select_attr: ID alias attr_list  */
#line 508 "yacc_sql.y"
                         {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, (yyvsp[-2].string), (yyvsp[-1].string));
			}

			RelAttr attr;
			relation_attr_init(&attr, NULL, (yyvsp[-2].string), false, 5, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		}
#line 2036 "yacc_sql.tab.c"
    break;

  case 74: /* select_attr: ID DOT ID alias attr_list  */
#line 517 "yacc_sql.y"
                                    {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], (yyvsp[-4].string), (yyvsp[-2].string), (yyvsp[-1].string));
			}

			RelAttr attr;
			relation_attr_init(&attr, (yyvsp[-4].string), (yyvsp[-2].string), false, 5, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		}
#line 2050 "yacc_sql.tab.c"
    break;

  case 75: /* select_attr: ID DOT STAR alias attr_list  */
#line 526 "yacc_sql.y"
                                      {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], (yyvsp[-4].string), (yyvsp[-2].string), (yyvsp[-1].string));
			}

			RelAttr attr;
			relation_attr_init(&attr, (yyvsp[-4].string), strdup("*"), false, 5, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		}
#line 2064 "yacc_sql.tab.c"
    break;

  case 76: /* select_attr: MAX LBRACE ID RBRACE alias attr_list  */
#line 535 "yacc_sql.y"
                                               {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, (yyvsp[-3].string), (yyvsp[-1].string));
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, (yyvsp[-3].string), MAXIMUM);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, NULL, (yyvsp[-3].string), true, MAXIMUM, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		}
#line 2081 "yacc_sql.tab.c"
    break;

  case 77: /* select_attr: MAX LBRACE ID DOT ID RBRACE alias attr_list  */
#line 547 "yacc_sql.y"
                                                      {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], (yyvsp[-5].string), (yyvsp[-3].string), (yyvsp[-1].string));
			}
			
			RelAggr aggr;
			relation_aggr_init(&aggr, (yyvsp[-5].string), (yyvsp[-3].string), MAXIMUM);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, (yyvsp[-5].string), (yyvsp[-3].string), true, MAXIMUM, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		}
#line 2098 "yacc_sql.tab.c"
    break;

  case 78: /* select_attr: MIN LBRACE ID RBRACE alias attr_list  */
#line 559 "yacc_sql.y"
                                               {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, (yyvsp[-3].string), (yyvsp[-1].string));
			}
				
			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, (yyvsp[-3].string), MINIMUM);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, NULL, (yyvsp[-3].string), true, MINIMUM, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
#line 2116 "yacc_sql.tab.c"
    break;

  case 79: /* select_attr: MIN LBRACE ID DOT ID RBRACE alias attr_list  */
#line 572 "yacc_sql.y"
                                                      {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], (yyvsp[-5].string), (yyvsp[-3].string), (yyvsp[-1].string));
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, (yyvsp[-5].string), (yyvsp[-3].string), MINIMUM);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, (yyvsp[-5].string), (yyvsp[-3].string), true, MINIMUM, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
#line 2134 "yacc_sql.tab.c"
    break;

  case 80: /* select_attr: AVG LBRACE ID DOT ID RBRACE alias attr_list  */
#line 585 "yacc_sql.y"
                                                      {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], (yyvsp[-5].string), (yyvsp[-3].string), (yyvsp[-1].string));
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, (yyvsp[-5].string), (yyvsp[-3].string), AVERAGE);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, (yyvsp[-5].string), (yyvsp[-3].string), true, AVERAGE, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
#line 2152 "yacc_sql.tab.c"
    break;

  case 81: /* select_attr: AVG LBRACE ID RBRACE alias attr_list  */
#line 598 "yacc_sql.y"
                                               {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, (yyvsp[-3].string), (yyvsp[-1].string));
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, (yyvsp[-3].string), AVERAGE);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, NULL, (yyvsp[-3].string), true, AVERAGE, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
#line 2170 "yacc_sql.tab.c"
    break;

  case 82: /* select_attr: SUM LBRACE ID DOT ID RBRACE alias attr_list  */
#line 611 "yacc_sql.y"
                                                      {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], (yyvsp[-5].string), (yyvsp[-3].string), (yyvsp[-1].string));
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, (yyvsp[-5].string), (yyvsp[-3].string), SUMMARY);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, (yyvsp[-5].string), (yyvsp[-3].string), true, SUMMARY, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
#line 2188 "yacc_sql.tab.c"
    break;

  case 83: /* select_attr: SUM LBRACE ID RBRACE alias attr_list  */
#line 624 "yacc_sql.y"
                                               {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, (yyvsp[-3].string), (yyvsp[-1].string));
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, (yyvsp[-3].string), SUMMARY);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, NULL, (yyvsp[-3].string), true, SUMMARY, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
#line 2206 "yacc_sql.tab.c"
    break;

  case 84: /* select_attr: CNT LBRACE STAR RBRACE alias attr_list  */
#line 637 "yacc_sql.y"
                                                 {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, "*", (yyvsp[-1].string));
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, "*", COUNT);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, NULL, strdup("*"), true, COUNT, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
#line 2224 "yacc_sql.tab.c"
    break;

  case 85: /* select_attr: CNT LBRACE number RBRACE alias attr_list  */
#line 650 "yacc_sql.y"
                                                   {
			RelAggr aggr;
			char number[12];
			int i = 0;
			while ((yyvsp[-3].number)) {
				number[i++] = '0' + (yyvsp[-3].number) % 10;
				(yyvsp[-3].number) = (yyvsp[-3].number) / 10;
			}
			number[i--] = '\0';
			for (int j = 0; j < i; j++, i--) {
				char c = number[j];
				number[j] = number[i];
				number[i] = c;
			}
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, number, (yyvsp[-1].string));
			}

			relation_aggr_init(&aggr, NULL, number, COUNT);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
		}
#line 2250 "yacc_sql.tab.c"
    break;

  case 86: /* select_attr: CNT LBRACE ID RBRACE alias attr_list  */
#line 671 "yacc_sql.y"
                                               {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, (yyvsp[-3].string), (yyvsp[-1].string));
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, (yyvsp[-3].string), COUNT);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, NULL, (yyvsp[-3].string), true, COUNT, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
#line 2268 "yacc_sql.tab.c"
    break;

  case 87: /* select_attr: CNT LBRACE ID DOT ID RBRACE alias attr_list  */
#line 684 "yacc_sql.y"
                                                      {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], (yyvsp[-5].string), (yyvsp[-3].string), (yyvsp[-1].string));
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, (yyvsp[-5].string), (yyvsp[-3].string), COUNT);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, (yyvsp[-5].string), (yyvsp[-3].string), true, COUNT, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
#line 2286 "yacc_sql.tab.c"
    break;

  case 89: /* attr_list: COMMA MAX LBRACE ID RBRACE alias attr_list  */
#line 700 "yacc_sql.y"
                                                     {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, (yyvsp[-3].string), (yyvsp[-1].string));
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, (yyvsp[-3].string), MAXIMUM);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, NULL, (yyvsp[-3].string), true, MAXIMUM, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
#line 2304 "yacc_sql.tab.c"
    break;

  case 90: /* attr_list: COMMA MAX LBRACE ID DOT ID RBRACE alias attr_list  */
#line 713 "yacc_sql.y"
                                                            {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], (yyvsp[-5].string), (yyvsp[-3].string), (yyvsp[-1].string));
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, (yyvsp[-5].string), (yyvsp[-3].string), MAXIMUM);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, (yyvsp[-5].string), (yyvsp[-3].string), true, MAXIMUM, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
#line 2322 "yacc_sql.tab.c"
    break;

  case 91: /* attr_list: COMMA MIN LBRACE ID RBRACE alias attr_list  */
#line 726 "yacc_sql.y"
                                                     {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, (yyvsp[-3].string), (yyvsp[-1].string));
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, (yyvsp[-3].string), MINIMUM);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, NULL, (yyvsp[-3].string), true, MINIMUM, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
#line 2340 "yacc_sql.tab.c"
    break;

  case 92: /* attr_list: COMMA MIN LBRACE ID DOT ID RBRACE alias attr_list  */
#line 739 "yacc_sql.y"
                                                            {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], (yyvsp[-5].string), (yyvsp[-3].string), (yyvsp[-1].string));
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, (yyvsp[-5].string), (yyvsp[-3].string), MINIMUM);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, (yyvsp[-5].string), (yyvsp[-3].string), true, MINIMUM, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
#line 2358 "yacc_sql.tab.c"
    break;

  case 93: /* attr_list: COMMA AVG LBRACE ID RBRACE alias attr_list  */
#line 752 "yacc_sql.y"
                                                     {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, (yyvsp[-3].string), (yyvsp[-1].string));
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, (yyvsp[-3].string), AVERAGE);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, NULL, (yyvsp[-3].string), true, AVERAGE, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
#line 2376 "yacc_sql.tab.c"
    break;

  case 94: /* attr_list: COMMA AVG LBRACE ID DOT ID RBRACE alias attr_list  */
#line 765 "yacc_sql.y"
                                                            {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], (yyvsp[-5].string), (yyvsp[-3].string), (yyvsp[-1].string));
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, (yyvsp[-5].string), (yyvsp[-3].string), AVERAGE);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, (yyvsp[-5].string), (yyvsp[-3].string), true, AVERAGE, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		}
#line 2393 "yacc_sql.tab.c"
    break;

  case 95: /* attr_list: COMMA SUM LBRACE ID RBRACE alias attr_list  */
#line 777 "yacc_sql.y"
                                                     {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, (yyvsp[-3].string), (yyvsp[-1].string));
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, (yyvsp[-3].string), SUMMARY);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, NULL, (yyvsp[-3].string), true, SUMMARY, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
#line 2411 "yacc_sql.tab.c"
    break;

  case 96: /* attr_list: COMMA SUM LBRACE ID DOT ID RBRACE alias attr_list  */
#line 790 "yacc_sql.y"
                                                            {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], (yyvsp[-5].string), (yyvsp[-3].string), (yyvsp[-1].string));
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, (yyvsp[-5].string), (yyvsp[-3].string), SUMMARY);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, (yyvsp[-5].string), (yyvsp[-3].string), true, SUMMARY, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
#line 2429 "yacc_sql.tab.c"
    break;

  case 97: /* attr_list: COMMA CNT LBRACE STAR RBRACE alias attr_list  */
#line 803 "yacc_sql.y"
                                                       {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, "*", (yyvsp[-1].string));
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, "*", COUNT);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			
			RelAttr attr;
			relation_attr_init(&attr, NULL, strdup("*"), true, COUNT, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		}
#line 2447 "yacc_sql.tab.c"
    break;

  case 98: /* attr_list: COMMA CNT LBRACE number RBRACE alias attr_list  */
#line 816 "yacc_sql.y"
                                                         {
			RelAggr aggr;
			char number[12];
			int i = 0;
			while ((yyvsp[-3].number)) {
				number[i++] = '0' + (yyvsp[-3].number) % 10;
				(yyvsp[-3].number) = (yyvsp[-3].number) / 10;
			}
			number[i--] = '\0';
			for (int j = 0; j < i; j++, i--) {
				char c = number[j];
				number[j] = number[i];
				number[i] = c;
			}
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, number, (yyvsp[-1].string));
			}

			relation_aggr_init(&aggr, NULL, number, COUNT);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
		}
#line 2473 "yacc_sql.tab.c"
    break;

  case 99: /* attr_list: COMMA CNT LBRACE ID RBRACE alias attr_list  */
#line 837 "yacc_sql.y"
                                                     {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, (yyvsp[-3].string), (yyvsp[-1].string));
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, NULL, (yyvsp[-3].string), COUNT);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, NULL, (yyvsp[-3].string), true, COUNT, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
#line 2491 "yacc_sql.tab.c"
    break;

  case 100: /* attr_list: COMMA CNT LBRACE ID DOT ID RBRACE alias attr_list  */
#line 850 "yacc_sql.y"
                                                            {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], (yyvsp[-5].string), (yyvsp[-3].string), (yyvsp[-1].string));
			}

			RelAggr aggr;
			relation_aggr_init(&aggr, (yyvsp[-5].string), (yyvsp[-3].string), COUNT);
			selects_append_aggragation(CONTEXT->selects[CONTEXT->select_length], &aggr);
			RelAttr attr;
			relation_attr_init(&attr, (yyvsp[-5].string), (yyvsp[-3].string), true, COUNT, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
			
		}
#line 2509 "yacc_sql.tab.c"
    break;

  case 101: /* attr_list: COMMA ID alias attr_list  */
#line 863 "yacc_sql.y"
                               {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], NULL, (yyvsp[-2].string), (yyvsp[-1].string));
			}
			RelAttr attr;
			relation_attr_init(&attr, NULL, (yyvsp[-2].string), false, 5, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
     	  // CONTEXT->ssql->sstr.selection.attributes[CONTEXT->select_length].relation_name = NULL;
        // CONTEXT->ssql->sstr.selection.attributes[CONTEXT->select_length++].attribute_name=$2;
      }
#line 2524 "yacc_sql.tab.c"
    break;

  case 102: /* attr_list: COMMA ID DOT ID alias attr_list  */
#line 873 "yacc_sql.y"
                                      {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], (yyvsp[-4].string), (yyvsp[-2].string), (yyvsp[-1].string));
			}
			RelAttr attr;
			relation_attr_init(&attr, (yyvsp[-4].string), (yyvsp[-2].string), false, 5, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
        // CONTEXT->ssql->sstr.selection.attributes[CONTEXT->select_length].attribute_name=$4;
        // CONTEXT->ssql->sstr.selection.attributes[CONTEXT->select_length++].relation_name=$2;
  	  }
#line 2539 "yacc_sql.tab.c"
    break;

  case 103: /* attr_list: COMMA ID DOT STAR alias attr_list  */
#line 883 "yacc_sql.y"
                                            {
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_column_alias(CONTEXT->selects[CONTEXT->select_length], (yyvsp[-4].string), (yyvsp[-2].string), (yyvsp[-1].string));
			}
			RelAttr attr;
			relation_attr_init(&attr, (yyvsp[-4].string), strdup("*"), false, 5, SELECTFIELD);
			selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
  	  }
#line 2552 "yacc_sql.tab.c"
    break;

  case 105: /* rel_list: COMMA ID alias rel_list  */
#line 895 "yacc_sql.y"
                              {	
			if (strcmp((yyvsp[-1].string), "")) {
				selects_append_table_alias(CONTEXT->selects[CONTEXT->select_length], (yyvsp[-2].string), (yyvsp[-1].string));
			}	
			selects_append_relation(CONTEXT->selects[CONTEXT->select_length], (yyvsp[-2].string));
		  }
#line 2563 "yacc_sql.tab.c"
    break;

  case 106: /* order: ID  */
#line 903 "yacc_sql.y"
           {
		RelAttr attr;
		relation_attr_init(&attr, NULL, (yyvsp[0].string), false, 5, ORDERFIELD);
		selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		Order order;
		order_init(&order, true);
		selects_append_order(CONTEXT->selects[CONTEXT->select_length], &order);
	}
#line 2576 "yacc_sql.tab.c"
    break;

  case 107: /* order: ID ASC  */
#line 911 "yacc_sql.y"
                 {
		RelAttr attr;
		relation_attr_init(&attr, NULL, (yyvsp[-1].string), false, 5, ORDERFIELD);
		selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		Order order;
		order_init(&order, true);
		selects_append_order(CONTEXT->selects[CONTEXT->select_length], &order);
	}
#line 2589 "yacc_sql.tab.c"
    break;

  case 108: /* order: ID DESC  */
#line 919 "yacc_sql.y"
                  {
		RelAttr attr;
		relation_attr_init(&attr, NULL, (yyvsp[-1].string), false, 5, ORDERFIELD);
		selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		Order order;
		order_init(&order, false);
		selects_append_order(CONTEXT->selects[CONTEXT->select_length], &order);
	}
#line 2602 "yacc_sql.tab.c"
    break;

  case 109: /* order: ID DOT ID  */
#line 927 "yacc_sql.y"
                    {
		RelAttr attr;
		relation_attr_init(&attr, (yyvsp[-2].string), (yyvsp[0].string), false, 5, ORDERFIELD);
		selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		Order order;
		order_init(&order, true);
		selects_append_order(CONTEXT->selects[CONTEXT->select_length], &order);
	}
#line 2615 "yacc_sql.tab.c"
    break;

  case 110: /* order: ID DOT ID ASC  */
#line 935 "yacc_sql.y"
                        {
		RelAttr attr;
		relation_attr_init(&attr, (yyvsp[-3].string), (yyvsp[-1].string), false, 5, ORDERFIELD);
		selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		Order order;
		order_init(&order, true);
		selects_append_order(CONTEXT->selects[CONTEXT->select_length], &order);
	}
#line 2628 "yacc_sql.tab.c"
    break;

  case 111: /* order: ID DOT ID DESC  */
#line 943 "yacc_sql.y"
                         {
		RelAttr attr;
		relation_attr_init(&attr, (yyvsp[-3].string), (yyvsp[-1].string), false, 5, ORDERFIELD);
		selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		Order order;
		order_init(&order, false);
		selects_append_order(CONTEXT->selects[CONTEXT->select_length], &order);
	}
#line 2641 "yacc_sql.tab.c"
    break;

  case 116: /* group: ID  */
#line 961 "yacc_sql.y"
           {
		RelAttr attr;
		relation_attr_init(&attr, NULL, (yyvsp[0].string), false, 5, GROUPFIELD);
		selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		if(CONTEXT->select_length == 0) {
			CONTEXT->groups[CONTEXT->group_length++] = attr;
		} else {
			selects_append_group(CONTEXT->selects[CONTEXT->select_length], &attr);
		}
	}
#line 2656 "yacc_sql.tab.c"
    break;

  case 117: /* group: ID DOT ID  */
#line 971 "yacc_sql.y"
                    {
		RelAttr attr;
		relation_attr_init(&attr, (yyvsp[-2].string), (yyvsp[0].string), false, 5, GROUPFIELD);
		selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		if(CONTEXT->select_length == 0) {
			CONTEXT->groups[CONTEXT->group_length++] = attr;
		} else {
			selects_append_group(CONTEXT->selects[CONTEXT->select_length], &attr);
		}
	}
#line 2671 "yacc_sql.tab.c"
    break;

  case 118: /* aggr_flag: MAX  */
#line 984 "yacc_sql.y"
            {
		CONTEXT->cur_agg = MAXIMUM;
	}
#line 2679 "yacc_sql.tab.c"
    break;

  case 119: /* aggr_flag: MIN  */
#line 987 "yacc_sql.y"
              {
		CONTEXT->cur_agg = MINIMUM;
	}
#line 2687 "yacc_sql.tab.c"
    break;

  case 120: /* aggr_flag: AVG  */
#line 990 "yacc_sql.y"
              {
		CONTEXT->cur_agg = AVERAGE;
	}
#line 2695 "yacc_sql.tab.c"
    break;

  case 121: /* aggr_flag: SUM  */
#line 993 "yacc_sql.y"
              {
		CONTEXT->cur_agg = SUMMARY;
	}
#line 2703 "yacc_sql.tab.c"
    break;

  case 122: /* aggr_flag: CNT  */
#line 996 "yacc_sql.y"
              {
		CONTEXT->cur_agg = COUNT;
	}
#line 2711 "yacc_sql.tab.c"
    break;

  case 125: /* part: aggr_flag LBRACE ID RBRACE  */
#line 1004 "yacc_sql.y"
                                   {
		RelAttr attr;
		relation_attr_init(&attr, NULL, (yyvsp[-1].string), true, CONTEXT->cur_agg, HAVINGFIELD);
		selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		if(!CONTEXT->is_right) {
			HavingCondition condition;
			having_condition_init(&condition, 6, &attr, NULL, CONTEXT->cur_agg);
			CONTEXT->having_condition = condition;
		} else {
			having_condition_append(&CONTEXT->having_condition, 6, &attr, NULL, CONTEXT->cur_agg);
		}
		CONTEXT->is_right = !CONTEXT->is_right;
	}
#line 2729 "yacc_sql.tab.c"
    break;

  case 126: /* part: aggr_flag LBRACE STAR RBRACE  */
#line 1017 "yacc_sql.y"
                                       {
		RelAttr attr;
		relation_attr_init(&attr, NULL, strdup("*"), true, CONTEXT->cur_agg, HAVINGFIELD);
		selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		if(!CONTEXT->is_right) {
			HavingCondition condition;
			having_condition_init(&condition, 6, &attr, NULL, CONTEXT->cur_agg);
			CONTEXT->having_condition = condition;
		} else {
			having_condition_append(&CONTEXT->having_condition, 6, &attr, NULL, CONTEXT->cur_agg);
		}
		CONTEXT->is_right = !CONTEXT->is_right;
	}
#line 2747 "yacc_sql.tab.c"
    break;

  case 127: /* part: aggr_flag LBRACE ID DOT ID RBRACE  */
#line 1030 "yacc_sql.y"
                                            {
		RelAttr attr;
		relation_attr_init(&attr, (yyvsp[-3].string), (yyvsp[-1].string), true, CONTEXT->cur_agg, HAVINGFIELD);
		selects_append_attribute(CONTEXT->selects[CONTEXT->select_length], &attr);
		if(!CONTEXT->is_right) {
			HavingCondition condition;
			having_condition_init(&condition, 6, &attr, NULL, CONTEXT->cur_agg);
			CONTEXT->having_condition = condition;
		} else {
			having_condition_append(&CONTEXT->having_condition, 6, &attr, NULL, CONTEXT->cur_agg);
		}
		CONTEXT->is_right = !CONTEXT->is_right;
	}
#line 2765 "yacc_sql.tab.c"
    break;

  case 128: /* part: value  */
#line 1043 "yacc_sql.y"
                {
		Value *value = &CONTEXT->values[CONTEXT->value_length - 1];
		if(!CONTEXT->is_right) {
			HavingCondition condition;
			having_condition_init(&condition, 2, NULL, value, CONTEXT->cur_agg);
			CONTEXT->having_condition = condition;
		} else {
			having_condition_append(&CONTEXT->having_condition, 2, NULL, value, CONTEXT->cur_agg);
		}
		CONTEXT->is_right = !CONTEXT->is_right;
	}
#line 2781 "yacc_sql.tab.c"
    break;

  case 130: /* having: HAVING part comOp part  */
#line 1057 "yacc_sql.y"
                                 {
		CONTEXT->is_having = true;
		having_condition_append_comp(&CONTEXT->having_condition, (yyvsp[-1].number));
	}
#line 2790 "yacc_sql.tab.c"
    break;

  case 133: /* where_flag: WHERE  */
#line 1067 "yacc_sql.y"
              {
		CONTEXT->is_and[CONTEXT->cur_condition]=true;
		CONTEXT->cur_condition++;
	}
#line 2799 "yacc_sql.tab.c"
    break;

  case 135: /* where: where_flag condition condition_list  */
#line 1074 "yacc_sql.y"
                                          {	
			}
#line 2806 "yacc_sql.tab.c"
    break;

  case 136: /* and_or: AND  */
#line 1078 "yacc_sql.y"
            {
		CONTEXT->is_and[CONTEXT->cur_condition]=true;
		CONTEXT->cur_condition++;
	}
#line 2815 "yacc_sql.tab.c"
    break;

  case 137: /* and_or: OR  */
#line 1082 "yacc_sql.y"
             {
		CONTEXT->is_and[CONTEXT->cur_condition]=false;
		CONTEXT->cur_condition++;
	}
#line 2824 "yacc_sql.tab.c"
    break;

  case 139: /* condition_list: and_or condition condition_list  */
#line 1089 "yacc_sql.y"
                                      {
			}
#line 2831 "yacc_sql.tab.c"
    break;

  case 140: /* subquery_start: LBRACE  */
#line 1094 "yacc_sql.y"
        {
		CONTEXT->selects[CONTEXT->select_length++];
	}
#line 2839 "yacc_sql.tab.c"
    break;

  case 141: /* sublist_start: LBRACE  */
#line 1100 "yacc_sql.y"
        {
		CONTEXT->value_start = CONTEXT->value_length;
	}
#line 2847 "yacc_sql.tab.c"
    break;

  case 142: /* condition: ID comOp value  */
#line 1106 "yacc_sql.y"
                {
			RelAttr left_attr;
			relation_attr_init(&left_attr, NULL, (yyvsp[-2].string), false, 5, CONDITIONFIELD);

			Value *right_value = &CONTEXT->values[CONTEXT->value_length - 1];

			Condition condition;
			condition_init(&condition, (yyvsp[-1].number), 1, &left_attr, NULL, NULL, NULL, 2, NULL, right_value, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
			CONTEXT->cur_condition--;
			if(CONTEXT->select_length == 0) {
				CONTEXT->conditions[CONTEXT->condition_length++] = condition;
			} else {
				selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
			}
		}
#line 2867 "yacc_sql.tab.c"
    break;

  case 143: /* condition: value comOp value  */
#line 1122 "yacc_sql.y"
                {
			Value *left_value = &CONTEXT->values[CONTEXT->value_length - 2];
			Value *right_value = &CONTEXT->values[CONTEXT->value_length - 1];
			
			Condition condition;
			condition_init(&condition, (yyvsp[-1].number), 2, NULL, left_value, NULL, NULL, 2, NULL, right_value, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
			CONTEXT->cur_condition--;
			if(CONTEXT->select_length == 0) {
				CONTEXT->conditions[CONTEXT->condition_length++] = condition;
			} else {
				selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
			}
		}
#line 2885 "yacc_sql.tab.c"
    break;

  case 144: /* condition: ID comOp ID  */
#line 1136 "yacc_sql.y"
                {
			RelAttr left_attr;
			relation_attr_init(&left_attr, NULL, (yyvsp[-2].string), false, 5, CONDITIONFIELD);
			RelAttr right_attr;
			relation_attr_init(&right_attr, NULL, (yyvsp[0].string), false, 5, CONDITIONFIELD);
			
			Condition condition;
			condition_init(&condition, (yyvsp[-1].number), 1, &left_attr, NULL, NULL, NULL, 1, &right_attr, NULL, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
			CONTEXT->cur_condition--;
			
			if(CONTEXT->select_length == 0) {
				CONTEXT->conditions[CONTEXT->condition_length++] = condition;
			} else {
				selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
			}

		}
#line 2907 "yacc_sql.tab.c"
    break;

  case 145: /* condition: value comOp ID  */
#line 1154 "yacc_sql.y"
                {
			Value *left_value = &CONTEXT->values[CONTEXT->value_length - 1];
			RelAttr right_attr;
			relation_attr_init(&right_attr, NULL, (yyvsp[0].string), false, 5, CONDITIONFIELD);
			
			Condition condition;
			condition_init(&condition, (yyvsp[-1].number), 2, NULL, left_value, NULL, NULL, 1, &right_attr, NULL, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
			CONTEXT->cur_condition--;
			if(CONTEXT->select_length == 0) {
				CONTEXT->conditions[CONTEXT->condition_length++] = condition;
			} else {
				selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
			}
		}
#line 2926 "yacc_sql.tab.c"
    break;

  case 146: /* condition: ID DOT ID comOp value  */
#line 1169 "yacc_sql.y"
                {
			RelAttr left_attr;
			relation_attr_init(&left_attr, (yyvsp[-4].string), (yyvsp[-2].string), false, 5, CONDITIONFIELD);
			Value *right_value = &CONTEXT->values[CONTEXT->value_length - 1];
			
			Condition condition;
			condition_init(&condition, (yyvsp[-1].number), 1, &left_attr, NULL, NULL, NULL, 2, NULL, right_value ,NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
			CONTEXT->cur_condition--;
			if(CONTEXT->select_length == 0) {
				CONTEXT->conditions[CONTEXT->condition_length++] = condition;
			} else {
				selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
			}
							
    }
#line 2946 "yacc_sql.tab.c"
    break;

  case 147: /* condition: value comOp ID DOT ID  */
#line 1185 "yacc_sql.y"
                {
			Value *left_value = &CONTEXT->values[CONTEXT->value_length - 1];

			RelAttr right_attr;
			relation_attr_init(&right_attr, (yyvsp[-2].string), (yyvsp[0].string), false, 5, CONDITIONFIELD);
			
			Condition condition;
			condition_init(&condition, (yyvsp[-3].number), 2, NULL, left_value, NULL, NULL, 1, &right_attr, NULL, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
			CONTEXT->cur_condition--;
			if(CONTEXT->select_length == 0) {
				CONTEXT->conditions[CONTEXT->condition_length++] = condition;
			} else {
				selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
			}
									
    }
#line 2967 "yacc_sql.tab.c"
    break;

  case 148: /* condition: ID DOT ID comOp ID DOT ID  */
#line 1202 "yacc_sql.y"
                {
			RelAttr left_attr;
			relation_attr_init(&left_attr, (yyvsp[-6].string), (yyvsp[-4].string), false, 5, CONDITIONFIELD);
			RelAttr right_attr;
			relation_attr_init(&right_attr, (yyvsp[-2].string), (yyvsp[0].string), false, 5, CONDITIONFIELD);
			
			Condition condition;
			condition_init(&condition, (yyvsp[-3].number), 1, &left_attr, NULL, NULL, NULL, 1, &right_attr, NULL, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
			CONTEXT->cur_condition--;
			if(CONTEXT->select_length == 0) {
				CONTEXT->conditions[CONTEXT->condition_length++] = condition;
			} else {
				selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
			}
    }
#line 2987 "yacc_sql.tab.c"
    break;

  case 149: /* condition: comOp subquery_start select RBRACE  */
#line 1218 "yacc_sql.y"
        {
		Value *left_value = NULL;
		
		Condition condition;
		condition_init(&condition, (yyvsp[-3].number), 0, NULL, left_value, NULL, NULL, 3, NULL, NULL, CONTEXT->selects[CONTEXT->select_length], NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		CONTEXT->select_length--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
#line 3005 "yacc_sql.tab.c"
    break;

  case 150: /* condition: value comOp subquery_start select RBRACE  */
#line 1232 "yacc_sql.y"
        {
		Value *left_value = &CONTEXT->values[CONTEXT->value_length - 1];
		
		Condition condition;
		condition_init(&condition, (yyvsp[-3].number), 2, NULL, left_value, NULL, NULL, 3, NULL, NULL, CONTEXT->selects[CONTEXT->select_length], NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		CONTEXT->select_length--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
#line 3023 "yacc_sql.tab.c"
    break;

  case 151: /* condition: ID comOp sublist_start value value_list RBRACE  */
#line 1245 "yacc_sql.y"
                                                         {
		RelAttr left_attr;
		relation_attr_init(&left_attr, NULL, (yyvsp[-5].string), false, 5, CONDITIONFIELD);
		ValueList right_value_list;
		right_value_list.value_num = 0;

		for(int i = CONTEXT->value_start; i < CONTEXT->value_length; i++) {
			Value *right_value = &CONTEXT->values[i];
			list_append_value(&right_value_list,right_value);
		}
		
		Condition condition;
		condition_init(&condition, (yyvsp[-4].number), 1, &left_attr, NULL, NULL, NULL, 4, NULL, NULL, NULL, &right_value_list, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
#line 3048 "yacc_sql.tab.c"
    break;

  case 152: /* condition: ID DOT ID comOp sublist_start value value_list RBRACE  */
#line 1265 "yacc_sql.y"
                                                                {
		RelAttr left_attr;
		relation_attr_init(&left_attr, (yyvsp[-7].string), (yyvsp[-5].string), false, 5, CONDITIONFIELD);
		ValueList right_value_list;
		right_value_list.value_num = 0;

		for(int i = CONTEXT->value_start; i < CONTEXT->value_length; i++) {
			Value *right_value = &CONTEXT->values[i];
			list_append_value(&right_value_list,right_value);
		}
		
		Condition condition;
		condition_init(&condition, (yyvsp[-4].number), 1, &left_attr, NULL, NULL, NULL, 4, NULL, NULL, NULL, &right_value_list, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
#line 3073 "yacc_sql.tab.c"
    break;

  case 153: /* condition: value comOp sublist_start value value_list RBRACE  */
#line 1285 "yacc_sql.y"
                                                            {
		Value *left_value = &CONTEXT->values[CONTEXT->value_start - 1];

		ValueList right_value_list;
		right_value_list.value_num = 0;

		for(int i = CONTEXT->value_start; i < CONTEXT->value_length; i++) {
			Value *right_value = &CONTEXT->values[i];
			list_append_value(&right_value_list,right_value);
		}
		
		Condition condition;
		condition_init(&condition, (yyvsp[-4].number), 2, NULL, left_value, NULL, NULL, 4, NULL, NULL, NULL, &right_value_list, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
#line 3098 "yacc_sql.tab.c"
    break;

  case 154: /* condition: sublist_start value RBRACE comOp sublist_start value value_list RBRACE  */
#line 1305 "yacc_sql.y"
                                                                                 {
		Value *left_value = &CONTEXT->values[CONTEXT->value_start - 1];

		ValueList right_value_list;
		right_value_list.value_num = 0;

		for(int i = CONTEXT->value_start; i < CONTEXT->value_length; i++) {
			Value *right_value = &CONTEXT->values[i];
			list_append_value(&right_value_list,right_value);
		}
		
		Condition condition;
		condition_init(&condition, (yyvsp[-4].number), 2, NULL, left_value, NULL, NULL, 4, NULL, NULL, NULL, &right_value_list, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
#line 3123 "yacc_sql.tab.c"
    break;

  case 155: /* condition: subquery_start select RBRACE comOp sublist_start value value_list RBRACE  */
#line 1325 "yacc_sql.y"
                                                                                   {

		ValueList right_value_list;
		right_value_list.value_num = 0;

		for(int i = CONTEXT->value_start; i < CONTEXT->value_length; i++) {
			Value *right_value = &CONTEXT->values[i];
			list_append_value(&right_value_list,right_value);
		}
		
		Condition condition;
		condition_init(&condition, (yyvsp[-4].number), 3, NULL, NULL, CONTEXT->selects[CONTEXT->select_length], NULL, 4, NULL, NULL, NULL, &right_value_list, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
#line 3147 "yacc_sql.tab.c"
    break;

  case 156: /* condition: sublist_start value RBRACE comOp ID RBRACE  */
#line 1344 "yacc_sql.y"
                                                     {
		Value *left_value = &CONTEXT->values[CONTEXT->value_length - 1];

		RelAttr right_attr;
		relation_attr_init(&right_attr, NULL, (yyvsp[-1].string), false, 5, CONDITIONFIELD);
		
		Condition condition;
		condition_init(&condition, (yyvsp[-2].number), 2, NULL, left_value, NULL, NULL, 1, &right_attr, NULL, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
#line 3167 "yacc_sql.tab.c"
    break;

  case 157: /* condition: sublist_start value RBRACE comOp ID DOT ID RBRACE  */
#line 1359 "yacc_sql.y"
                                                            {
		Value *left_value = &CONTEXT->values[CONTEXT->value_length - 1];

		RelAttr right_attr;
		relation_attr_init(&right_attr, (yyvsp[-3].string), (yyvsp[-1].string), false, 5, CONDITIONFIELD);
		
		Condition condition;
		condition_init(&condition, (yyvsp[-4].number), 2, NULL, left_value, NULL, NULL, 1, &right_attr, NULL, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
#line 3187 "yacc_sql.tab.c"
    break;

  case 158: /* condition: sublist_start value RBRACE comOp value RBRACE  */
#line 1374 "yacc_sql.y"
                                                        {
		Value *left_value = &CONTEXT->values[CONTEXT->value_length - 2];
		Value *right_value = &CONTEXT->values[CONTEXT->value_length - 1];
		
		Condition condition;
		condition_init(&condition, (yyvsp[-2].number), 2, NULL, left_value, NULL, NULL, 2, NULL, right_value, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
#line 3205 "yacc_sql.tab.c"
    break;

  case 159: /* condition: sublist_start value RBRACE comOp subquery_start select RBRACE  */
#line 1388 "yacc_sql.y"
        {
		Value *left_value = &CONTEXT->values[CONTEXT->value_length - 1];
		
		Condition condition;
		condition_init(&condition, (yyvsp[-3].number), 2, NULL, left_value, NULL, NULL, 3, NULL, NULL, CONTEXT->selects[CONTEXT->select_length], NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		CONTEXT->select_length--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
#line 3223 "yacc_sql.tab.c"
    break;

  case 160: /* condition: ID comOp subquery_start select RBRACE  */
#line 1402 "yacc_sql.y"
        {
		RelAttr left_attr;
		relation_attr_init(&left_attr, NULL, (yyvsp[-4].string), false, 5, CONDITIONFIELD);
		
		Condition condition;
		condition_init(&condition, (yyvsp[-3].number), 1, &left_attr, NULL, NULL, NULL, 3, NULL, NULL, CONTEXT->selects[CONTEXT->select_length], NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		CONTEXT->select_length--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
#line 3242 "yacc_sql.tab.c"
    break;

  case 161: /* condition: ID DOT ID comOp subquery_start select RBRACE  */
#line 1417 "yacc_sql.y"
        {
		RelAttr left_attr;
		relation_attr_init(&left_attr, (yyvsp[-6].string), (yyvsp[-4].string), false, 5, CONDITIONFIELD);
		
		Condition condition;
		condition_init(&condition, (yyvsp[-3].number), 1, &left_attr, NULL, NULL, NULL, 3, NULL, NULL, CONTEXT->selects[CONTEXT->select_length], NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		CONTEXT->select_length--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
#line 3261 "yacc_sql.tab.c"
    break;

  case 162: /* condition: subquery_start select RBRACE comOp value  */
#line 1432 "yacc_sql.y"
        {
		Value *left_value = &CONTEXT->values[CONTEXT->value_length - 1];
		
		Condition condition;
		condition_init(&condition, (yyvsp[-1].number), 3, NULL, NULL, CONTEXT->selects[CONTEXT->select_length], NULL, 2, NULL, left_value, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		CONTEXT->select_length--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
#line 3279 "yacc_sql.tab.c"
    break;

  case 163: /* condition: subquery_start select RBRACE comOp ID  */
#line 1446 "yacc_sql.y"
        {
		RelAttr right_attr;
		relation_attr_init(&right_attr, NULL, (yyvsp[0].string), false, 5, CONDITIONFIELD);
		
		Condition condition;
		condition_init(&condition, (yyvsp[-1].number), 3, NULL, NULL, CONTEXT->selects[CONTEXT->select_length], NULL, 1, &right_attr, NULL, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		CONTEXT->select_length--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
#line 3298 "yacc_sql.tab.c"
    break;

  case 164: /* condition: subquery_start select RBRACE comOp ID DOT ID  */
#line 1461 "yacc_sql.y"
        {
		RelAttr right_attr;
		relation_attr_init(&right_attr, (yyvsp[-2].string), (yyvsp[0].string), false, 5, CONDITIONFIELD);
		
		Condition condition;
		condition_init(&condition, (yyvsp[-3].number), 3, NULL, NULL, CONTEXT->selects[CONTEXT->select_length], NULL, 1, &right_attr, NULL, NULL, NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		CONTEXT->select_length--;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
#line 3317 "yacc_sql.tab.c"
    break;

  case 165: /* condition: subquery_start select RBRACE comOp subquery_start select RBRACE  */
#line 1476 "yacc_sql.y"
        {
		
		Condition condition;
		condition_init(&condition, (yyvsp[-3].number), 3, NULL, NULL, CONTEXT->selects[CONTEXT->select_length - 1], NULL, 3, NULL, NULL, CONTEXT->selects[CONTEXT->select_length], NULL, CONTEXT->is_and[CONTEXT->cur_condition-1]);
		CONTEXT->cur_condition--;
		CONTEXT->select_length -= 2;
		if(CONTEXT->select_length == 0) {
			CONTEXT->conditions[CONTEXT->condition_length++] = condition;
		} else {
			selects_append_condition(CONTEXT->selects[CONTEXT->select_length], &condition);
		}
	}
#line 3334 "yacc_sql.tab.c"
    break;

  case 167: /* eq_comp_list: COMMA eq_comp eq_comp_list  */
#line 1491 "yacc_sql.y"
                                 {
				
			}
#line 3342 "yacc_sql.tab.c"
    break;

  case 168: /* eq_comp: ID EQ value  */
#line 1497 "yacc_sql.y"
        {
		RelAttr left_attr;
		relation_attr_init(&left_attr, NULL, (yyvsp[-2].string), false, 5, CONDITIONFIELD);
		Value *right_value = &CONTEXT->values[CONTEXT->value_length - 1];
		SetCondition condition;
		set_condition_init(&condition, &left_attr, 2, right_value, NULL);
		CONTEXT->set_conditions[CONTEXT->set_condition_length++] = condition;
	}
#line 3355 "yacc_sql.tab.c"
    break;

  case 169: /* eq_comp: ID EQ subquery_start select RBRACE  */
#line 1505 "yacc_sql.y"
                                             {
		RelAttr left_attr;
		relation_attr_init(&left_attr, NULL, (yyvsp[-4].string), false, 5, CONDITIONFIELD);
		SetCondition condition;
		set_condition_init(&condition, &left_attr, 3, NULL, CONTEXT->selects[CONTEXT->select_length]);
		CONTEXT->select_length--;
		CONTEXT->set_conditions[CONTEXT->set_condition_length++] = condition;
	}
#line 3368 "yacc_sql.tab.c"
    break;

  case 170: /* comOp: EQ  */
#line 1515 "yacc_sql.y"
             { (yyval.number) = EQUAL_TO; }
#line 3374 "yacc_sql.tab.c"
    break;

  case 171: /* comOp: LT  */
#line 1516 "yacc_sql.y"
         { (yyval.number) = LESS_THAN; }
#line 3380 "yacc_sql.tab.c"
    break;

  case 172: /* comOp: GT  */
#line 1517 "yacc_sql.y"
         { (yyval.number) = GREAT_THAN; }
#line 3386 "yacc_sql.tab.c"
    break;

  case 173: /* comOp: LE  */
#line 1518 "yacc_sql.y"
         { (yyval.number) = LESS_EQUAL; }
#line 3392 "yacc_sql.tab.c"
    break;

  case 174: /* comOp: GE  */
#line 1519 "yacc_sql.y"
         { (yyval.number) = GREAT_EQUAL; }
#line 3398 "yacc_sql.tab.c"
    break;

  case 175: /* comOp: NE  */
#line 1520 "yacc_sql.y"
         { (yyval.number) = NOT_EQUAL; }
#line 3404 "yacc_sql.tab.c"
    break;

  case 176: /* comOp: IN  */
#line 1521 "yacc_sql.y"
             { (yyval.number) = IS_IN; }
#line 3410 "yacc_sql.tab.c"
    break;

  case 177: /* comOp: NOT IN  */
#line 1522 "yacc_sql.y"
                 { (yyval.number) = NOT_IN; }
#line 3416 "yacc_sql.tab.c"
    break;

  case 178: /* comOp: NOT EXIST  */
#line 1523 "yacc_sql.y"
                    { (yyval.number) = NOT_EXIST; }
#line 3422 "yacc_sql.tab.c"
    break;

  case 179: /* comOp: EXIST  */
#line 1524 "yacc_sql.y"
                { (yyval.number) = EXIST_IN; }
#line 3428 "yacc_sql.tab.c"
    break;

  case 180: /* comOp: CIS  */
#line 1525 "yacc_sql.y"
              { (yyval.number) = IS; }
#line 3434 "yacc_sql.tab.c"
    break;

  case 181: /* comOp: CIS NOT  */
#line 1526 "yacc_sql.y"
                  { (yyval.number) = IS_NOT; }
#line 3440 "yacc_sql.tab.c"
    break;

  case 182: /* comOp: LIKE  */
#line 1527 "yacc_sql.y"
               { (yyval.number) = LIKE_OP; }
#line 3446 "yacc_sql.tab.c"
    break;

  case 183: /* comOp: NOT LIKE  */
#line 1528 "yacc_sql.y"
                   { (yyval.number) = NOT_LIKE_OP; }
#line 3452 "yacc_sql.tab.c"
    break;

  case 184: /* load_data: LOAD DATA INFILE SSS INTO TABLE ID SEMICOLON  */
#line 1533 "yacc_sql.y"
                {
		  CONTEXT->ssql->flag = SCF_LOAD_DATA;
			load_data_init(&CONTEXT->ssql->sstr.load_data, (yyvsp[-1].string), (yyvsp[-4].string));
		}
#line 3461 "yacc_sql.tab.c"
    break;

  case 186: /* tuple_list: COMMA LBRACE value value_list RBRACE tuple_list  */
#line 1541 "yacc_sql.y"
                                                      {	

	}
#line 3469 "yacc_sql.tab.c"
    break;


#line 3473 "yacc_sql.tab.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (scanner, YY_("syntax error"));
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, scanner);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp, scanner);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;


#if !defined yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (scanner, YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturn;
#endif


/*-------------------------------------------------------.
| yyreturn -- parsing is finished, clean up and return.  |
`-------------------------------------------------------*/
yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, scanner);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp, scanner);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 1545 "yacc_sql.y"

//_____________________________________________________________________
extern void scan_string(const char *str, yyscan_t scanner);

int sql_parse(const char *s, Query *sqls){
	ParserContext context;
	memset(&context, 0, sizeof(context));
	yyscan_t scanner;
	yylex_init_extra(&context, &scanner);
	context.ssql = sqls;
	scan_string(s, scanner);
	int result = yyparse(scanner);
	yylex_destroy(scanner);
	return result;
}
