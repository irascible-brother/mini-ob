/* Copyright (c) 2021 Xie Meiyi(xiemeiyi@hust.edu.cn) and OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by Meiyi
//

#ifndef __OBSERVER_SQL_PARSER_PARSE_DEFS_H__
#define __OBSERVER_SQL_PARSER_PARSE_DEFS_H__

#include <stddef.h>
#include <stdbool.h>

#define MAX_SELECT_NUM 5
#define MAX_NUM 20
#define MAX_REL_NAME 20
#define MAX_ATTR_NAME 20
#define MAX_ERROR_MESSAGE 20
#define MAX_DATA 50

typedef enum {
  MAXIMUM = 0,
  MINIMUM,
  AVERAGE,
  COUNT,
  SUMMARY,
  NONEAGG
} Aggregator;

typedef enum {
  SELECTFIELD = 0,
  ORDERFIELD,
  GROUPFIELD,
  HAVINGFIELD,
  CONDITIONFIELD
} FIELDTYPE;
//属性结构体
typedef struct {
  char *relation_name;   // relation name (may be NULL) 表名
  char *attribute_name;  // attribute name              属性名
  Aggregator agg;
  bool is_agg;
  FIELDTYPE field_type;
} RelAttr;

typedef struct {
  Aggregator aggregator_type;
  char *relation_name;
  char *attribute_name;
} RelAggr;


typedef enum {
  EQUAL_TO,     //"="     0
  LESS_EQUAL,   //"<="    1
  NOT_EQUAL,    //"<>"    2
  LESS_THAN,    //"<"     3
  GREAT_EQUAL,  //">="    4
  GREAT_THAN,   //">"     5
  EXIST_IN,    //"in"    6
  IS_IN, //7
  NOT_EXIST, //8
  NOT_IN,      //"not in"9
  IS,          //"is"    10
  IS_NOT,      //"is not"11
  LIKE_OP,
  NOT_LIKE_OP,
  NO_OP,
} CompOp;

//属性值类型
typedef enum
{
  UNDEFINED,
  CHARS,
  INTS,
  FLOATS,
  DATES,
  TEXTS,
  NULLS,
} AttrType;

//属性值
typedef struct _Value {
  AttrType type;  // type of value
  void *data;     // value
} Value;

typedef struct _ValueList{
  int value_num;
  Value values[MAX_NUM];
} ValueList;
//order
typedef struct _Order
{
  bool asc;     // asc or desc
} Order;

typedef enum {
  NONE,//0
  FIELD,//1
  VALUE,//2
  QUERY,//3
  LIST,//4
  EXPR,//5
  AGGR,//6
  NON,
} ExprType;

typedef struct _Condition {
  ExprType left_type;    // TRUE if left-hand side is an attribute
                       // 1时，操作符左边是属性名，0时，是属性值
  Value left_value;    // left-hand side value if left_is_attr = FALSE
  RelAttr left_attr;   // left-hand side attribute
  struct Selects_t *right_subquery; // sub-query
  struct Selects_t *left_subquery;  // sub-query
  ValueList left_value_list;
  CompOp comp;         // comparison operator
  ExprType right_type;   // TRUE if right-hand side is an attribute
                       // 1时，操作符右边是属性名，0时，是属性值
  size_t left_list_length;
  RelAttr right_attr;  // right-hand side attribute if right_is_attr = TRUE 右边的属性
  Value right_value;   // right-hand side value if right_is_attr = FALSE
  size_t right_list_length;
  ValueList right_value_list;
  bool is_and;
} Condition;

typedef struct _HavingCondition {
  ExprType left_type;
  RelAttr left_field;
  Value left_value;
  ExprType right_type;
  RelAttr right_field;
  Value right_value;
  Aggregator left_agg_type;
  Aggregator right_agg_type;
  CompOp comp;
} HavingCondition;
typedef struct _SetCondition {
  struct Selects_t *right_subquery;
  RelAttr left_field;
  Value right_value;
  ExprType type;
} SetCondition;

typedef struct Selects_t {
  size_t attr_num;                // Length of attrs in Select clause
  RelAttr attributes[MAX_NUM];    // attrs in Select clause
  size_t relation_num;            // Length of relations in Fro clause
  char *relations[MAX_NUM];       // relations in From clause
  size_t out_relations_num;            // Length of relations in Fro clause
  char *out_relations[MAX_NUM];       // relations in From clause
  size_t condition_num;           // Length of conditions in Where clause
  // struct SubQuery_t subquerys[MAX_SELECT_NUM];
  Condition conditions[MAX_NUM];  // conditions in Where clause
  size_t subquery_num;                         // Length of sub-query
  size_t group_num;                            // Length of groups
  RelAttr groups[MAX_NUM];                     // groups
  size_t order_num;                            // Length of orders
  Order orders[MAX_NUM];                       // orders
  size_t    aggr_num;
  RelAggr    aggregations[MAX_NUM];
  HavingCondition having_condition;
  bool is_having;
  bool agg;                                    // 标志是否有聚合函数
  char *column_alias_table[MAX_NUM];
  char *column_alias_from[MAX_NUM];
  char *column_alias_to[MAX_NUM];
  size_t column_alias_num;
  char *table_alias_from[MAX_NUM];
  char *table_alias_to[MAX_NUM];
  size_t table_alias_num;
} Selects;

// struct of insert
typedef struct {
  char *relation_name;    // Relation to insert into
  size_t value_num;       // Length of values
  Value values[MAX_NUM];  // values to insert
} Inserts;

// struct of delete
typedef struct {
  char *relation_name;            // Relation to delete from
  size_t condition_num;           // Length of conditions in Where clause
  Condition conditions[MAX_NUM];  // conditions in Where clause
} Deletes;

// struct of update
typedef struct {
  char *relation_name;            // Relation to update
  SetCondition set_conditions[MAX_NUM];          // Length of conditions in Where clause
  Condition conditions[MAX_NUM];  // conditions in Where clause
  size_t condition_num;
  size_t set_condition_num;
} Updates;

typedef struct {
  char *name;     // Attribute name
  AttrType type;  // Type of attribute
  size_t length;  // Length of attribute
  bool nullable;
} AttrInfo;

// struct of craete_table
typedef struct {
  char *relation_name;           // Relation name
  size_t attribute_count;        // Length of attribute
  AttrInfo attributes[MAX_NUM];  // attributes
} CreateTable;

// struct of drop_table
typedef struct {
  char *relation_name;  // Relation name
} DropTable;

// struct of create_index
typedef struct {
  char *index_name;      // Index name
  char *relation_name;   // Relation name
  bool is_unique;
  char *attributes_name[MAX_NUM];  // Attribute name
  int attribute_num;
} CreateIndex;


// struct of  drop_index
typedef struct {
  const char *index_name;  // Index name
} DropIndex;

typedef struct {
  const char *relation_name;
} DescTable;

typedef struct {
  const char *relation_name;
  const char *file_name;
} LoadData;

typedef struct {
  const char *relation_name;
} ShowIndex;

union Queries {
  Selects selection;
  Inserts insertion;
  Deletes deletion;
  Updates update;
  CreateTable create_table;
  DropTable drop_table;
  CreateIndex create_index;
  DropIndex drop_index;
  DescTable desc_table;
  LoadData load_data;
  ShowIndex show_index;
  char *errors;
};

// 修改yacc中相关数字编码为宏定义
enum SqlCommandFlag {
  SCF_ERROR = 0,
  SCF_SELECT,
  SCF_INSERT,
  SCF_UPDATE,
  SCF_DELETE,
  SCF_CREATE_TABLE,
  SCF_DROP_TABLE,
  SCF_CREATE_INDEX,
  SCF_DROP_INDEX,
  SCF_SYNC,
  SCF_SHOW_TABLES,
  SCF_DESC_TABLE,
  SCF_BEGIN,
  SCF_COMMIT,
  SCF_CLOG_SYNC,
  SCF_ROLLBACK,
  SCF_LOAD_DATA,
  SCF_HELP,
  SCF_EXIT,
  SCF_SHOW_INDEX
};
// struct of flag and sql_struct
typedef struct Query {
  enum SqlCommandFlag flag;
  union Queries sstr;
} Query;

#ifdef __cplusplus
extern "C" {
#endif  // __cplusplus

void relation_attr_init(RelAttr *relation_attr, const char *relation_name, const char *attribute_name, bool is_agg, Aggregator agg, FIELDTYPE field_type);
void relation_attr_destroy(RelAttr *relation_attr);

void relation_aggr_init(RelAggr *relation_agg, const char *relation_name, const char *attribute_name, const Aggregator aggregator_type);
void relation_aggr_destroy(RelAggr *relation_aggr);

void value_init_integer(Value *value, int v);
void value_init_float(Value *value, float v);
void value_init_string(Value *value, const char *v);
void value_init_null(Value *value);
void value_destroy(Value *value);

void order_init(Order *order, bool asc);
void set_condition_init(SetCondition *condition,RelAttr *left_attr, ExprType type, Value *value, Selects *selects);
// void subquery_init(SubQuery *subquery, CompOp comp, ExprType left_type, RelAttr *left_attr, Value *left_value, Selects *left_selects,
//                     ExprType right_type, RelAttr *right_attr, Value *right_value, Selects *right_selects);
void condition_init(Condition *condition, CompOp comp,
                      ExprType left_type, RelAttr *left_attr, Value *left_value, Selects *left_selects, ValueList *left_list,
                      ExprType right_type, RelAttr *right_attr, Value *right_value, Selects *right_selects, ValueList *right_list, bool is_and);
void condition_destroy(Condition *condition);

void attr_info_init(AttrInfo *attr_info, const char *name, AttrType type, size_t length, bool nullable);
void attr_info_destroy(AttrInfo *attr_info);

void select_init(Selects **selects);
void selects_init(Selects *selects, ...);
void selects_append_attribute(Selects *selects, RelAttr *rel_attr);
void selects_append_relation(Selects *selects, const char *relation_name);
void selects_append_conditions(Selects *selects, Condition conditions[], size_t condition_num);
void selects_append_condition(Selects *selects, Condition *condition);
void list_append_value(ValueList *list, Value *left_value);
void selects_append_groups(Selects *selects, RelAttr groups[], size_t group_num);
void selects_append_order(Selects *selects, Order *order);
void selects_append_aggragation(Selects *selects, RelAggr *rel_agg);
// void selects_append_subquery(Selects *selects, SubQuery subquery);
void selects_set_agg(Selects *selects, bool agg);

void order_destroy(Order *order);
void selects_append_group(Selects *selects, RelAttr *group_rel);
void having_condition_init(HavingCondition *condition, ExprType left_type, RelAttr *left_attr, Value * left_value, Aggregator agg_type);
void having_condition_append(HavingCondition *condition, ExprType right_type, RelAttr *right_attr, Value * right_value, Aggregator agg_type);
void having_condition_append_comp(HavingCondition *condition, CompOp comp);
void selects_append_having_condition(Selects *select, HavingCondition *condition, bool is_having);
void having_condition_destroy(HavingCondition *having_condition);
// void subquery_destroy(SubQuery *subquery);
void selects_destroy(Selects *selects);
void selects_clear(Selects **selects);

void inserts_init(Inserts *inserts, const char *relation_name, Value values[], size_t value_num);
void inserts_destroy(Inserts *inserts);

void deletes_init_relation(Deletes *deletes, const char *relation_name);
void deletes_set_conditions(Deletes *deletes, Condition conditions[], size_t condition_num);
void deletes_destroy(Deletes *deletes);

void updates_init(Updates *updates, const char *relation_name, SetCondition set_conditions[], size_t set_condition_num,
    Condition conditions[], size_t condition_num);
void updates_destroy(Updates *updates);
void print_func(size_t i);
void print_add(void *i);
void create_table_append_attribute(CreateTable *create_table, AttrInfo *attr_info);
void create_table_init_name(CreateTable *create_table, const char *relation_name);
void create_table_destroy(CreateTable *create_table);

void drop_table_init(DropTable *drop_table, const char *relation_name);
void drop_table_destroy(DropTable *drop_table);

void create_index_init(
    CreateIndex *create_index, const char *index_name, const char *relation_name, bool is_unique);
void index_append_attribute(CreateIndex *create_index, const char *attribute_name);

void drop_index_init(DropIndex *drop_index, const char *index_name);
void drop_index_destroy(DropIndex *drop_index);

void desc_table_init(DescTable *desc_table, const char *relation_name);
void desc_table_destroy(DescTable *desc_table);

void show_index_init(ShowIndex *show_index, const char *relation_name);
void show_index_destroy(ShowIndex *show_index);

void load_data_init(LoadData *load_data, const char *relation_name, const char *file_name);
void load_data_destroy(LoadData *load_data);

void query_init(Query *query);
Query *query_create();  // create and init
void query_reset(Query *query);
void query_destroy(Query *query);  // reset and delete

void selects_append_column_alias(Selects *selects, char *table, char *from, char *to); 
void column_alias_destroy(Selects *selects, size_t i);
void selects_append_table_alias(Selects *selects, char *from, char *to);
void table_alias_destroy(Selects *selects, size_t i);

#ifdef __cplusplus
}
#endif  // __cplusplus

#endif  // __OBSERVER_SQL_PARSER_PARSE_DEFS_H__
