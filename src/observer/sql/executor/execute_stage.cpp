/* Copyright (c) 2021 Xie Meiyi(xiemeiyi@hust.edu.cn) and OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by Meiyi & Longda on 2021/4/13.
//

#include <string>
#include <sstream>

#include "execute_stage.h"

#include "common/io/io.h"
#include "common/log/log.h"
#include "common/lang/defer.h"
#include "common/seda/timer_stage.h"
#include "common/lang/string.h"
#include "session/session.h"
#include "event/storage_event.h"
#include "event/sql_event.h"
#include "event/session_event.h"
#include "sql/expr/tuple.h"
#include "sql/operator/table_scan_operator.h"
#include "sql/operator/index_scan_operator.h"
#include "sql/operator/predicate_operator.h"
#include "sql/operator/delete_operator.h"
#include "sql/operator/project_operator.h"
#include "sql/stmt/stmt.h"
#include "sql/stmt/select_stmt.h"
#include "sql/stmt/update_stmt.h"
#include "sql/stmt/delete_stmt.h"
#include "sql/stmt/insert_stmt.h"
#include "sql/stmt/filter_stmt.h"
#include "storage/common/table.h"
#include "storage/common/field.h"
#include "storage/index/index.h"
#include "storage/default/default_handler.h"
#include "storage/common/condition_filter.h"
#include "storage/trx/trx.h"
#include "storage/clog/clog.h"
#include "sql/operator/update_operator.h"
#include "sql/expr/tuple_cell.h"

using namespace common;
PredicateOperator *get_predictor_tree(SelectStmt *select_stmt, std::map<std::string, Operator *> table_operator_mp, std::vector<TmpStmt *> &stmt_pointers, std::vector<Operator *> &op_pointers);
//RC create_selection_executor(
//   Trx *trx, const Selects &selects, const char *db, const char *table_name, SelectExeNode &select_node);

//! Constructor
ExecuteStage::ExecuteStage(const char *tag) : Stage(tag)
{}

//! Destructor
ExecuteStage::~ExecuteStage()
{}

//! Parse properties, instantiate a stage object
Stage *ExecuteStage::make_stage(const std::string &tag)
{
  ExecuteStage *stage = new (std::nothrow) ExecuteStage(tag.c_str());
  if (stage == nullptr) {
    LOG_ERROR("new ExecuteStage failed");
    return nullptr;
  }
  stage->set_properties();
  return stage;
}

//! Set properties for this object set in stage specific properties
bool ExecuteStage::set_properties()
{
  //  std::string stageNameStr(stageName);
  //  std::map<std::string, std::string> section = theGlobalProperties()->get(
  //    stageNameStr);
  //
  //  std::map<std::string, std::string>::iterator it;
  //
  //  std::string key;

  return true;
}

//! Initialize stage params and validate outputs
bool ExecuteStage::initialize()
{
  LOG_TRACE("Enter");

  std::list<Stage *>::iterator stgp = next_stage_list_.begin();
  default_storage_stage_ = *(stgp++);
  mem_storage_stage_ = *(stgp++);

  LOG_TRACE("Exit");
  return true;
}

//! Cleanup after disconnection
void ExecuteStage::cleanup()
{
  LOG_TRACE("Enter");

  LOG_TRACE("Exit");
}

void ExecuteStage::handle_event(StageEvent *event)
{
  LOG_TRACE("Enter\n");

  handle_request(event);

  LOG_TRACE("Exit\n");
  return;
}

void ExecuteStage::callback_event(StageEvent *event, CallbackContext *context)
{
  LOG_TRACE("Enter\n");

  // here finish read all data from disk or network, but do nothing here.

  LOG_TRACE("Exit\n");
  return;
}

void ExecuteStage::handle_request(common::StageEvent *event)
{
  SQLStageEvent *sql_event = static_cast<SQLStageEvent *>(event);
  SessionEvent *session_event = sql_event->session_event();
  Stmt *stmt = sql_event->stmt();
  Session *session = session_event->session();
  Query *sql = sql_event->query();

  if (stmt != nullptr) {
    switch (stmt->type()) {
    case StmtType::SELECT: {
      do_select(sql_event);
    } break;
    case StmtType::INSERT: {
      do_insert(sql_event);
    } break;
    case StmtType::UPDATE: {
      //do_update((UpdateStmt *)stmt, session_event);
      do_update(sql_event);
    } break;
    case StmtType::DELETE: {
      do_delete(sql_event);
    } break;
    default: {
      LOG_WARN("should not happen. please implenment");
    } break;
    }
  } else {
    switch (sql->flag) {
    case SCF_HELP: {
      do_help(sql_event);
    } break;
    case SCF_CREATE_TABLE: {
      do_create_table(sql_event);
    } break;
    case SCF_CREATE_INDEX: {
      do_create_index(sql_event);
    } break;
    case SCF_SHOW_TABLES: {
      do_show_tables(sql_event);
    } break;
    case SCF_DESC_TABLE: {
      do_desc_table(sql_event);
    } break;
    case SCF_SHOW_INDEX: {
      do_show_index(sql_event);
    } break;

    case SCF_DROP_TABLE: {
      do_drop_table(sql_event);
    } break;
    case SCF_DROP_INDEX:
    case SCF_LOAD_DATA: {
      default_storage_stage_->handle_event(event);
    } break;
    case SCF_SYNC: {
      /*
      RC rc = DefaultHandler::get_default().sync();
      session_event->set_response(strrc(rc));
      */
    } break;
    case SCF_BEGIN: {
      do_begin(sql_event);
      /*
      session_event->set_response("SUCCESS\n");
      */
    } break;
    case SCF_COMMIT: {
      do_commit(sql_event);
      /*
      Trx *trx = session->current_trx();
      RC rc = trx->commit();
      session->set_trx_multi_operation_mode(false);
      session_event->set_response(strrc(rc));
      */
    } break;
    case SCF_CLOG_SYNC: {
      do_clog_sync(sql_event);
    }
    case SCF_ROLLBACK: {
      Trx *trx = session_event->get_client()->session->current_trx();
      RC rc = trx->rollback();
      session->set_trx_multi_operation_mode(false);
      session_event->set_response(strrc(rc));
    } break;
    case SCF_EXIT: {
      // do nothing
      const char *response = "Unsupported\n";
      session_event->set_response(response);
    } break;
    default: {
      LOG_ERROR("Unsupported command=%d\n", sql->flag);
    }
    }
  }
}

void end_trx_if_need(Session *session, Trx *trx, bool all_right)
{
  if (!session->is_trx_multi_operation_mode()) {
    if (all_right) {
      trx->commit();
    } else {
      trx->rollback();
    }
  }
}

void print_tuple_header(std::ostream &os, const ProjectOperator &oper, int start, std::vector<QueryField> query_fields)
{
  const int cell_num = oper.tuple_cell_num();
  const TupleCellSpec *cell_spec = nullptr;
  for (int i = start; i < cell_num;) {
    oper.tuple_cell_spec_at(i, cell_spec);
    if (i != start) {
      os << " | ";
    }
    if (query_fields[i].alias() != "") {
      os << query_fields[i].alias();
    } else {
      if(query_fields[i].is_agg) {
        std:: string s = "";
        switch (query_fields[i].agg)
        {
        case Aggregator::AVERAGE:s = "AVG(";break;
        case Aggregator::COUNT: s = "COUNT(";break;
        case Aggregator::MAXIMUM: s = "MAX(";break;
        case Aggregator::MINIMUM: s = "MIN(";break;
        case Aggregator::SUMMARY: s = "SUM(";break;
        default:
          break;
        }
        os << s;
      }
      if((query_fields[i].end - i) > 1) {
        os << "*";
      } else {
        os << cell_spec->alias();
      }
      
      if(query_fields[i].is_agg) {
        os << ")";
      }
    }
    if(query_fields[i].is_agg) {
      i = query_fields[i].end;
    } else {
      i++;
    }
  }

  if (cell_num > 0) {
    os << '\n';
  }
}
void print_agg_header(std::ostream &os, const std::vector<RelAggr> aggs) {
  bool is_single = true;
  for(std::size_t i = 1; i < aggs.size(); i++) {
    if(aggs[i].relation_name != aggs[i - 1].relation_name) {
      is_single = false;
    }
  }
  for(std::size_t i = 0; i < aggs.size(); i++) {
    if (i != 0) {
      os << " | ";
    }
    std:: string s = "";
    switch (aggs[i].aggregator_type)
    {
    case Aggregator::AVERAGE:s = "AVG(";break;
    case Aggregator::COUNT: s = "COUNT(";break;
    case Aggregator::MAXIMUM: s = "MAX(";break;
    case Aggregator::MINIMUM: s = "MIN(";break;
    case Aggregator::SUMMARY: s = "SUM(";break;
    default:
      break;
    }
    os << s;
    if(!is_single) {
      os << aggs[i].relation_name;
    }
    os << aggs[i].attribute_name;
    os << ")";
  }
  if (aggs.size() > 0) {
    os << '\n';
  }
}


RC get_result(MemTuple *m, RelAggr aggr, int cur, int start, int end, TulpeCellOut *resultCell) {
  if(m->length() == 0) {
    //no data
    return RC::NOTFOUND;
  }
  resultCell->set_data(m->get(start)->get(cur)->data());
  resultCell->set_type(m->get(start)->get(cur)->attr_type());
  resultCell->set_length(m->get(start)->get(cur)->length());

  if(aggr.aggregator_type == Aggregator::MAXIMUM || aggr.aggregator_type == Aggregator::MINIMUM) {
    int count = 0;
    for(int i = start; i < end; i++) {
      MemRowTuple *row = m->get(i);
      TulpeCellOut *cell = row->get(cur);
      resultCell->set_type(cell->attr_type());
      if(cell->is_null()) {
        continue;
      } else {
        count++;
      }
      if(aggr.aggregator_type == Aggregator::MAXIMUM) {
        if(resultCell->compare(*cell) < 0) {
          resultCell->set_data(cell->data());
        }
      } else {
        if(resultCell->compare(*cell) > 0) {
          resultCell->set_data(cell->data());
        }
      }
    }
    if(count == 0) {
      resultCell->set_null();
    } else {
      resultCell->set_not_null();
    }
    return RC::SUCCESS;
  }
  if(aggr.aggregator_type == Aggregator::AVERAGE) {
    float avg = 0;
    int count = 0;
    for(int i = start; i < end; i++) {
      MemRowTuple *row = m->get(i);
      TulpeCellOut *cell = row->get(cur);
      if(cell->is_null()) {
        continue;
      } else {
        count++;
      }
      if(cell->attr_type() ==AttrType::INTS){
        avg += *(int *)(cell->data());
      } else if(cell->attr_type() ==AttrType::FLOATS){
        avg += *(float *)(cell->data());
      } else if(cell->attr_type() ==AttrType::CHARS) {
        avg += TupleCell::get_float_from_char(const_cast<char *>(cell->data()));
      }
    }
    if(count == 0) {
      resultCell->set_null();
    } else {
      avg /= count;
      resultCell->set_type(AttrType::FLOATS);
      resultCell->set_normal_data((char *) (&avg));
      resultCell->set_not_null();
    }
    // memcpy((void *)(resultCell->data()), &avg, sizeof(float));
  }
  if(aggr.aggregator_type == Aggregator::SUMMARY) {
    float sum = 0;
    int count = 0;
    for(int i = start; i < end; i++) {
      MemRowTuple *row = m->get(i);
      TulpeCellOut *cell = row->get(cur);
      if(cell->is_null()) {
        continue;
      } else {
        count++;
      }
      resultCell->set_type(cell->attr_type());
      if(cell->attr_type() ==AttrType::INTS){
        sum += *(int *)(cell->data());
      } else if(cell->attr_type() ==AttrType::FLOATS){
        sum += *(float *)(cell->data());
      } else if(cell->attr_type() ==AttrType::CHARS) {
        sum += TupleCell::get_float_from_char(const_cast<char *>(cell->data()));
      }
    }
    if(count == 0) {
      resultCell->set_null();
    } else {
      if(resultCell->attr_type() == AttrType::INTS) {
        int s = sum;
        resultCell->set_normal_data((char *) (&s));
      }else {
        resultCell->set_normal_data((char *) (&sum));
      }
      resultCell->set_not_null();
    }

    // memcpy((void *)(resultCell->data()), &avg, sizeof(float));
  }
  else if(aggr.aggregator_type == Aggregator::COUNT) {
    resultCell->set_type(AttrType::INTS);
    if(0 == strcmp(aggr.attribute_name, "*")) {
      int a = m->length();
      resultCell->set_normal_data((char *)(&a));
    } else {
      int count = 0;
      for(int i = start; i < end; i++) {
        MemRowTuple *row = m->get(i);
        TulpeCellOut *cell = row->get(cur);
        if(cell->is_null()) {
          continue;
        } else {
          count++;
        }
      }
      resultCell->set_normal_data((char *)(&count));
    }
    resultCell->set_not_null();
    // memcpy((void *)(resultCell->data()), &a, sizeof(int));
  }
  return RC::SUCCESS;
}

void tuple_to_mem(MemRowTuple *row, Tuple *tuple) {
  TupleCell cell;
  RC rc = RC::SUCCESS;
  for (int i = 0; i < tuple->cell_num(); i++) {
    rc = tuple->cell_at(i, cell);
    if (rc != RC::SUCCESS) {
      LOG_WARN("failed to fetch field of cell. index=%d, rc=%s", i, strrc(rc));
      break;
    }
    row->add(cell);
  }
}

void tuple_to_string(std::ostream &os, MemRowTuple &tuple, std::vector<RecordFileHandler*> handlers, int start)
{
  bool first_field = true;
  for (int i = start; i < tuple.length(); i++) {
    if (!first_field) {
      os << " | ";
    } else {
      first_field = false;
    }
    tuple.get(i)->to_string(os, handlers[i]);
  }
}

void min_tuple_cell(TupleCell *tuple_cell, AttrType type, int length) {
  static int i = std::numeric_limits<int>::min();
  static float f = std::numeric_limits<float>::min();
  static unsigned char c[4] = { 0, 0, 0, 0 };
  switch (type) {
  case INTS: {
    tuple_cell->set_type(INTS);
    tuple_cell->set_length(length);
    tuple_cell->set_data((char *)&i);
  } break;
  case FLOATS: {
    tuple_cell->set_type(FLOATS);
    tuple_cell->set_length(length);
    tuple_cell->set_data((char *)&f);
  } break;
  case CHARS: {
    tuple_cell->set_type(CHARS);
    tuple_cell->set_length(4);
    tuple_cell->set_data((char *)c);
  } break;
  case DATES: {
    tuple_cell->set_type(DATES);
    tuple_cell->set_length(length);
    tuple_cell->set_data((char *)c);
  } break;
  case UNDEFINED: {
    tuple_cell->set_type(UNDEFINED);
    tuple_cell->set_length(length);
    tuple_cell->set_data((char *)c);
  } break;
  default: {

  } break;
  }
}

void max_tuple_cell(TupleCell *tuple_cell, AttrType type, int length) {
  static int i = std::numeric_limits<int>::max();
  static float f = std::numeric_limits<float>::max();
  static unsigned char c[4] = { 255, 255, 255, 255 };
  switch (type) {
  case INTS: {
    tuple_cell->set_type(INTS);
    tuple_cell->set_length(length);
    tuple_cell->set_data((char *)&i);
  } break;
  case FLOATS: {
    tuple_cell->set_type(FLOATS);
    tuple_cell->set_length(length);
    tuple_cell->set_data((char *)&f);
  } break;
  case CHARS: {
    tuple_cell->set_type(CHARS);
    tuple_cell->set_length(4);
    tuple_cell->set_data((char *)c);
  } break;
  case DATES: {
    tuple_cell->set_type(DATES);
    tuple_cell->set_length(length);
    tuple_cell->set_data((char *)c);
  } break;
  case UNDEFINED: {
    tuple_cell->set_type(UNDEFINED);
    tuple_cell->set_length(length);
    tuple_cell->set_data((char *)c);
  } break;
  default: {

  } break;
  }
}

RC read_operator(Operator *op, MemTuple *mem_tuples) {
  RC rc = RC::SUCCESS;
  while ((rc = op->next()) == RC::SUCCESS) {
    // get current record
    // write to response
    Tuple * tuple = op->current_tuple();
    if (nullptr == tuple) {
      rc = RC::INTERNAL;
      LOG_WARN("failed to get current record. rc=%s", strrc(rc));
      break;
    }
    MemRowTuple *row = new MemRowTuple();
    tuple_to_mem(row, tuple);
    mem_tuples->add(row);
    // tuple_to_string(ss, *tuple);
    // ss << std::endl;
  }
  return rc;
}
IndexScanOperator *try_to_create_index_scan_operator(const Table *table, const std::vector<FilterUnit *> &filter_units)
{
    // const std::vector<FilterUnit *> &filter_units = filter_stmt->filter_units();
  if (filter_units.empty() ) {
    return nullptr;
  }

  // 找索引
  // 合适的filter unit
  std::vector<FilterUnit *> proper_filters;
  for (FilterUnit *filter_unit: filter_units) {
    CompOp comp = filter_unit->comp();
    // null不走索引 - 1
    if (comp == NOT_EQUAL || comp == IS_NOT || comp == IS) {
      continue;
    }
    Expression *left = filter_unit->left();
    Expression *right = filter_unit->right();
    if (!left || !right ||left->type() == QUERY ||right->type() == QUERY || left->type() == LIST ||right->type() == LIST || left->type() == EXPR ||right->type() == EXPR) {
      continue;
    }
    if (left->type() == ExprType::LIST && right->type() == ExprType::FIELD) {
      std::swap(left, right);
    }
    if (left->type() == ExprType::LIST || right->type() == ExprType::FIELD) {
      continue;
    }
    // null不走索引 - 2
    ValueListExpr &right_value_expr = *(ValueListExpr *)right;
    if(right_value_expr.length() != 1) {
      continue;
    }
    TupleCell tuple_cell;
    if(right_value_expr.length() != 1) {
      continue;
    }
    right_value_expr.get_tuple_cell(tuple_cell);
    if (tuple_cell.attr_type() == NULLS) {
      continue;
    }
    FieldExpr &left_field_expr = *(FieldExpr *)left;
    const Field &field = left_field_expr.field();
    const Table *field_table = field.table();
    if (strcmp(field_table->name(), table->name())) {
      continue;
    }
    proper_filters.push_back(filter_unit);
  }
  if (proper_filters.empty()) {
    return nullptr;
  }
  // 找到一个最合适的索引
  Index *chosen_index = nullptr;
  std::vector<FilterUnit *> chosen_filters;
  std::vector<int> chosen_fields_length;
  for (int i = 0; i < table->index_num(); i++) {
    Index *index = table->find_index(i);
    IndexMeta index_meta = index->index_meta();
    std::vector<FilterUnit *> index_filters;
    std::vector<int> length;
    // 索引的每一列是否都出现在了条件中
    for (int j = 0; j < index_meta.field_num(); j++) {
      const char* field_name = index_meta.field(j);
      for (FilterUnit *filter_unit : proper_filters) {
        Expression *left = filter_unit->left();
        Expression *right = filter_unit->right();
        if (left->type() == ExprType::LIST && right->type() == ExprType::FIELD) {
          std::swap(left, right);
        }
        FieldExpr &left_field_expr = *(FieldExpr *)left;
        const Field &field = left_field_expr.field();
        if (!strcmp(field_name, field.field_name())) {
          index_filters.push_back(filter_unit);
          length.push_back(field.meta()->len());
          break;
        }
      }
      // 索引的某一列未曾出现在条件中，放弃
      if (static_cast<int>(index_filters.size()) == j) {
        break;
      }
    }
    if (index_meta.field_num() == static_cast<int>(index_filters.size())) {
      chosen_index = index;
      chosen_filters.swap(index_filters);
      chosen_fields_length.swap(length);
      if (index_meta.field_num() > 1) {
        break;
      }
    }
  }
  if (!chosen_index) {
    return nullptr;
  }

  // 构造上下界
  TupleCell left_cells[chosen_filters.size()];
  TupleCell right_cells[chosen_filters.size()];
  for (std::size_t i = 0; i < chosen_filters.size(); i++) {
    FilterUnit *filter_unit = chosen_filters[i];
    Expression *left = filter_unit->left();
    Expression *right = filter_unit->right();
    CompOp comp = filter_unit->comp();
    if (left->type() == ExprType::LIST && right->type() == ExprType::FIELD) {
      std::swap(left, right);
      switch (comp) {
      case EQUAL_TO:    { comp = EQUAL_TO; }    break;
      case LESS_EQUAL:  { comp = GREAT_THAN; }  break;
      case NOT_EQUAL:   { comp = NOT_EQUAL; }   break;
      case LESS_THAN:   { comp = GREAT_EQUAL; } break;
      case GREAT_EQUAL: { comp = LESS_THAN; }   break;
      case GREAT_THAN:  { comp = LESS_EQUAL; }  break;
      default: {
        LOG_WARN("should not happen");
      }
      }
    }
    ValueListExpr &right_value_expr = *(ValueListExpr *)right;
    switch (comp) {
    case EQUAL_TO: {
      right_value_expr.get_tuple_cell(left_cells[i]);
      right_value_expr.get_tuple_cell(right_cells[i]);
    } break;
    case LESS_EQUAL:
    case LESS_THAN: {
      right_value_expr.get_tuple_cell(right_cells[i]);
      min_tuple_cell(&left_cells[i], right_cells[i].attr_type(), right_cells[i].length());
    } break;
    case GREAT_EQUAL:
    case GREAT_THAN: {
      right_value_expr.get_tuple_cell(left_cells[i]);
      max_tuple_cell(&right_cells[i], left_cells[i].attr_type(), left_cells[i].length());
    } break;
    default: {
      LOG_WARN("should not happen. comp=%d", comp);
    } break;
    }
  }
  int key_length = 0;
  for (const int length : chosen_fields_length) {
    key_length += length;
  }
  std::vector<char> left_key(key_length);
  std::vector<char> right_key(key_length);
  int offset = 0;
  for (std::size_t i = 0; i < chosen_fields_length.size(); i++) {
    memcpy(&left_key[0] + offset, left_cells[i].data(), chosen_fields_length[i]);
    memcpy(&right_key[0] + offset, right_cells[i].data(), chosen_fields_length[i]);
    offset += chosen_fields_length[i];
  }
  IndexScanOperator *oper = new IndexScanOperator(table, chosen_index, left_key, right_key);

  LOG_INFO("use index for scan: %s in table %s", chosen_index->index_meta().name(), table->name());
  return oper;
}



RC ExecuteStage::get_operator(const std::vector<Table *> &tables, const std::vector<Table *> &out_tables,const std::vector<FilterUnit *> &filter_units, Operator* &op, std::vector<TmpStmt *> &stmt_pointers, std::vector<Operator *> &op_pointers) {
  RC rc = RC::SUCCESS;
  rc = transform_predicate_expression_to_date_if_nessasery(filter_units);
  if (rc != RC::SUCCESS) {
    return rc;
  }

  std::vector<Operator *> scan_opers;
  std::map<std::string, Operator *> table_operator_mp;
  for (std::size_t i = 0; i < tables.size(); i++) {
    Operator *scan_oper = try_to_create_index_scan_operator(tables[i], filter_units);
    if (nullptr == scan_oper) {
      scan_oper = new TableScanOperator(tables[i]);
    }
    table_operator_mp[tables[i]->name()] = scan_oper;
    scan_opers.push_back(scan_oper);
  }
  for(Operator *op_s : scan_opers) {
    op_pointers.push_back(op_s); 
  }
  
  //===================================
  // Operator *pred_oper = nullptr;
  rc = get_predictor_tree(tables, out_tables, filter_units, table_operator_mp, stmt_pointers, op_pointers, op);
  return rc;
}

bool is_out_table(const std::vector<Table *> in_tables, Expression *expr) {
  bool is_out_field = true;

  if(expr->type() != ExprType::FIELD) {
    return false;
  }

  FieldExpr &field = *(FieldExpr *)(expr);
  for(std::size_t j = 0; j < in_tables.size(); j++) {
    if(strcmp(field.table_name(), in_tables[j]->name()) == 0) {
      is_out_field = false;
    }
  }
  return is_out_field;
}

bool is_in_table(const std::vector<Table *> in_tables, Expression *expr) {
  bool is_in_field = false;
  if(expr->type() == ExprType::QUERY) {
    return true;
  }
  if(expr->type() != ExprType::FIELD) {
    return false;
  }

  FieldExpr &field = *(FieldExpr *)(expr);
  for(std::size_t j = 0; j < in_tables.size(); j++) {
    if(strcmp(field.table_name(), in_tables[j]->name()) == 0) {
      is_in_field = true;
    }
  }
  return is_in_field;
}
void get_out_table(std::vector<Table*> &out_tables, const std::vector<Table*> &father_tables, const std::vector<Table *> &father_out_tables, const std::vector<Table *> &in_tables) {
  bool is_out = true;
  for(auto father : father_tables) {
    is_out = true;
    for(auto in_table : in_tables) {
      if(strcmp(father->name(), in_table->name()) == 0) {
        is_out = false;
      }
    }
    if(is_out == true) {
      out_tables.push_back(father);
    }
  }
  for(auto father : father_out_tables) {
    for(auto in_table : in_tables) {
      if(strcmp(father->name(), in_table->name()) == 0) {
        is_out = false;
      }
    }
    if(is_out == true) {
      out_tables.push_back(father);
    }
  }
}

RC ExecuteStage::create_sub_select(SelectStmt *select_stmt_sub, const std::vector<Table *> &father_tables, const std::vector<Table *> &father_out_tables, std::vector<TmpStmt *> &stmt_pointers, std::vector<Operator *> &op_pointers, Operator *&sub_result) {
  RC rc = RC::SUCCESS;
  std::vector<int> erase_int_list;
  std::vector<FilterUnit *> out_out;
  std::vector<FilterUnit *> in_in;
  std::vector<FilterUnit *> in_out;
  std::vector<bool> swaps;
  for(std::size_t i = 0; i < select_stmt_sub->filter_stmt()->filter_units().size(); i++) {
    FilterUnit *sub_filter_unit = select_stmt_sub->filter_stmt()->filter_units()[i];
    //left out column
    if(is_out_table(select_stmt_sub->tables(), sub_filter_unit->left()) && is_out_table(select_stmt_sub->tables(), sub_filter_unit->right())) {
      sub_filter_unit->left()->set_is_out(true);
      sub_filter_unit->right()->set_is_out(true);
      out_out.push_back(sub_filter_unit);
    } else if(is_in_table(select_stmt_sub->tables(), sub_filter_unit->left()) && is_out_table(select_stmt_sub->tables(), sub_filter_unit->right())) {
      sub_filter_unit->left()->set_is_out(false);
      sub_filter_unit->right()->set_is_out(true);
      in_out.push_back(sub_filter_unit);
      swaps.push_back(false);
    } else if(is_out_table(select_stmt_sub->tables(), sub_filter_unit->left()) && is_in_table(select_stmt_sub->tables(), sub_filter_unit->right())) {
      swaps.push_back(true);
      sub_filter_unit->left()->set_is_out(true);
      sub_filter_unit->right()->set_is_out(false);
      in_out.push_back(sub_filter_unit);
    } else if(is_out_table(select_stmt_sub->tables(), sub_filter_unit->left())){
      sub_filter_unit->left()->set_is_out(true);
      sub_filter_unit->right()->set_is_out(false);
      out_out.push_back(sub_filter_unit);
    } else if(is_out_table(select_stmt_sub->tables(), sub_filter_unit->right())){
      sub_filter_unit->left()->set_is_out(false);
      sub_filter_unit->right()->set_is_out(true);
      out_out.push_back(sub_filter_unit);
    } else {
      sub_filter_unit->left()->set_is_out(false);
      sub_filter_unit->right()->set_is_out(false);
      in_in.push_back(sub_filter_unit);
    }
  }
  std::vector<Table *> sub_tables;
  get_out_table(sub_tables, father_tables, father_out_tables, select_stmt_sub->tables());

  TmpStmt *sss = new TmpStmt();
  stmt_pointers.push_back(sss);
  Operator *op_sub = new PredicateOperator(sss);
  op_pointers.push_back(op_sub);
  
  rc = get_operator(select_stmt_sub->tables(), sub_tables, in_in, op_sub, stmt_pointers, op_pointers);
  sub_result = op_sub;
  // if(out_out.empty() && in_out.empty()) {
  //   return RC::NOT_RELATIVE;
  // }

  SubOperator *sub_oper = new SubOperator();
  op_pointers.push_back(sub_oper);
  sub_oper->add_child(op_sub);
  sub_oper->add_units(out_out);
  sub_oper->add_units(in_out);
  sub_result = sub_oper;
  if(out_out.empty() && in_out.empty()) {
    return RC::NOT_RELATIVE;
  }
  return rc;
}

RC ExecuteStage::get_expr(SelectStmt *select_stmt_sub, Operator *&sub_result, ValueListExpr *&expr) {
  RC rc = RC::SUCCESS;
  MemTuple mem_tuples;
  ProjectOperator poj;
  poj.add_child(sub_result);
  if(select_stmt_sub->tables().size() == 1) {
    for (const Field &field : select_stmt_sub->query_fields()) {
      poj.add_projection(field.table(), field.meta());
    }
  } else {
    for (const Field &field : select_stmt_sub->query_fields()) {
      poj.add_multi_table_projection(field.table(), field.meta());
    }
  }
  rc = poj.open();
  if (rc != RC::SUCCESS) {
    LOG_WARN("failed to open operator");
    return rc;
  }
  poj.add_child(sub_result);
  rc = read_operator(&poj, &mem_tuples);
  if(rc != RC::RECORD_EOF) {
    return rc;
  }
  if(mem_tuples.length() == 0) {
    return RC::SUCCESS;
  }
  if(mem_tuples.length() > 1 && mem_tuples.get(0)->length() > 1) {
    if(select_stmt_sub->query_fields().size() != select_stmt_sub->aggrs().size()) {
      return RC::INVALID_ARGUMENT;
    }
  }
  if(select_stmt_sub->aggrs().empty()) {
    for(int i = 0; i < mem_tuples.length(); i++) {
      expr->add(mem_tuples.get(i)->get(0));
    }
  } else {
    MemRowTuple print_row;
    if(mem_tuples.length() != 0) {
      for(std::size_t i = 0; i < select_stmt_sub->aggrs().size(); i++) {
        const char *p = "1";
        print_row.add(p,4,AttrType::CHARS);
        get_result(&mem_tuples, select_stmt_sub->aggrs()[i], i, 0 , mem_tuples.length(), print_row.get(i));
        expr->add(print_row.get(i));
      }
    }
  }
  return rc;  
}

RC ExecuteStage::get_predictor_tree(const std::vector<Table *> &father_tables, const std::vector<Table *> &father_out_tables, const std::vector<FilterUnit *> &filter_units, std::map<std::string, Operator *> table_operator_mp, std::vector<TmpStmt *> &stmt_pointers, std::vector<Operator *> &op_pointers, Operator* &op) {
  RC rc = RC::SUCCESS;
  SubPredicateOperator *sub_predicate_oper = new SubPredicateOperator();
  op_pointers.push_back(sub_predicate_oper);
  std::set<std::pair<std::string, std::string>> table2tables;
  //single table map
  std::map<std::string, TmpStmt*> mp;
  std::map<std::pair<std::string, std::string>, TmpStmt*> table2table_mp;
  std::map<std::string, std::vector<Operator*>> table_2_sub_ops;
  for (const FilterUnit *filter_unit1 : filter_units) {
    FilterUnit *filter_unit = const_cast<FilterUnit *>(filter_unit1);
    Expression *left_expr = filter_unit->left();
    Expression *right_expr = filter_unit->right();
    CompOp comp = filter_unit->comp();
    // single table
    const Table *table;
    if(left_expr->type() == ExprType::FIELD && (right_expr->type() == ExprType::LIST)) {
      FieldExpr &left_field_expr = *(FieldExpr *)left_expr;
      const Field &field = left_field_expr.field();
      table = field.table();
      std::string s(table->name());
      if(mp.find(table->name()) == mp.end()) {
        mp[s] = new TmpStmt();
        stmt_pointers.push_back(mp[s]);
      } 
      mp[s]->filter_units.push_back(filter_unit);
    } else if(left_expr->type() == ExprType::LIST && right_expr->type() == ExprType::FIELD) {
      ValueListExpr &left_field_expr = *(ValueListExpr *)left_expr;
      if(left_field_expr.length() != 1) {
        return RC::INVALID_ARGUMENT;
      }
      FieldExpr &right_field_expr = *(FieldExpr *)right_expr;
      const Field &field = right_field_expr.field();
      table = field.table();
      std::string s(table->name());
      if(mp.find(table->name()) == mp.end()) {
        mp[s] = new TmpStmt();
        stmt_pointers.push_back(mp[s]);
      } 
      mp[s]->filter_units.push_back(filter_unit);
    } else if(left_expr->type() == ExprType::FIELD && right_expr->type() == ExprType::FIELD){
      //table2table
      //two tables
      if(comp == NOT_IN ||comp == EXIST_IN || comp == IS_IN || comp == NOT_EXIST) {
        return RC::INVALID_ARGUMENT;
      }
      FieldExpr &left_field_expr = *(FieldExpr *)left_expr;
      const Field &left_field = left_field_expr.field();
      const Table *left_table = left_field.table();
      FieldExpr &right_fild_expr = *(FieldExpr *)right_expr;
      const Field &right_field = right_fild_expr.field();
      const Table *right_table = right_field.table();
      //
      std::pair<std::string, std::string> table_pair;
      if(strcmp(left_table->name(), right_table->name()) > 0) {
        std::string s1(left_table->name());
        std::string s2(right_table->name());
        std::pair<std::string, std::string> p(s1,s2);
        table2tables.insert(p);
        table_pair = p;
      } else if(strcmp(left_table->name(), right_table->name()) < 0){
        std::string s1(right_table->name());
        std::string s2(left_table->name());
        std::pair<std::string, std::string> p(s1,s2);
        table2tables.insert(p);
        table_pair = p;
      } else {
        //single table
        std::string s(left_table->name());
        if(mp.find(s) == mp.end()) {
          mp[s] = new TmpStmt();
          stmt_pointers.push_back(mp[s]);
        } 
        mp[s]->filter_units.push_back(filter_unit);
        continue;
      }
      //multi tables
      if(table2table_mp.find(table_pair) == table2table_mp.end()) {
        table2table_mp[table_pair] = new TmpStmt();
        stmt_pointers.push_back(table2table_mp[table_pair]);
      }
      table2table_mp[table_pair]->filter_units.push_back(filter_unit);
    } else if(left_expr->type() == ExprType::LIST && right_expr->type() == ExprType::LIST){
      //value-2-value
      ValueListExpr &left_value_expr = *(ValueListExpr *)left_expr;
      ValueListExpr &right_value_expr = *(ValueListExpr *)right_expr;
      if(left_value_expr.length() != 1) {
        return RC::INVALID_ARGUMENT;
      }
      if(comp >= EQUAL_TO && comp <= GREAT_THAN && right_value_expr.length() != 1) {
        return RC::INVALID_ARGUMENT;
      }
      std::string s;
      for(std::pair<std::string, Operator*> kv: table_operator_mp) {
        s = kv.first;
      }
      if(mp.find(s) == mp.end()) {
        mp[s] = new TmpStmt();
          stmt_pointers.push_back(mp[s]);
        } 
        mp[s]->filter_units.push_back(filter_unit);
    } else if(left_expr->type() == ExprType::LIST && right_expr->type() == ExprType::QUERY) {
      ValueListExpr &left_value_expr = *(ValueListExpr *)left_expr;
      if(left_value_expr.length() != 1) {
        return RC::INVALID_ARGUMENT;
      }
      QueryExpr &right_field_expr = *(QueryExpr *)right_expr;
      SelectStmt *select_stmt_sub = (SelectStmt *)(right_field_expr.get_stmt());
      Operator *sub_result;
      rc = create_sub_select(select_stmt_sub, father_tables, father_out_tables,stmt_pointers, op_pointers,sub_result);
      if(rc == RC::SUCCESS || rc == RC::NOT_RELATIVE) {
        //relative select
        sub_predicate_oper->add_child(sub_result);
        sub_predicate_oper->add_unit(filter_unit);
        sub_predicate_oper->expr_2_stmt[filter_unit->right()] = sub_result;
      } else {
        return rc;
      }
    } else if(left_expr->type() == ExprType::QUERY && right_expr->type() == ExprType::QUERY) {
      bool is_add_unit = false;
      QueryExpr &left_field_expr = *(QueryExpr *)left_expr;
      QueryExpr &right_field_expr = *(QueryExpr *)right_expr;
      SelectStmt *select_stmt_sub_left = (SelectStmt *)(left_field_expr.get_stmt());
      Operator *sub_result_left;
      rc = create_sub_select(select_stmt_sub_left, father_tables, father_out_tables,stmt_pointers, op_pointers,sub_result_left);
      if(rc == RC::SUCCESS || rc == RC::NOT_RELATIVE) {
        is_add_unit = true;
        sub_predicate_oper->add_child(sub_result_left);
        sub_predicate_oper->add_unit(filter_unit);
        sub_predicate_oper->expr_2_stmt[filter_unit->left()] = sub_result_left;
      } else {
        return rc;
      }
      //relative select
      
      SelectStmt *select_stmt_sub_right = (SelectStmt *)(right_field_expr.get_stmt());
      Operator *sub_result_right;
      rc = create_sub_select(select_stmt_sub_right, father_tables, father_out_tables,stmt_pointers, op_pointers,sub_result_right);
      if(rc == RC::SUCCESS || rc == RC::NOT_RELATIVE) {
        if(is_add_unit == false) {
          sub_predicate_oper->add_unit(filter_unit);
        }
        sub_predicate_oper->add_child(sub_result_right);
        sub_predicate_oper->expr_2_stmt[filter_unit->right()] = sub_result_right;
      } else {
        return rc;
      }
      
    } else if(left_expr->type() == ExprType::QUERY && right_expr->type() == ExprType::FIELD) {
      QueryExpr &left_field_expr = *(QueryExpr *)left_expr;
      SelectStmt *select_stmt_sub = (SelectStmt *)(left_field_expr.get_stmt());
      Operator *sub_result;
      rc = create_sub_select(select_stmt_sub, father_tables, father_out_tables,stmt_pointers, op_pointers,sub_result);
      if(rc == RC::SUCCESS || rc == RC::NOT_RELATIVE) {
        //relative select
        sub_predicate_oper->add_child(sub_result);
        sub_predicate_oper->add_unit(filter_unit);
        sub_predicate_oper->expr_2_stmt[filter_unit->left()] = sub_result;
      } else {
        return rc;
      }
      
    } else if(left_expr->type() == ExprType::QUERY && right_expr->type() == ExprType::LIST) {
      QueryExpr &left_field_expr = *(QueryExpr *)left_expr;
      SelectStmt *select_stmt_sub = (SelectStmt *)(left_field_expr.get_stmt());
      Operator *sub_result;
      rc = create_sub_select(select_stmt_sub, father_tables, father_out_tables,stmt_pointers, op_pointers,sub_result);
      if(rc == RC::SUCCESS || rc == RC::NOT_RELATIVE) {
        //relative select
        sub_predicate_oper->add_child(sub_result);
        sub_predicate_oper->add_unit(filter_unit);
        sub_predicate_oper->expr_2_stmt[filter_unit->left()] = sub_result;
      } else {
        return rc;
      }
    } else if(left_expr->type() == ExprType::NONE && right_expr->type() == ExprType::QUERY) {
      // FieldExpr &left_field_expr = *(FieldExpr *)left_expr;
      QueryExpr &right_field_expr = *(QueryExpr *)right_expr;
      SelectStmt *select_stmt_sub = (SelectStmt *)(right_field_expr.get_stmt());
      Operator *sub_result;
      rc = create_sub_select(select_stmt_sub, father_tables, father_out_tables,stmt_pointers, op_pointers,sub_result);
      if(rc == RC::NOT_RELATIVE) {
        return RC::INVALID_ARGUMENT;
      } else if(rc != RC::SUCCESS) {
        return rc;
      } else {
        //relative select
        sub_predicate_oper->add_child(sub_result);
        sub_predicate_oper->add_unit(filter_unit);
        sub_predicate_oper->expr_2_stmt[filter_unit->right()] = sub_result;
      }
      
    }else if(left_expr->type() == ExprType::FIELD && right_expr->type() == ExprType::QUERY) {
      QueryExpr &right_field_expr = *(QueryExpr *)right_expr;
      SelectStmt *select_stmt_sub = (SelectStmt *)(right_field_expr.get_stmt());
      Operator *sub_result;
      rc = create_sub_select(select_stmt_sub, father_tables, father_out_tables,stmt_pointers, op_pointers,sub_result);
      if(rc == RC::SUCCESS || rc == RC::NOT_RELATIVE) {
        //relative select
        sub_predicate_oper->add_child(sub_result);
        sub_predicate_oper->add_unit(filter_unit);
        sub_predicate_oper->expr_2_stmt[filter_unit->right()] = sub_result;
      } else {
        return rc;
      }
      
    } 
  }
  std::map<std::string, Operator *> pred_mp;
  for(std::pair<std::string, Operator*> kv: table_operator_mp) {
    if(mp.find(kv.first) == mp.end()) {
      pred_mp[kv.first] = kv.second;
    } else {
      PredicateOperator *pred = new PredicateOperator(mp[kv.first]);
      op_pointers.push_back(pred);
      pred->add_child(kv.second);
      pred_mp[kv.first] = pred;
    }
  }
  for(std::pair<std::pair<std::string, std::string>, TmpStmt*> kv : table2table_mp) {
    if(pred_mp[kv.first.first] != pred_mp[kv.first.second]) {
      PredicateOperator *pred = new PredicateOperator(kv.second);
      op_pointers.push_back(pred);
      pred->add_child(pred_mp[kv.first.first]);
      pred->add_child(pred_mp[kv.first.second]);

      // pred_mp[kv.first.first] = pred;
      Operator *op = pred_mp[kv.first.first];
      std::vector<std::string> op_v;
      for(std::pair<std::string, Operator*> pred_kv : pred_mp) {
        if(op == pred_kv.second) {
          op_v.push_back(pred_kv.first);
        }
      }
      for(std::string s : op_v) {
        pred_mp[s] = pred;
      }
      // right
      op = pred_mp[kv.first.second];
      op_v.clear();
      for(std::pair<std::string, Operator*> pred_kv : pred_mp) {
        if(op == pred_kv.second) {
          op_v.push_back(pred_kv.first);
        }
      }
      for(std::string s : op_v) {
        pred_mp[s] = pred;
      }
    } else {
      for (FilterUnit *filter_unit : kv.second->filter_units) {
        static_cast<PredicateOperator *>(pred_mp[kv.first.first])->add_unit(filter_unit);
      }
    }
  }
  std::vector<Operator*> op_set;
  bool is_same = false;
  for(std::pair<std::string, Operator*> kv : pred_mp) {
    is_same = false;
    for(Operator *op: op_set) {
      if(op == kv.second) {
        is_same = true;
      }
    }
    if(!is_same) {
      op_set.push_back(kv.second);
    }
  }
  TmpStmt *stmt = new TmpStmt();
  stmt_pointers.push_back(stmt);
  PredicateOperator *pred = new PredicateOperator(stmt);
  op_pointers.push_back(pred);
  int count = 0;
  for(Operator *op :op_set) {
    pred->add_child(op);
    count++;
    if(count == 2) {
      stmt = new TmpStmt();
      stmt_pointers.push_back(stmt);
      PredicateOperator *p = new PredicateOperator(stmt);
      op_pointers.push_back(p);
      p->add_child(pred);
      count = 1;
      pred = p;
    }
  }
  
  if(sub_predicate_oper->children_num() != 0) {
    sub_predicate_oper->add_child(pred);
    op->add_child(sub_predicate_oper);
  } else {
    op->add_child(pred);
  }
  return rc;
}

void clear_mem(std::vector<TmpStmt *> &stmt_pointers, std::vector<Operator *> &op_pointers) {
  // for(auto pointer : unit_pointers) {
  //   delete pointer;
  // }
  for(auto pointer : stmt_pointers) {
    if(pointer) {
      delete pointer;
      pointer = nullptr;
    }
  }
  stmt_pointers.clear();
  for(auto pointer : op_pointers) {
    if(pointer) {
      delete pointer;
      pointer = nullptr;
    }
  }
  op_pointers.clear();
}

bool compare_tuple(SelectStmt *select_stmt, MemTuple &mem_tuples, int start, int having_start, int i, CompOp comp) {
  const char *p = "1";
  TulpeCellOut *left_cell = new TulpeCellOut(p ,4, CHARS);
  TulpeCellOut *right_cell = new TulpeCellOut(p ,4, CHARS);;
  if(select_stmt->condition_.right_type == VALUE) {
    right_cell->set_data((char*)select_stmt->condition_.right_value.data);
    right_cell->set_type(select_stmt->condition_.right_value.type);
    if(select_stmt->condition_.right_value.type == CHARS) {
      right_cell->set_length(strlen((const char *)select_stmt->condition_.right_value.data));
    }
  } else if(select_stmt->condition_.right_type == FIELD){
    right_cell->set_data(mem_tuples.get(start)->get(having_start)->data());
    right_cell->set_type(mem_tuples.get(start)->get(having_start)->attr_type());
    right_cell->set_length(mem_tuples.get(start)->get(having_start)->length());
    having_start++;
  } else {
    RelAggr relagg;
    relagg.attribute_name = const_cast<char *>(select_stmt->query_fields()[having_start].field_name());
    relagg.relation_name = const_cast<char *>(select_stmt->query_fields()[having_start].table_name());
    relagg.aggregator_type = select_stmt->query_fields()[having_start].agg;
    get_result(&mem_tuples, relagg, having_start, start, i, right_cell);
    having_start++;
  }
  
  if(select_stmt->condition_.left_type == VALUE) {
    left_cell->set_data((char*)select_stmt->condition_.left_value.data);
    left_cell->set_type(select_stmt->condition_.left_value.type);
    if(select_stmt->condition_.left_value.type == CHARS) {
      left_cell->set_length(strlen((const char *)select_stmt->condition_.left_value.data));
    }
  } else if(select_stmt->condition_.left_type == FIELD){
    left_cell->set_data(mem_tuples.get(start)->get(having_start)->data());
    left_cell->set_type(mem_tuples.get(start)->get(having_start)->attr_type());
    left_cell->set_length(mem_tuples.get(start)->get(having_start)->length());
  } else {
    RelAggr relagg;
    relagg.attribute_name = const_cast<char *>(select_stmt->query_fields()[having_start].field_name());
    relagg.relation_name = const_cast<char *>(select_stmt->query_fields()[having_start].table_name());
    relagg.aggregator_type = select_stmt->query_fields()[having_start].agg;
    get_result(&mem_tuples, relagg, having_start, start, i, left_cell);
  }


  int compare = left_cell->compare(*right_cell);
  bool filter_result;
  switch (comp) {
  case EQUAL_TO: {
    filter_result = (0 == compare); 
  filter_result = (0 == compare); 
    filter_result = (0 == compare); 
  } break;
  case LESS_EQUAL: {
    filter_result = (compare <= 0); 
  filter_result = (compare <= 0); 
    filter_result = (compare <= 0); 
  } break;
  case NOT_EQUAL: {
    filter_result = (compare != 0);
  } break;
  case LESS_THAN: {
    filter_result = (compare < 0);
  } break;
  case GREAT_EQUAL: {
    filter_result = (compare >= 0);
  } break;
  case GREAT_THAN: {
    filter_result = (compare > 0);
  } break;
  default: {
    LOG_WARN("invalid compare type: %d", comp);
  } break;
  }
  delete right_cell;
  delete left_cell;
  return filter_result;
}

RC ExecuteStage::do_select(SQLStageEvent *sql_event)
{
  SelectStmt *select_stmt = (SelectStmt *)(sql_event->stmt());
  SessionEvent *session_event = sql_event->session_event();
  RC rc = RC::SUCCESS;
  ProjectOperator project_oper;
  std::vector<TmpStmt *> stmt_pointers;
  std::vector<Operator *> op_pointers;
  TmpStmt tmp_stmt;
  PredicateOperator pred_oper(&tmp_stmt);
  Operator *pred_op = &pred_oper;
  project_oper.add_child(pred_op);
  rc = get_operator(select_stmt->tables(), select_stmt->out_tables(), select_stmt->filter_stmt()->filter_units(), pred_op, stmt_pointers, op_pointers);
  if (rc != RC::SUCCESS && rc != NOT_RELATIVE && rc != RC::RECORD_EOF) {
    session_event->set_response("FAILURE\n");
    clear_mem(stmt_pointers,op_pointers);
    return rc;
  }

  std::vector<RecordFileHandler*> handlers;
  if(select_stmt->tables().size() == 1) {
    for (const Field &field : select_stmt->query_fields()) {
      project_oper.add_projection(field.table(), field.meta());
      handlers.push_back(field.table()->record_handler());
    }
  } else {
    for (const Field &field : select_stmt->query_fields()) {
      project_oper.add_multi_table_projection(field.table(), field.meta());
      handlers.push_back(field.table()->record_handler());
    }
  }
  rc = project_oper.open();
  if (rc != RC::SUCCESS) {
    LOG_WARN("failed to open operator");
    clear_mem(stmt_pointers,op_pointers);
    return rc;
  }

  MemTuple mem_tuples;
  rc = read_operator(&project_oper, &mem_tuples);
  if (rc != RC::RECORD_EOF) {
    LOG_WARN("something wrong while iterate operator. rc=%s", strrc(rc));
    clear_mem(stmt_pointers,op_pointers);
    session_event->set_response("FAILURE\n");
    return rc;
  }
  //first group
  std::vector<Order> orders;
  std::size_t group_start = 0;
  std::size_t group_end = 0;
  std::size_t query_start = 0;
  std::size_t having_start = 0;
  for(std::size_t k = 0; k < select_stmt->query_fields().size(); k++) {
    if(select_stmt->query_fields()[k].field_type == GROUPFIELD) {
      group_start= k;
      break;
    }
  }

  for(std::size_t k = 0; k < select_stmt->query_fields().size(); k++) {
    if(select_stmt->query_fields()[k].field_type == HAVINGFIELD) {
      having_start= k;
      break;
    }
  }
  for(std::size_t k = group_start; k < select_stmt->query_fields().size(); k++) {
    if(select_stmt->query_fields()[k].field_type != GROUPFIELD) {
      group_end= k;
      break;
    }
  }
  for(std::size_t k = 0; k < select_stmt->query_fields().size(); k++) {
    if(select_stmt->query_fields()[k].field_type == SELECTFIELD) {
      query_start = k;
      break;
    }
  }
  for(int i = 0; i < select_stmt->group_num; i++) {
    Order order;
    order.asc = true;
    orders.push_back(order);
  }
  mem_tuples.sort(group_start, orders);
  

  std::stringstream ss;
  print_tuple_header(ss, project_oper, query_start, select_stmt->query_fields());
  if(select_stmt->group_num > 0) {
    int start = 0;
    for(int i = 1; i < mem_tuples.length(); i++) {
      bool is_same = true;
      for(std::size_t j = group_start; j < group_end; j++) {
        if(mem_tuples.get(i)->get(j)->compare(*mem_tuples.get(start)->get(j)) != 0) {
          is_same = false;
          break;
        }
      }
      if(is_same == false) {
        //filter
        MemRowTuple print_row;
        
        for(std::size_t j = query_start; j < select_stmt->query_fields().size();) {
          const char *p = "1";
          print_row.add(p,4,AttrType::CHARS);
          if(select_stmt->query_fields()[j].is_agg == true) {
            RelAggr relagg;
            relagg.attribute_name = const_cast<char *>(select_stmt->query_fields()[j].field_name());
            relagg.relation_name = const_cast<char *>(select_stmt->query_fields()[j].table_name());
            relagg.aggregator_type = select_stmt->query_fields()[j].agg;
            get_result(&mem_tuples, relagg, j, start, i, print_row.get(print_row.length() - 1));
            j = select_stmt->query_fields()[j].end;
          } else {
            print_row.get(print_row.length() - 1)->set_type(select_stmt->query_fields()[j].meta()->type());
            print_row.get(print_row.length() - 1)->set_data(mem_tuples.get(start)->get(j)->data());
            j++;
          }
        }
        if(select_stmt->is_having == true && compare_tuple(select_stmt,mem_tuples,start, having_start, i, select_stmt->condition_.comp) == false) {
          
        } else {
          tuple_to_string(ss, print_row, handlers, 0);
          ss << std::endl;
        }
        //print
        start = i;
      }
    }

    //filter
    MemRowTuple print_row;
    
    for(std::size_t j = query_start; j < select_stmt->query_fields().size();) {
      const char *p = "1";
      print_row.add(p,4,AttrType::CHARS);
      if(select_stmt->query_fields()[j].is_agg == true) {
        RelAggr relagg;
        relagg.attribute_name = const_cast<char *>(select_stmt->query_fields()[j].field_name());
        relagg.relation_name = const_cast<char *>(select_stmt->query_fields()[j].table_name());
        relagg.aggregator_type = select_stmt->query_fields()[j].agg;
        get_result(&mem_tuples, relagg, j, start, mem_tuples.length(), print_row.get(print_row.length() - 1));
        j = select_stmt->query_fields()[j].end;
      } else {
        print_row.get(print_row.length() - 1)->set_type(select_stmt->query_fields()[j].meta()->type());
        print_row.get(print_row.length() - 1)->set_data(mem_tuples.get(start)->get(j)->data());
        j++;
      }
    }
    if(select_stmt->is_having == true && compare_tuple(select_stmt,mem_tuples,start, having_start, mem_tuples.length(), select_stmt->condition_.comp) == false) {
      
    } else {
      tuple_to_string(ss, print_row, handlers, 0);
      ss << std::endl;
    }
    //print
    start = mem_tuples.length();
  } else {
    int order_start = 0;
    for(std::size_t k = 0; k < select_stmt->query_fields().size(); k++) {
      if(select_stmt->query_fields()[k].field_type == ORDERFIELD) {
        order_start = k;
        break;
      }
    }
    mem_tuples.sort(order_start, select_stmt->orders());
    if(select_stmt->aggrs().empty()) {
      for(int i = 0; i < mem_tuples.length(); i++) {
        tuple_to_string(ss, *(mem_tuples.get(i)), handlers, select_stmt->orders().size());
        ss << std::endl;
      }
    } else {
      MemRowTuple print_row;
      if(mem_tuples.length() != 0) {
        for(std::size_t i = 0; i < select_stmt->aggrs().size(); i++) {
          const char *p = "1";
          print_row.add(p,4,AttrType::CHARS);
          get_result(&mem_tuples, select_stmt->aggrs()[i], i, 0, mem_tuples.length(), print_row.get(i));
        }
        tuple_to_string(ss, print_row, handlers, select_stmt->orders().size());
        ss << std::endl;
      }
    }
  }
  
  if (rc != RC::SUCCESS && rc != RC::RECORD_EOF) {
    LOG_WARN("something wrong while iterate operator. rc=%s", strrc(rc));
    project_oper.close();
    clear_mem(stmt_pointers,op_pointers);
    session_event->set_response("FAILURE\n");
    return rc;
  } else {
    rc = project_oper.close();
    clear_mem(stmt_pointers,op_pointers);
  }
  session_event->set_response(ss.str());
  return rc;
}


RC ExecuteStage::do_help(SQLStageEvent *sql_event)
{
  SessionEvent *session_event = sql_event->session_event();
  const char *response = "show tables;\n"
                         "desc `table name`;\n"
                         "create table `table name` (`column name` `column type`, ...);\n"
                         "create index `index name` on `table` (`column`);\n"
                         "insert into `table` values(`value1`,`value2`);\n"
                         "update `table` set column=value [where `column`=`value`];\n"
                         "delete from `table` [where `column`=`value`];\n"
                         "select [ * | `columns` ] from `table`;\n";
  session_event->set_response(response);
  return RC::SUCCESS;
}

RC ExecuteStage::do_create_table(SQLStageEvent *sql_event)
{
  const CreateTable &create_table = sql_event->query()->sstr.create_table;
  SessionEvent *session_event = sql_event->session_event();
  Db *db = session_event->session()->get_current_db();
  RC rc = db->create_table(create_table.relation_name,
			create_table.attribute_count, create_table.attributes);
  if (rc == RC::SUCCESS) {
    session_event->set_response("SUCCESS\n");
  } else {
    session_event->set_response("FAILURE\n");
  }
  return rc;
}
RC ExecuteStage::do_create_index(SQLStageEvent *sql_event)
{
  SessionEvent *session_event = sql_event->session_event();
  Db *db = session_event->session()->get_current_db();
  const CreateIndex &create_index = sql_event->query()->sstr.create_index;
  Table *table = db->find_table(create_index.relation_name);
  if (nullptr == table) {
    session_event->set_response("FAILURE\n");
    return RC::SCHEMA_TABLE_NOT_EXIST;
  }

  RC rc = table->create_index(nullptr, create_index.index_name, create_index.attributes_name, create_index.attribute_num, create_index.is_unique);
  sql_event->session_event()->set_response(rc == RC::SUCCESS ? "SUCCESS\n" : "FAILURE\n");
  return rc;
}

RC ExecuteStage::do_show_tables(SQLStageEvent *sql_event)
{
  SessionEvent *session_event = sql_event->session_event();
  Db *db = session_event->session()->get_current_db();
  std::vector<std::string> all_tables;
  db->all_tables(all_tables);
  if (all_tables.empty()) {
    session_event->set_response("No table\n");
  } else {
    std::stringstream ss;
    for (const auto &table : all_tables) {
      ss << table << std::endl;
    }
    session_event->set_response(ss.str().c_str());
  }
  return RC::SUCCESS;
}

RC ExecuteStage::do_desc_table(SQLStageEvent *sql_event)
{
  Query *query = sql_event->query();
  Db *db = sql_event->session_event()->session()->get_current_db();
  const char *table_name = query->sstr.desc_table.relation_name;
  Table *table = db->find_table(table_name);
  std::stringstream ss;
  if (table != nullptr) {
    table->table_meta().desc(ss);
  } else {
    ss << "No such table: " << table_name << std::endl;
  }
  sql_event->session_event()->set_response(ss.str().c_str());
  return RC::SUCCESS;
}

RC ExecuteStage::do_insert(SQLStageEvent *sql_event)
{
  Stmt *stmt = sql_event->stmt();
  SessionEvent *session_event = sql_event->session_event();
  Session *session = session_event->session();
  Db *db = session->get_current_db();
  Trx *trx = session->current_trx();
  CLogManager *clog_manager = db->get_clog_manager();

  if (stmt == nullptr) {
    LOG_WARN("cannot find statement");
    return RC::GENERIC_ERROR;
  }

  InsertStmt *insert_stmt = (InsertStmt *)stmt;
  Table *table = insert_stmt->table();
  const TableMeta table_meta = table->table_meta();

  RC rc = RC::SUCCESS;
  const int field_num = table_meta.field_num() - table_meta.sys_field_num();
  int offset = 0;
  for (int i = 0; i < insert_stmt->tuple_amount(); i++) {
    rc = table->insert_record(trx, field_num, insert_stmt->values() + offset);
    if (rc != RC::SUCCESS) {
      break;
    }   
    offset += field_num;
  }

  if (rc == RC::SUCCESS) {
    if (!session->is_trx_multi_operation_mode()) {
      CLogRecord *clog_record = nullptr;
      rc = clog_manager->clog_gen_record(CLogType::REDO_MTR_COMMIT, trx->get_current_id(), clog_record);
      if (rc != RC::SUCCESS || clog_record == nullptr) {
        session_event->set_response("FAILURE\n");
        return rc;
      }

      rc = clog_manager->clog_append_record(clog_record);
      if (rc != RC::SUCCESS) {
        session_event->set_response("FAILURE\n");
        return rc;
      } 

      trx->next_current_id();
      session_event->set_response("SUCCESS\n");
    } else {
      session_event->set_response("SUCCESS\n");
    }
  } else {
    if (rc != RC::RECORD_DUPLICATE_KEY) {
      trx->rollback();
    }
    session_event->set_response("FAILURE\n");
  }
  return rc;
}

RC ExecuteStage::do_delete(SQLStageEvent *sql_event)
{
  Stmt *stmt = sql_event->stmt();
  SessionEvent *session_event = sql_event->session_event();
  Session *session = session_event->session();
  Db *db = session->get_current_db();
  Trx *trx = session->current_trx();
  CLogManager *clog_manager = db->get_clog_manager();

  if (stmt == nullptr) {
    LOG_WARN("cannot find statement");
    return RC::GENERIC_ERROR;
  }

  DeleteStmt *delete_stmt = (DeleteStmt *)stmt;
  TableScanOperator scan_oper(delete_stmt->table());
  PredicateOperator pred_oper(delete_stmt->filter_stmt());
  pred_oper.add_child(&scan_oper);
  DeleteOperator delete_oper(delete_stmt, trx);
  delete_oper.add_child(&pred_oper);

  RC rc = transform_predicate_expression_to_date_if_nessasery(delete_stmt->filter_stmt()->filter_units());
  if (rc != RC::SUCCESS) {
    session_event->set_response("FAILURE\n");
    return rc;
  }

  rc = delete_oper.open();
  if (rc != RC::SUCCESS) {
    session_event->set_response("FAILURE\n");
  } else {
    session_event->set_response("SUCCESS\n");
    if (!session->is_trx_multi_operation_mode()) {
      CLogRecord *clog_record = nullptr;
      rc = clog_manager->clog_gen_record(CLogType::REDO_MTR_COMMIT, trx->get_current_id(), clog_record);
      if (rc != RC::SUCCESS || clog_record == nullptr) {
        session_event->set_response("FAILURE\n");
        return rc;
      }

      rc = clog_manager->clog_append_record(clog_record);
      if (rc != RC::SUCCESS) {
        session_event->set_response("FAILURE\n");
        return rc;
      } 

      trx->next_current_id();
      session_event->set_response("SUCCESS\n");
    }
  }
  return rc;
}

RC ExecuteStage::do_begin(SQLStageEvent *sql_event)
{
  RC rc = RC::SUCCESS;
  SessionEvent *session_event = sql_event->session_event();
  Session *session = session_event->session();
  Db *db = session->get_current_db();
  Trx *trx = session->current_trx();
  CLogManager *clog_manager = db->get_clog_manager();

  session->set_trx_multi_operation_mode(true);

  CLogRecord *clog_record = nullptr;
  rc = clog_manager->clog_gen_record(CLogType::REDO_MTR_BEGIN, trx->get_current_id(), clog_record);
  if (rc != RC::SUCCESS || clog_record == nullptr) {
    session_event->set_response("FAILURE\n");
    return rc;
  }

  rc = clog_manager->clog_append_record(clog_record);
  if (rc != RC::SUCCESS) {
    session_event->set_response("FAILURE\n");
  } else {
    session_event->set_response("SUCCESS\n");
  }

  return rc;
}

RC ExecuteStage::do_commit(SQLStageEvent *sql_event)
{
  RC rc = RC::SUCCESS;
  SessionEvent *session_event = sql_event->session_event();
  Session *session = session_event->session();
  Db *db = session->get_current_db();
  Trx *trx = session->current_trx();
  CLogManager *clog_manager = db->get_clog_manager();

  session->set_trx_multi_operation_mode(false);

  CLogRecord *clog_record = nullptr;
  rc = clog_manager->clog_gen_record(CLogType::REDO_MTR_COMMIT, trx->get_current_id(), clog_record);
  if (rc != RC::SUCCESS || clog_record == nullptr) {
    session_event->set_response("FAILURE\n");
    return rc;
  }

  rc = clog_manager->clog_append_record(clog_record);
  if (rc != RC::SUCCESS) {
    session_event->set_response("FAILURE\n");
  } else {
    session_event->set_response("SUCCESS\n");
  }

  trx->next_current_id();

  return rc;
}

RC ExecuteStage::do_clog_sync(SQLStageEvent *sql_event)
{
  RC rc = RC::SUCCESS;
  SessionEvent *session_event = sql_event->session_event();
  Db *db = session_event->session()->get_current_db();
  CLogManager *clog_manager = db->get_clog_manager();

  rc = clog_manager->clog_sync();
  if (rc != RC::SUCCESS) {
    session_event->set_response("FAILURE\n");
  } else {
    session_event->set_response("SUCCESS\n");
  }

  return rc;
}

RC ExecuteStage::do_drop_table(SQLStageEvent *sql_event) {
  const DropTable &drop_table = sql_event->query()->sstr.drop_table;
  SessionEvent *session_event = sql_event->session_event();
  Db *db = session_event->session()->get_current_db();
  RC rc = db->drop_table(drop_table.relation_name);
  if (rc == RC::SUCCESS) {
    session_event->set_response("SUCCESS\n");
  } else {
    session_event->set_response("FAILURE\n");
  }
  return rc;
}

RC ExecuteStage::transform_predicate_expression_to_date_if_nessasery(const std::vector<FilterUnit *> &filter_units) {
  for (const FilterUnit *filter_unit : filter_units) {
    Expression *left_expr = filter_unit->left();
    Expression *right_expr = filter_unit->right();
    if (left_expr->type() == ExprType::LIST && right_expr->type() == ExprType::FIELD) {
      std::swap(left_expr, right_expr);
    }

    if (left_expr->type() == ExprType::FIELD && right_expr->type() == ExprType::LIST && ((ValueListExpr *)right_expr)->length() == 1) {
      FieldExpr *field_expr = static_cast<FieldExpr*>(left_expr);
      Field &field = field_expr->field();
      if (field.attr_type() == DATES) {
        ValueListExpr *value_expr = static_cast<ValueListExpr*>(right_expr);
        TupleCell tuple_cell;
        value_expr->get_tuple_cell(tuple_cell);
        if (tuple_cell.is_null()) {
          continue;
        }
        uint32_t date = transform_date_to_int(tuple_cell.data());
        if (date == 0) {
          return RC::SCHEMA_FIELD_TYPE_MISMATCH;
        }
        value_expr->set_tuple_cell_type(DATES);
        memcpy(const_cast<char*>(tuple_cell.data()), &date, sizeof(date));
      }
    }
  }
  return RC::SUCCESS;
}

RC ExecuteStage::do_update(SQLStageEvent *sql_event) {
  RC rc = RC::SUCCESS;
  Stmt *stmt = sql_event->stmt();
  SessionEvent *session_event = sql_event->session_event();
  Session *session = session_event->session();
  Db *db = session->get_current_db();
  Trx *trx = session->current_trx();
  CLogManager *clog_manager = db->get_clog_manager();

  if (stmt == nullptr) {
    LOG_WARN("cannot find statement");
    return RC::GENERIC_ERROR;
  }

  UpdateStmt *update_stmt = static_cast<UpdateStmt*>(stmt);
  std::vector<TmpStmt *> stmt_pointers;
  std::vector<Operator *> op_pointers;
  std::vector<Table *> tables;
  std::vector<Table *> father_tables;
  for(auto unit : update_stmt->set_stmt()->filter_units()) {
    if(unit->right()->type() == QUERY) {
      QueryExpr &right_field_expr = *(QueryExpr *)unit->right();
      SelectStmt *select_stmt_sub = (SelectStmt *)(right_field_expr.get_stmt());
      Operator *sub_result;
      rc = create_sub_select(select_stmt_sub, tables, father_tables, stmt_pointers, op_pointers,sub_result);
      if(rc == RC::NOT_RELATIVE) {
        ValueListExpr *list = new ValueListExpr();
        rc = get_expr(select_stmt_sub, sub_result, list);
        if(rc != RC::SUCCESS && rc != RC::RECORD_EOF) {
          session_event->set_response("FAILURE\n");
          clear_mem(stmt_pointers, op_pointers);
          return rc;
        }
        if(list->length() > 1) {
          session_event->set_response("FAILURE\n");
          return RC::INVALID_ARGUMENT;
        }
        unit->set_right(list);
      }
    }
  }
  rc = transform_predicate_expression_to_date_if_nessasery(update_stmt->filter_stmt()->filter_units());
  if (rc != RC::SUCCESS) {
    session_event->set_response("FAILURE\n");
    clear_mem(stmt_pointers, op_pointers);
    return rc;
  }
  // TableScanOperator scan_oper(update_stmt->table());
  TmpStmt *s = new TmpStmt();
  stmt_pointers.push_back(s);
  PredicateOperator pred_oper(s);
  Operator *op = &pred_oper;
  std::vector<Table *> update_tables;
  update_tables.push_back(update_stmt->table());
  rc = get_operator(update_tables, tables, update_stmt->filter_stmt()->filter_units(), op, stmt_pointers, op_pointers);
  
  UpdateOperator update_oper(update_stmt, trx);
  update_oper.add_child(&pred_oper);

  rc = update_oper.open();
  if (rc != RC::SUCCESS) {
    session_event->set_response("FAILURE\n");
  } else {
    session_event->set_response("SUCCESS\n");
    if (!session->is_trx_multi_operation_mode()) {
      CLogRecord *clog_record = nullptr;
      rc = clog_manager->clog_gen_record(CLogType::REDO_MTR_COMMIT, trx->get_current_id(), clog_record);
      if (rc != RC::SUCCESS || clog_record == nullptr) {
        session_event->set_response("FAILURE\n");
        clear_mem(stmt_pointers, op_pointers);
        return rc;
      }

      rc = clog_manager->clog_append_record(clog_record);
      if (rc != RC::SUCCESS) {
        session_event->set_response("FAILURE\n");
        clear_mem(stmt_pointers, op_pointers);
        return rc;
      } 

      trx->next_current_id();
      session_event->set_response("SUCCESS\n");
    }
  }
  clear_mem(stmt_pointers, op_pointers);
  return rc;
}

void index_to_string(std::ostream &os, const Index &index, const Table *table) {
  const IndexMeta index_meta = index.index_meta();

  std::string table_name(table->name());
  for (std::size_t i = 0; i < table_name.size(); i++) {
    table_name[i] = toupper(table_name[i]);
  }
  std::string key_name(index_meta.name());
  for (std::size_t i = 0; i < key_name.size(); i++) {
    key_name[i] = toupper(key_name[i]);
  }
  for (int i = 0; i < index_meta.field_num(); i++) {
    os << table_name;
    os << " | ";
    if (index_meta.is_unique()) {
      os << "0";
    } else {
      os << "1";
    }
    os << " | ";
    os << key_name;
    os << " | ";
    os << i + 1 << " | ";
    std::string column_name(index_meta.field(i));
    for (std::size_t j = 0; j < column_name.size(); j++) {
      column_name[j] = toupper(column_name[j]);
    }
    os << column_name;
    os << '\n';
  }
}

void print_index_header(std::ostream &os) {
  os << "Table | Non_unique | Key_name | Seq_in_index | Column_name\n";
}

RC ExecuteStage::do_show_index(SQLStageEvent *sql_event) {
  Query *query = sql_event->query();
  Db *db = sql_event->session_event()->session()->get_current_db();
  const char *table_name = query->sstr.show_index.relation_name;
  Table *table = db->find_table(table_name);
  std::stringstream ss;
  if (nullptr == table) {
    ss << "FAILURE" << std::endl;
  } else {
    print_index_header(ss);
    for (int i = 0; i < table->index_num(); i++) {
      index_to_string(ss, *(table->find_index(i)), table);    
    }
  }
  sql_event->session_event()->set_response(ss.str().c_str());
  return RC::SUCCESS;
}
