/* Copyright (c) 2021 Xie Meiyi(xiemeiyi@hust.edu.cn) and OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by Longda on 2021/4/13.
//

#ifndef __OBSERVER_SQL_EXECUTE_STAGE_H__
#define __OBSERVER_SQL_EXECUTE_STAGE_H__

#include "common/seda/stage.h"
#include "sql/parser/parse.h"
#include "rc.h"
#include "vector"
#include "sql/expr/expression.h"

class SQLStageEvent;
class SessionEvent;
class SelectStmt;
class FilterStmt;
class Operator;
class TmpStmt;
class PredicateOperator;
class FilterUnit;
class Table;

class ExecuteStage : public common::Stage {
public:
  ~ExecuteStage();
  static Stage *make_stage(const std::string &tag);
  static RC get_expr(SelectStmt *select_stmt_sub, Operator *&sub_result, ValueListExpr *&expr);
protected:
  // common function
  ExecuteStage(const char *tag);
  bool set_properties() override;

  bool initialize() override;
  void cleanup() override;
  void handle_event(common::StageEvent *event) override;
  void callback_event(common::StageEvent *event, common::CallbackContext *context) override;

  void handle_request(common::StageEvent *event);
  RC do_help(SQLStageEvent *session_event);
  RC do_create_table(SQLStageEvent *sql_event);
  RC do_create_index(SQLStageEvent *sql_event);
  RC do_show_tables(SQLStageEvent *sql_event);
  RC do_desc_table(SQLStageEvent *sql_event);
  RC do_show_index(SQLStageEvent *sql_event);
  RC do_select(SQLStageEvent *sql_event);
  RC do_insert(SQLStageEvent *sql_event);
  RC do_delete(SQLStageEvent *sql_event);
  RC do_begin(SQLStageEvent *sql_event);
  RC do_commit(SQLStageEvent *sql_event);
  RC do_clog_sync(SQLStageEvent *sql_event);

  RC do_drop_table(SQLStageEvent *sql_event);
  RC do_update(SQLStageEvent *sql_event);
  RC transform_predicate_expression_to_date_if_nessasery(const std::vector<FilterUnit *> &filter_units);
  RC create_sub_select(SelectStmt *select_stmt_sub, const std::vector<Table *> &father_tables, const std::vector<Table *> &father_out_tables, std::vector<TmpStmt *> &stmt_pointers, std::vector<Operator *> &op_pointers, Operator *&sub_result);
  RC get_predictor_tree(const std::vector<Table *> &father_tables, const std::vector<Table *> &father_out_tables, const std::vector<FilterUnit *> &filter_units, std::map<std::string, Operator *> table_operator_mp, std::vector<TmpStmt *> &stmt_pointers, std::vector<Operator *> &op_pointers, Operator *&o);

  RC get_operator(const std::vector<Table *> &tables, const std::vector<Table *> &out_tables, const std::vector<FilterUnit *> &filter_units, Operator *&op, std::vector<TmpStmt *> &stmt_pointers, std::vector<Operator *> &op_pointers);
protected:
private:
  Stage *default_storage_stage_ = nullptr;
  Stage *mem_storage_stage_ = nullptr;
};

#endif  //__OBSERVER_SQL_EXECUTE_STAGE_H__
