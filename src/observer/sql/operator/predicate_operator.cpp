/* Copyright (c) 2021 Xie Meiyi(xiemeiyi@hust.edu.cn) and OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by WangYunlai on 2022/6/27.
//

#include "common/log/log.h"
#include "sql/operator/predicate_operator.h"
#include "storage/record/record.h"
#include "sql/stmt/filter_stmt.h"
#include "storage/common/field.h"
#include "sql/executor/execute_stage.h"


#define SIZE 256    //字符集字符数

void generateBadChar(char *b, int m, int *badchar)//(模式串字符b，模式串长度m，模式串的哈希表)
{
    int i, ascii;
    for(i = 0; i < SIZE; ++i)
    {
        badchar[i] = -1;//哈希表初始化为-1
    }
    for(i = 0; i < m; ++i)
    {
        if (b[i] == '_') {
            for (int z = 0; z < SIZE; ++z) {
                badchar[z] = i;
            }
        } else {
            ascii = int(b[i]);  //计算字符的ASCII值
            badchar[ascii] = i;//重复字符被覆盖，记录的是最后出现的该字符的位置
        }
    }
}

void generateGS(char *b, int m, int *suffix, int *prefix)//预处理模式串，填充suffix，prefix
{
    int i, j, k;
    for(i = 0; i < m; ++i)//两个数组初始化
    {
        suffix[i] = -1;
        prefix[i] = 0;
    }
    for(i = 0; i < m-1; ++i)//b[0,i]
    {
        j = i;
        k = 0;//公共后缀子串长度(模式串尾部取k个出来，分别比较)
        while(j >= 0 && (b[j] == b[m-1-k] || b[j] == '_' || b[m-1-k] == '_'))//与b[0,m-1]求公共后缀子串
        {
            --j;
            ++k;
            suffix[k] = j+1;
            //相同后缀子串长度为k时，该子串在b[0,i]中的起始下标
            // (如果有多个相同长度的子串，被赋值覆盖，存较大的)
        }
        if(j == -1)//查找到模式串的头部了
            prefix[k] = 1;//如果公共后缀子串也是模式串的前缀子串
    }
}

int moveByGS(int j, int m, int *suffix, int *prefix)//传入的j是坏字符对应的模式串中的字符下标
{
    int k = m - 1 - j;//好后缀长度
    if(suffix[k] != -1)//case1，找到跟好后缀一样的模式子串（多个的话，存的靠后的那个（子串起始下标））
        return j - suffix[k] + 1;
    for(int r = j + 2; r < m; ++r)//case2
    {
        if(prefix[m-r] == 1)//m-r是好后缀的子串的长度，如果这个好后缀的子串是模式串的前缀子串
            return r;//在上面没有找到相同的好后缀下，移动r位，对齐前缀到好后缀
    }
    return m;//case3,都没有匹配的，移动m位（模式串长度）
}

int str_bm(char *a, int n, char *b, int m)//a表示主串，长n; b表示模式串,长m
{
    int *badchar = (int*) malloc(SIZE * sizeof(int)); //记录模式串中每个字符最后出现的位置
    generateBadChar(b,m,badchar);     //构建坏字符哈希表
    int *suffix = (int*) malloc(m * sizeof(int));
    int *prefix = (int*) malloc(m * sizeof(int));
    generateGS(b, m, suffix, prefix);   //预处理模式串，填充suffix，prefix
    int i = 0, j, moveLen1, moveLen2;//j表示主串与模式串匹配的第一个字符
    while(i < n-m+1)
    {
        for(j = m -1; j >= 0; --j)  //模式串从后往前匹配
        {
            if(a[i+j] != '_' && b[j] != '_' && a[i+j] != b[j])
                break;  //坏字符对应模式串中的下标是j
        }
        if(j < 0)   //匹配成功
        {
            free(badchar);
            free(suffix);
            free(prefix);
            return i;   //返回主串与模式串第一个匹配的字符的位置
        }
        //这里等同于将模式串往后滑动 j-badchar[int(a[i+j])] 位
        moveLen1 = j - badchar[int(a[i+j])];//按照坏字符规则移动距离
        moveLen2 = 0;
        if(j < m-1)//如果有好后缀的话
        {
            moveLen2 = moveByGS(j, m, suffix, prefix);//按照好后缀规则移动距离
        }
        i = i + std::max(moveLen1, moveLen2);//取大的移动
    }
    free(badchar);
    free(suffix);
    free(prefix);
    return -1;
}

int helper(char *a, int b, char *c, int d) {
    if (d == 0) {
        return 0;
    }

    int first = c[0] == '%' ? 1 : 0;
    int last = c[d - 1] == '%' ? 1 : 0;

    char *tokens[d + 1];
    int tokens_len = 0;
    tokens[tokens_len++] = strtok(c, "%");
    while(tokens[tokens_len - 1] != NULL) {
        tokens[tokens_len++] = strtok(NULL, "%");
    }
    tokens_len--;

    int prev = 0, i = 0;
    for (; i < tokens_len; ++i) {
        int idx = str_bm(a + prev, b - prev, tokens[i], strlen(tokens[i]));
        if (idx < 0) {
            return 0;
        }
        if (i == 0 && !first && idx > 0) {
            return 0;
        }
        prev += idx + strlen(tokens[i]);
        if (i == tokens_len - 1 && !last && prev != b) {
            if (str_bm(a + prev, b - prev, tokens[i], strlen(tokens[i])) == -1 || (first == 0 && tokens_len == 1)) return 0;
            i--;
        }
    }
    return 1;
}

bool base_do_predicate(std::vector<FilterUnit *> filter_units_, RowTuple &tuple, bool is_sub_query, Tuple *out_tuple, std::map<Expression *, ValueListExpr *> query_2_value) {
  bool is_and = true;
  for(auto unit : filter_units_) {
    if(!unit->is_and()) {
      is_and = false;
    }
  }
  for (const FilterUnit *filter_unit : filter_units_) {
    CompOp comp = filter_unit->comp();
    if(comp == EXIST_IN) {
      TupleCell right_cell;
      Expression *right_expr = query_2_value[filter_unit->right()];
      if(is_sub_query == true && right_expr->is_out()) {
        right_expr->get_value(*out_tuple, right_cell);
      } else {
        right_expr->get_value(tuple, right_cell);
      }
      if(right_cell.attr_type() == NULLS || right_cell.is_null()) {
        if(is_and) {
          return false;
        } else {
          continue;
        }
      } else {
        if(!is_and) {
          return true;
        } else {
          continue;
        }
      }
    } else if(comp == NOT_EXIST) {
      TupleCell right_cell;
      Expression *right_expr = query_2_value[filter_unit->right()];
      if(is_sub_query == true && right_expr->is_out()) {
        right_expr->get_value(*out_tuple, right_cell);
      } else {
        right_expr->get_value(tuple, right_cell);
      }
      if(right_cell.attr_type() == NULLS || right_cell.is_null()) {
        if(!is_and) {
          return true;
        } else {
          continue;
        }
      } else {
        if(is_and) {
          return false;
        } else {
          continue;
        }
      }
    }
    Expression *left_expr = filter_unit->left();
    if(left_expr->type() == QUERY) {
      left_expr = query_2_value[left_expr];
    }
    Expression *right_expr = filter_unit->right();
    if(right_expr->type() == QUERY) {
      right_expr = query_2_value[right_expr];
    }
    
    TupleCell left_cell;
    TupleCell right_cell;
    if(is_sub_query == true && left_expr->is_out()) {
      left_expr->get_value(*out_tuple, left_cell);
    } else {
      left_expr->get_value(tuple, left_cell);
    }
    
    // null需要特殊判断
    bool left_is_null = false;
    if (left_expr->type() == ExprType::FIELD) {
      left_is_null = left_cell.is_null();
    } else if (left_expr->type() == ExprType::LIST) {
      left_is_null = left_cell.attr_type() == NULLS;
    }
    
    if(comp == CompOp::IS_IN) {
      if(left_is_null) {
        if(is_and) {
          return false;
        } else {
          continue;
        }
      }
      ValueListExpr *value_list = static_cast<ValueListExpr *>(right_expr);
      bool is_in = false;
      for(int i = 0; i < value_list->length(); i++) {
        if(value_list->get_i_value(i).is_null()) {
          continue;
        }
        if(left_cell.compare(value_list->get_i_value(i)) == 0) {
          is_in = true;
          if(!is_and) {
            return true;
          }
          break;
        }
      }
      if(is_in == false) {
        if(is_and) {
          return false;
        }
      }
    } else if(comp == CompOp::NOT_IN) {
      if(left_is_null) {
        if(is_and) {
          return false;
        } else {
          continue;
        }
      }
      ValueListExpr *value_list = static_cast<ValueListExpr *>(right_expr);
      for(int i = 0; i < value_list->length(); i++) {
        if(value_list->get_i_value(i).is_null()) {
          if(is_and) {
            return false;
          }
          
        }
        if(left_cell.compare(value_list->get_i_value(i)) == 0) {
          if(is_and){
            return false;
          }
        }
      }
      if(!is_and) {
        return true;
      }
    } else if (comp == CompOp::LIKE_OP || comp == CompOp::NOT_LIKE_OP) {
      if(is_sub_query == true && right_expr->is_out()) {
        right_expr->get_value(*out_tuple, right_cell);
      } else {
        right_expr->get_value(tuple, right_cell);
      }
      size_t left_len = strlen(left_cell.data());
      size_t right_len = strlen(right_cell.data());
      char left_str[left_len + 1];
      memcpy(left_str, left_cell.data(), left_len + 1);
      char right_str[right_len + 1];
      memcpy(right_str, right_cell.data(), right_len + 1);
      const bool is_equal = helper(left_str, left_len, right_str, right_len) == 1;
      bool filter_result = false;
      filter_result = (is_equal && comp == CompOp::LIKE_OP) || (!is_equal && comp == CompOp::NOT_LIKE_OP);
      if (!filter_result) {
        if(is_and) {
          return false;
        }
      } else {
        if(!is_and) {
          return true;
        }
      }
    } else {
      if(is_sub_query == true && right_expr->is_out()) {
        right_expr->get_value(*out_tuple, right_cell);
      } else {
        right_expr->get_value(tuple, right_cell);
      }
      bool right_is_null = false;
      
      if (right_expr->type() == ExprType::FIELD) {
        right_is_null = right_cell.is_null();
      } else if (right_expr->type() == ExprType::LIST) {
        right_is_null = right_cell.attr_type() == NULLS;
      }
      if (left_is_null || right_is_null) {
        if (comp == IS_NOT) return !left_is_null && right_is_null; 
      if (comp == IS_NOT) return !left_is_null && right_is_null; 
        if (comp == IS_NOT) return !left_is_null && right_is_null; 
        if (comp == IS) return left_is_null && right_is_null;
        if(is_and) {
          return false;
        } else {
          continue;
        }
      }

      const int compare = left_cell.compare(right_cell);
      bool filter_result = false;
      switch (comp) {
      case EQUAL_TO: {
        filter_result = (0 == compare); 
      filter_result = (0 == compare); 
        filter_result = (0 == compare); 
      } break;
      case LESS_EQUAL: {
        filter_result = (compare <= 0); 
      filter_result = (compare <= 0); 
        filter_result = (compare <= 0); 
      } break;
      case NOT_EQUAL: {
        filter_result = (compare != 0);
      } break;
      case LESS_THAN: {
        filter_result = (compare < 0);
      } break;
      case GREAT_EQUAL: {
        filter_result = (compare >= 0);
      } break;
      case GREAT_THAN: {
        filter_result = (compare > 0);
      } break;
      default: {
        LOG_WARN("invalid compare type: %d", comp);
      } break;
      }
      if (!filter_result) {
        if(is_and) {
          return false;
        }
      } else {
        if(!is_and) {
          return true;
        }
      }
    }
  }
  if(is_and) {
    return true;
  } else {
    return false;
  }
  
}

Tuple *SubOperator::current_tuple() {
  return children_[0]->current_tuple();
}

RC SubOperator::close() {
  children_[0]->close();
  return RC::SUCCESS;
}
RC SubOperator::open() {
  if(children_.size() != 1) {
    LOG_WARN("subOperator predicate operator must has one child");
    return RC::INTERNAL;
  }
  children_[0]->open();
  return RC::SUCCESS;
}

RC SubOperator::next() {
  RC rc = RC::SUCCESS;
  Operator *op = children_[0];
  while(RC::SUCCESS == (rc = op->next())) {
    Tuple *sub_tuple = op->current_tuple();
    if (nullptr == sub_tuple) {
      rc = RC::INTERNAL;
      LOG_WARN("failed to get tuple from operator");
      break;
    }
    std::map<Expression *, ValueListExpr *> query_2_value;
    if(base_do_predicate(filter_units_, static_cast<RowTuple &>(*sub_tuple), true, tuple_, query_2_value)) {
      return RC::SUCCESS;
    }
  }
  return rc;
}
void SubOperator::add_out_tuple(Tuple *tuple) {
  tuple_ = tuple;
  children_[0]->add_out_tuple(tuple);
}

RC SubPredicateOperator::open()
{
  RC r = RC::SUCCESS;
  if(children_.size() < 1) {
    LOG_WARN("sub predicate operator must has some child");
    return RC::INTERNAL;
  }
  for(auto child : children_) {
    r = child->open();
    if(r != RC::SUCCESS) {
      return r;
    }
  }
  return RC::SUCCESS;
}
RC SubPredicateOperator::next() {
  RC rc = RC::SUCCESS;
  Operator *out_op = children_[children_.size() - 1];
  std::map<Expression *, ValueListExpr *> query_2_value;
  while(RC::SUCCESS == (rc = out_op->next())) {
    Tuple *out_tuple = out_op->current_tuple();
    if(is_out_ == true) {
      com_tuple_.set_tuples(static_cast<RowTuple &>(*real_out_tuple_), static_cast<RowTuple &>(*out_tuple));
      out_tuple = &com_tuple_;
    }
    if (nullptr == out_tuple) {
      rc = RC::INTERNAL;
      LOG_WARN("failed to get tuple from operator");
      break;
    }
    for(u_int i = 0; i < filter_units_.size(); i++) {
      FilterUnit *unit = filter_units_[i];
      if(expr_2_stmt.find(unit->left()) != expr_2_stmt.end()) {
        SubOperator *sub_op = (SubOperator *)expr_2_stmt[unit->left()];
        sub_op->add_out_tuple(out_tuple);
        QueryExpr &left_query = *(QueryExpr *)unit->left();
        SelectStmt *select_stmt_sub = (SelectStmt *)(left_query.get_stmt());
        Operator *tmp_op = sub_op;
        ValueListExpr *value_left = new ValueListExpr();
        rc = ExecuteStage::get_expr(select_stmt_sub, tmp_op, value_left);
        query_2_value[unit->left()] = value_left;
        if(rc != RC::SUCCESS && rc != RC::RECORD_EOF) {
          for(auto kv : query_2_value) {
            delete kv.second;
          }
          return RC::INVALID_ARGUMENT;
        }
        if(value_left->length() > 1) {
          for(auto kv : query_2_value) {
            delete kv.second;
          }
          query_2_value.clear();
          return RC::INVALID_ARGUMENT;
        }
        if(value_left->length() == 0) {
          value_left->set_tuple_cell_type(NULLS);
        }
      }
      if(expr_2_stmt.find(unit->right()) != expr_2_stmt.end()) {
        SubOperator *sub_op = (SubOperator *)expr_2_stmt[unit->right()];
        sub_op->add_out_tuple(out_tuple);
        QueryExpr &right_query = *(QueryExpr *)unit->right();
        SelectStmt *select_stmt_sub = (SelectStmt *)(right_query.get_stmt());
        Operator *tmp_op = sub_op;
        ValueListExpr *value_right = new ValueListExpr();
        rc = ExecuteStage::get_expr(select_stmt_sub, tmp_op, value_right);
        query_2_value[unit->right()] = value_right;
        if(rc != RC::SUCCESS && rc != RC::RECORD_EOF) {
          for(auto kv : query_2_value) {
            delete kv.second;
          }
          query_2_value.clear();
          return rc;
        }
        if(value_right->length() > 1 && unit->comp() >= EQUAL_TO && unit->comp() <= GREAT_THAN) {
          for(auto kv : query_2_value) {
            delete kv.second;
          }
          query_2_value.clear();
          return RC::INVALID_ARGUMENT;
        }
        if(value_right->length() == 0) {
          value_right->set_tuple_cell_type(NULLS);
        }
      }
    }
    if(base_do_predicate(filter_units_, static_cast<RowTuple &>(*out_tuple), false, nullptr, query_2_value)) {
      for(auto kv : query_2_value) {
        delete kv.second;
      }
      query_2_value.clear();
      return RC::SUCCESS;
    } else {
      for(auto kv : query_2_value) {
        delete kv.second;
      }
      query_2_value.clear();
    }
  }
  for(auto kv : query_2_value) {
    delete kv.second;
  }
  query_2_value.clear();
  return rc;
}

Tuple * SubPredicateOperator::current_tuple()
{
  return children_[children_.size() - 1]->current_tuple();
}
RC SubPredicateOperator::close()
{
  for(auto child : children_) {
    child->close();
  }
  return RC::SUCCESS;
}
RC PredicateOperator::open()
{
  // if (children_.size() != 1) {
  //   LOG_WARN("predicate operator must has one child");
  //   return RC::INTERNAL;
  // }
  for(auto child :children_) {
    RC r = child->open();
    if(r != RC::SUCCESS) {
      return r;
    }
  }
  if(children_.size() == 2) {
    if(RC::RECORD_EOF == children_[0]->next()) {
      is_empty_ = true;
    }
  }
  return RC::SUCCESS;
}

void PredicateOperator::add_unit(FilterUnit *unit) {
  if(tmp_stmt_) {
    tmp_stmt_->filter_units.push_back(unit);
  }else {
    filter_stmt_->add_unit(unit);
  }
}

RC PredicateOperator::next()
{
  RC rc = RC::SUCCESS;
  if(children_.size() == 1) {
    Operator *oper = children_[0];
    
    while (RC::SUCCESS == (rc = oper->next())) {
      Tuple *tuple = oper->current_tuple();
      if (nullptr == tuple) {
        rc = RC::INTERNAL;
        LOG_WARN("failed to get tuple from operator");
        break;
      }

      if (do_predicate(static_cast<RowTuple &>(*tuple))) {
        return rc;
      }
    }
    return rc;
  } else if(children_.size() == 2) {
    if(is_empty_) {
      return RC::RECORD_EOF;
    }
    Operator *oper1 = children_[0];
    Operator *oper2 = children_[1];
    while(true) {
      if(RC::SUCCESS == (rc = oper2->next())) {
        Tuple *tuple2 = oper2->current_tuple();
        Tuple *tuple1 = oper1->current_tuple();
        if (nullptr == tuple1 || nullptr == tuple2) {
          rc = RC::INTERNAL;
          LOG_WARN("failed to get tuple from operator");
          break;
        }
        com_tuple_.set_tuples(static_cast<RowTuple &>(*tuple1), static_cast<RowTuple &>(*tuple2));
        if (do_predicate(com_tuple_)) {
          return rc;
        }
      } else {
        if(RC::SUCCESS == (rc = oper1->next())) {
          oper2->open();
        } else {
          break;
        }
      }
    }
    return rc;
  }
  return RC::GENERIC_ERROR;
}

RC PredicateOperator::close()
{
  children_[0]->close();
  if(children_.size() == 2) {
    children_[1]->close();
  }
  return RC::SUCCESS;
}

Tuple * PredicateOperator::current_tuple()
{
  if(children_.size() == 1) {
    return children_[0]->current_tuple();
  } else if(children_.size() == 2) {
    Tuple *tuple1 = children_[0]->current_tuple();
    Tuple *tuple2 = children_[1]->current_tuple();
    com_tuple_.set_tuples(static_cast<RowTuple &>(*tuple1), static_cast<RowTuple &>(*tuple2));
    return &com_tuple_;
  }
  return nullptr;
}

bool PredicateOperator::do_predicate(RowTuple &tuple)
{
  if ((tmp_stmt_ == nullptr || tmp_stmt_->filter_units.empty()) &&(filter_stmt_ == nullptr || filter_stmt_->filter_units().empty())) {
    return true;
  }
  std::vector<FilterUnit*> filter_units_;
  if(tmp_stmt_) {
    for(auto unit: tmp_stmt_->filter_units) {
      filter_units_.push_back(unit);
    }
  } else {
    for(auto unit: filter_stmt_->filter_units()) {
      filter_units_.push_back(unit);
    }
  }
  std::map<Expression *, ValueListExpr *> query_2_value;
  return base_do_predicate(filter_units_, tuple, false, nullptr, query_2_value);
}
