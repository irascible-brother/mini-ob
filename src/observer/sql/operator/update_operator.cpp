/* Copyright (c) 2021 Xie Meiyi(xiemeiyi@hust.edu.cn) and OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by WangYunlai on 2022/6/27.
//

#include "common/log/log.h"
#include "sql/operator/update_operator.h"
#include "storage/record/record.h"
#include "storage/common/table.h"
#include "storage/trx/trx.h"
#include "sql/stmt/update_stmt.h"
#include <cmath>


// add by tong
int update_get_int_from_char(char in[]){
   int i = 0;
   int sum = 0;
   while ((in[i] != '\0') && (in[i] >= '0') && (in[i] <= '9')) {
      sum = sum * 10 + (in[i] - '0');
      i += 1;
   }
   return sum;
}
float update_get_float_from_char(char in[]){
   int i = 0;
   int flag = 0;
   float sum = 0.0;
   float after = 0.0;
   float after_b = 1;
   while ((in[i] >= '0') && (in[i] <= '9')){
      sum = sum * 10.0 + (in[i] - '0');
      i += 1; 
   }
   if (!((in[i] >= '0') && (in[i] <= '9')) && !(in[i] == '.')) {
      return (sum+after);
   } else if ((in[i] == '.') && (flag == 0)) {
      i += 1;
      flag = 1;
      while ((in[i] >= '0') && (in[i] <= '9')){
         after = after + 0.1 * (in[i] - '0') * after_b;
         after_b = 0.1 * after_b;
         i += 1;
      }
      return (sum+after);
   }
   return (sum+after);
}
// end
RC UpdateOperator::open()
{
  if (children_.size() != 1) {
    LOG_WARN("delete operator must has 1 child");
    return RC::INTERNAL;
  }


  Table *table = update_stmt_->table();
  const TableMeta table_meta = table->table_meta();
  std::vector<const char *> fild_names;
  std::vector<Value> values;
  for(auto unit : update_stmt_->set_stmt()->filter_units()) {
    FieldExpr &field = *(FieldExpr *)unit->left();
    ValueListExpr &value = *(ValueListExpr *)unit->right();
    fild_names.push_back(field.field_name());
    values.push_back(value.get_zero_value());
  }

  // const Value value = update_stmt_->values()[0];
  for(size_t i = 0; i < values.size(); i++) {
    FieldExpr &field = *(FieldExpr *)update_stmt_->set_stmt()->filter_units()[i]->left();
    const FieldMeta *field_meta = field.field().meta();
    if (nullptr == field_meta) {
      return RC::SCHEMA_FIELD_NOT_EXIST;
    }
    // Value value = values[i];
    if (field_meta->type() == DATES) {
      uint32_t date = transform_date_to_int(std::move(std::string(static_cast<char *>(values[i].data))));
      if (date == 0) {
        return RC::SCHEMA_FIELD_TYPE_MISMATCH;
      }
      AttrType type = DATES;
      memcpy((void *)&values[i].type, &type, sizeof(AttrType));
      memcpy((void *)values[i].data, &date, sizeof(date));
    } else if (field_meta->type() == TEXTS) {
      AttrType type = TEXTS;
      memcpy((void *)&values[i].type, &type, sizeof(AttrType));
    }
    if(values[i].type == NULLS && !field_meta->nullable()) {
      return RC::CONSTRAINT_NOTNULL;
    }
    if (field_meta->type() != values[i].type && values[i].type != NULLS) {
      const AttrType field_type = field_meta->type(); // 创建表时规定的类型
      const AttrType value_type = values[i].type; // 真实insert时输入的类型
      if (!(field_type == value_type || (field_type == DATES && value_type == CHARS))) { // TODO try to convert the value type to field type
        // type convertion tong
        if (field_type == INTS && value_type == CHARS){ // insert into t values('1','b','12');
          memcpy((void*)(&(values[i].type)), &field_type, sizeof(AttrType));
          int tmp2 = update_get_int_from_char((char*)(values[i].data));
          memcpy(values[i].data, &tmp2, field_meta->len());
        } else if (field_type == INTS && value_type == FLOATS) {
          memcpy((void*)(&(values[i].type)), &field_type, sizeof(AttrType));
          float* tmp = (float*)(values[i].data); //取多大
          int tmp2 = round(*tmp);
          memcpy(values[i].data, &tmp2, field_meta->len());
        } else if (field_type == FLOATS && value_type == INTS){ // insert into t values(1,'b','12');进入这里很不合理
          memcpy((void*)(&(values[i].type)), &field_type, sizeof(AttrType));
          int* tmp = (int*)(values[i].data); //取多大
          float tmp2 = (*tmp);
          memcpy(values[i].data, &tmp2, field_meta->len());
        } else if (field_type == FLOATS && value_type == CHARS){ // insert into t values(1,'b','12');
          memcpy((void*)(&(values[i].type)), &field_type, sizeof(AttrType));
          float tmp2 = update_get_float_from_char((char*)(values[i].data));
          memcpy(values[i].data, &tmp2, field_meta->len());
        } else if (field_type == CHARS && value_type == INTS) {
          memcpy((void*)(&(values[i].type)), &field_type, sizeof(AttrType));
          int *tmp = (int *)values[i].data;
          std::ostringstream str_stream;
          str_stream << *tmp;
          std::string tmp2 = str_stream.str();
          tmp2.push_back('\0');
          tmp2.resize(4);
          memcpy(values[i].data, tmp2.c_str(), tmp2.size());
        } else if (field_type == CHARS && value_type == FLOATS) {
          memcpy((void*)(&(values[i].type)), &field_type, sizeof(AttrType));
          float *tmp = (float *)values[i].data;
          std::ostringstream str_stream;
          str_stream << *tmp;
          std::string tmp2 = str_stream.str();
          tmp2.push_back('\0');
          tmp2.resize(4);
          memcpy(values[i].data, tmp2.c_str(), tmp2.size());
        } else if (field_type == TEXTS && value_type == CHARS) {
          memcpy((void*)(&(values[i].type)), &field_type, sizeof(AttrType));
        } else if (value_type == NULLS) {
          if (!field_meta->nullable()) {
            return RC::CONSTRAINT_NOTNULL;
          }
        } else {
          return RC::SCHEMA_FIELD_TYPE_MISMATCH;
        }
        // end
      } 
    }
  }

  Operator *child = children_[0];
  RC rc = child->open();
  if (rc != RC::SUCCESS) {
    LOG_WARN("failed to open child operator: %s", strrc(rc));
    return rc;
  }

  while (RC::SUCCESS == (rc = child->next())) {
    Tuple *tuple = child->current_tuple();
    if (nullptr == tuple) {
      LOG_WARN("failed to get current record: %s", strrc(rc));
      return rc;
    }

    rc = table->update_record(trx_, fild_names, values, tuple, table);
    if (rc != RC::SUCCESS) {
      LOG_WARN("failed to update record: %s", strrc(rc));
      return rc;
    }
  }
  return RC::SUCCESS;
}

RC UpdateOperator::next()
{
  return RC::RECORD_EOF;
}

RC UpdateOperator::close()
{
  children_[0]->close();
  return RC::SUCCESS;
}
