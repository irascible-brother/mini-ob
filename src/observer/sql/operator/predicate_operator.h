/* Copyright (c) 2021 Xie Meiyi(xiemeiyi@hust.edu.cn) and OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by WangYunlai on 2022/6/27.
//

#pragma once

#include "sql/operator/operator.h"

class FilterStmt;
class FilterUnit;
class TmpStmt;

/**
 * PredicateOperator 用于单个表中的记录过滤
 * 如果是多个表数据过滤，比如join条件的过滤，需要设计新的predicate或者扩展:w
 */
class PredicateOperator : public Operator
{
public:
  PredicateOperator(FilterStmt *filter_stmt)
    : filter_stmt_(filter_stmt)
  {}
  
  PredicateOperator(TmpStmt *filter_stmt)
    : tmp_stmt_(filter_stmt)
  {}

  virtual ~PredicateOperator() = default;

  RC open() override;
  RC next() override;
  RC close() override;
  void add_unit(FilterUnit *unit);

  Tuple * current_tuple() override;
  OperatorType get_type() override {
    return PREDICATE;
  }
  void add_out_tuple(Tuple *tuple) {
    for(auto op : children_) {
      op->add_out_tuple(tuple);
    }
    return;
  }
  //int tuple_cell_num() const override;
  //RC tuple_cell_spec_at(int index, TupleCellSpec &spec) const override;
private:
  bool do_predicate(RowTuple &tuple);
private:
  FilterStmt *filter_stmt_ = nullptr;
  TmpStmt *tmp_stmt_ = nullptr;
  // std::vector<FilterUnit *>  filter_units_;
  bool is_empty_ = false;//only two tables
  CompositeRowTuple com_tuple_;
};

class SubOperator : public Operator {
public:
  SubOperator() = default;
  virtual ~SubOperator() = default;
  RC open() override;
  RC next() override;
  RC close() override;
  Tuple * current_tuple() override;
  // RC add_out(Tuple *tuple);
  void add_units(std::vector<FilterUnit *> units) {
    for(auto unit : units) {
      filter_units_.push_back(unit);
    }
  }
  OperatorType get_type() override {
    return SUBOPERATOR;
  }
  void add_out_tuple(Tuple *tuple) override;
private:
  Tuple *tuple_;  
  std::vector<FilterUnit *> filter_units_;
};

class SubPredicateOperator : public Operator {
public:
  SubPredicateOperator() = default;

  virtual ~SubPredicateOperator() = default;

  RC open() override;
  RC next() override;
  RC close() override;
  void add_units(std::vector<FilterUnit *> units) {
    for(auto unit : units) {
      filter_units_.push_back(unit);
    }
  }
  void add_unit(FilterUnit * unit) {
    filter_units_.push_back(unit);
  }
  Tuple * current_tuple() override;
  int children_num() {
    return children_.size();
  }
  std::map<Expression *, Operator *> expr_2_stmt;
  OperatorType get_type() override {
    return SUBPREDICATE;
  }
  void add_out_tuple(Tuple *tuple) override {
    is_out_ = true;
    real_out_tuple_ = tuple;
    return;
  }
private:
  std::vector<FilterUnit *> filter_units_;
  Tuple *real_out_tuple_;
  CompositeRowTuple com_tuple_;
  bool is_out_ = false;
};