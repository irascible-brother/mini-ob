/* Copyright (c) 2021 Xie Meiyi(xiemeiyi@hust.edu.cn) and OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by Meiyi & Wangyunlai.wyl on 2021/5/18.
//

#include "storage/common/index_meta.h"
#include "storage/common/field_meta.h"
#include "storage/common/table_meta.h"
#include "common/lang/string.h"
#include "common/log/log.h"
#include "rc.h"
#include "json/json.h"

const static Json::StaticString FIELD_NAME("name");
const static Json::StaticString FIELD_FIELDS("fields");
const static Json::StaticString FIELD_IS_UNIQUE("is_unique");

RC IndexMeta::init(const char *name, const std::vector<FieldMeta> &fields, bool is_unique)
{
  if (common::is_blank(name)) {
    LOG_ERROR("Failed to init index, name is empty.");
    return RC::INVALID_ARGUMENT;
  }

  name_ = name;
  fields_ = fields;
  is_unique_ = is_unique;
  return RC::SUCCESS;
}

void IndexMeta::to_json(Json::Value &json_value) const
{
  json_value[FIELD_NAME] = name_;
  for (std::size_t i = 0; i < fields_.size(); i++) {
    json_value[FIELD_FIELDS].append(fields_[i].name());
  }
  json_value[FIELD_IS_UNIQUE] = is_unique_;
}

RC IndexMeta::from_json(const TableMeta &table, const Json::Value &json_value, IndexMeta &index)
{
  const Json::Value &name_value = json_value[FIELD_NAME];
  const Json::Value &fields_value = json_value[FIELD_FIELDS];
  const Json::Value &is_unique_value = json_value[FIELD_IS_UNIQUE];
  if (!name_value.isString()) {
    LOG_ERROR("Index name is not a string. json value=%s", name_value.toStyledString().c_str());
    return RC::GENERIC_ERROR;
  }
  if (!fields_value.isArray()) {
    LOG_ERROR("Field name of index [%s] is not a string. json value=%s",
        name_value.asCString(),
        fields_value.toStyledString().c_str());
    return RC::GENERIC_ERROR;
  }
  if (!is_unique_value.isBool()) {
    return RC::GENERIC_ERROR;
  }
  std::vector<FieldMeta> fields(fields_value.size());
  for (int i = 0; i < (int) fields.size(); i++) {
    const Json::Value field_value = fields_value[i];
    const FieldMeta *field = table.field(field_value.asCString());
    if (nullptr == field) {
      LOG_ERROR("Deserialize index [%s]: no such field: %s", name_value.asCString(), field_value.asCString());
      return RC::SCHEMA_FIELD_MISSING;
    }
    fields[i] = *field;
  }

  return index.init(name_value.asCString(), fields, is_unique_value.asBool());
}

const char *IndexMeta::name() const
{
  return name_.c_str();
}

const char *IndexMeta::field(int i) const {
  return fields_[i].name();
}

void IndexMeta::desc(std::ostream &os) const
{
  // os << "index name=" << name_ << ", field=" << field_;
}

bool IndexMeta::index_for(const char *field_name) const {
  for (std::size_t i = 0; i < fields_.size(); i++) {
    if (!strcmp(field_name, fields_[i].name())) {
      return true;
    }
  }
  return false;
}