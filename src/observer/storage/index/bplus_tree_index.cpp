/* Copyright (c) 2021 Xie Meiyi(xiemeiyi@hust.edu.cn) and OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by Meiyi & wangyunlai.wyl on 2021/5/19.
//

#include "storage/index/bplus_tree_index.h"
#include "common/log/log.h"

BplusTreeIndex::~BplusTreeIndex() noexcept
{
  close();
}

RC BplusTreeIndex::create(const char *file_name, const IndexMeta &index_meta, const std::vector<FieldMeta> &fields_meta)
{
  if (inited_) {
    LOG_WARN("Failed to create index due to the index has been created before. file_name:%s, index:%s",
        file_name,
        index_meta.name());
    return RC::RECORD_OPENNED;
  }

  Index::init(index_meta, fields_meta);

  RC rc = index_handler_.create(file_name, fields_meta);
  if (RC::SUCCESS != rc) {
    LOG_WARN("Failed to create index_handler, file_name:%s, index:%s, rc:%s",
        file_name,
        index_meta.name(),
        strrc(rc));
    return rc;
  }

  inited_ = true;
  LOG_INFO(
      "Successfully create index, file_name:%s, index:%s", file_name, index_meta.name());
  return RC::SUCCESS;
}

RC BplusTreeIndex::open(const char *file_name, const IndexMeta &index_meta, const std::vector<FieldMeta> &fields_meta)
{
  if (inited_) {
    LOG_WARN("Failed to open index due to the index has been initedd before. file_name:%s, index:%s",
        file_name,
        index_meta.name());
    return RC::RECORD_OPENNED;
  }

  Index::init(index_meta, fields_meta);

  RC rc = index_handler_.open(file_name);
  if (RC::SUCCESS != rc) {
    LOG_WARN("Failed to open index_handler, file_name:%s, index:%s, rc:%s",
        file_name,
        index_meta.name(),
        strrc(rc));
    return rc;
  }

  inited_ = true;
  LOG_INFO(
      "Successfully open index, file_name:%s, index:%s", file_name, index_meta.name());
  return RC::SUCCESS;
}

RC BplusTreeIndex::close()
{
  if (inited_) {
    LOG_INFO("Begin to close index, index:%s",
        index_meta_.name());
    index_handler_.close();
    inited_ = false;
  }
  LOG_INFO("Successfully close index.");
  return RC::SUCCESS;
}

RC BplusTreeIndex::insert_entry(const char *record, const RID *rid)
{
  char key[index_handler_.key_len()];
  LOG_WARN("key_len=%d", index_handler_.key_len());
  int offset = 0;
  for (std::size_t i = 0; i < fields_meta_.size(); i++) {
    LOG_WARN("offset=%d", fields_meta_[i].offset());
    memcpy(key + offset, record + fields_meta_[i].offset(), fields_meta_[i].len());
    offset += fields_meta_[i].len();
  }
  return index_handler_.insert_entry(key, rid);
}

RC BplusTreeIndex::delete_entry(const char *record, const RID *rid)
{
  char key[index_handler_.key_len()];
  int offset = 0;
  for (std::size_t i = 0; i < fields_meta_.size(); i++) {
    memcpy(key + offset, record + fields_meta_[i].offset(), fields_meta_[i].len());
    offset += fields_meta_[i].len();
  }
  return index_handler_.delete_entry(key, rid);
}


RC BplusTreeIndex::get_entry(const char *record, std::list<RID> &rids) {
  for (std::size_t i = 0; i < fields_meta_.size(); i++) {
    uint32_t meta = *(uint32_t *)(record + fields_meta_[i].offset() - 4);
    if (meta == 1) {
      return RC::EMPTY;
    }
  }
  char key[index_handler_.key_len()];
  int offset = 0;
  for (std::size_t i = 0; i < fields_meta_.size(); i++) {
    memcpy(key + offset, record + fields_meta_[i].offset(), fields_meta_[i].len());
    offset += fields_meta_[i].len();
  }
  return index_handler_.get_entry(key, rids);
}

IndexScanner *BplusTreeIndex::create_scanner(char *left_key, char *right_key)
{
  BplusTreeIndexScanner *index_scanner = new BplusTreeIndexScanner(index_handler_);
  RC rc = index_scanner->open(left_key, right_key);
  if (rc != RC::SUCCESS) {
    LOG_WARN("failed to open index scanner. rc=%d:%s", rc, strrc(rc));
    delete index_scanner;
    return nullptr;
  }
  return index_scanner;
}

RC BplusTreeIndex::sync()
{
  return index_handler_.sync();
}

////////////////////////////////////////////////////////////////////////////////
BplusTreeIndexScanner::BplusTreeIndexScanner(BplusTreeHandler &tree_handler) : tree_scanner_(tree_handler)
{}

BplusTreeIndexScanner::~BplusTreeIndexScanner() noexcept
{
  tree_scanner_.close();
}

RC BplusTreeIndexScanner::open(char *left_key, char *right_key)
{
  return tree_scanner_.open(left_key, right_key);
}

RC BplusTreeIndexScanner::next_entry(RID *rid)
{
  return tree_scanner_.next_entry(rid);
}

RC BplusTreeIndexScanner::destroy()
{
  delete this;
  return RC::SUCCESS;
}
