if [ -d "miniob" ];then
  rm -rf miniob
fi &&
for f in `ls`;do
    if [[ "$f" =~ ^"observer.log".* ]]; then
        rm $f
    fi
done &&
cd test/case &&
rm -rf result_tmp